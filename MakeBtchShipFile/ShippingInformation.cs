﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShipInEcometry
{
    class ShippingInformation
    {
        public string Account;
        public string FullOrderNo;
        public string Carrier;
        public string Service;
        public Int64 Weight;
        public string TrackingNumber;
        public string EcometryShipMethod;
        public Int64 Cost;
        
        public string ShippingDate;
        public List<package> Packages;
        public List<SerialNumber> SerialNumbers;

    }

    class package
    {
        public string Weight;
        public string Cost;
        public string TrackingNumber;
        public string msn;
    }

    //jtr 09/11/14
    class SerialNumber
    {
        public decimal LineNo;
        public decimal EdpNo;
        public string SerialNo;
    }
}
