﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShipInEcometry
{
    class OrderToShip
    {
        public string FullOrderNo;
        public string ShipMethod;
        public string Weight;
        public string Cost;
        public string TrackingNumber;
        public string ShipDate;
        public string ManifestZone;
        public string ToBatchShip()
        {
            StringBuilder sb = new StringBuilder();
             
//Field Name	        Description	                            Format	    Position

//SL-BATCH-KEY	        Future Use	                            X(10)	    1-10
            sb.Append(" ".PadRight(10, ' '));
//SL-RECORD-TYPE	    Constant value - SL	                    X(2)	    11-12
            sb.Append("SL");

//SL-FULL-ORDER-NO	    Full Order Number (required)	        X(12)	    13-24
            sb.Append(FullOrderNo.PadRight(12,' ').Substring(0,12));
//SL-PACKAGE-ID	        Package ID	                            X(12)	    25-36
            sb.Append(" ".PadRight(12, ' '));
//SL-TRACKING-NO	    Tracking Number	                        X(50)	    37-86
            sb.Append(TrackingNumber.PadRight(50, ' ').Substring(0,50));
//SL-MANIFEST-NO	    Manifest Number	                        X(12)	    87-98
            sb.Append(DateTime.Now.ToString("yyyyMMdd").PadRight(12, ' '));
//SL-SHIP-DATE	        Shipment Date (ccyymmdd) (required)	    X(8)	    9-106
            sb.Append(ShipDate.PadRight(8,' ').Substring(0,8));
//SL-A-WGHT-X	        Actual Weight	                        9(5)v9(4)	107-115
            sb.Append(Weight);
//SL-B-WGHT-X	        Billed Weight	                        9(5)v9(4)	116-124
            sb.Append(Weight);
//SL-POSTAGE-X	        Postage	                                9(7)v99	    125-133
            sb.Append(Cost);
//SL-DV-X	            Declared Value	                        9(7)v99	    134-142
            sb.Append(" ".PadRight(10, ' '));
//SL-INS-X	            Insurance	                            9(7)v99	    143-151
            sb.Append(" ".PadRight(10, ' '));
//SL-SCHG-X	            Surcharge	                            9(7)v99	    152-160
            sb.Append(" ".PadRight(10, ' '));
//SL-COD-AMT-X	        COD Amount	                            9(7)v99	    161-169
            sb.Append(" ".PadRight(10, ' '));
//SL-SM-ECOM	                                               X(2)	        170-171
            sb.Append(" ".PadRight(10, ' '));
//SL-SM-3PTY	        Third Party Ship method	               X(10)	    172-181
            sb.Append(" ".PadRight(10, ' '));
//SL-SM-ID	            Future Use	                           X(6)	        182-187
            sb.Append(" ".PadRight(10, ' '));
//SL-SM-CODE            Future Use	                           X(4)	        188-191
            sb.Append(" ".PadRight(10, ' '));
//SL-STATION-ID	        Used with 3rd Party Inline Shipping	   X(8)	        192-199
            sb.Append(" ".PadRight(10, ' '));
//SL-ACTION-CODE	    Action Code	                           X(1)	        200
            sb.Append(" ".PadRight(10, ' '));
//SL-MANIFEST-ZONE	Manifest Zone	                       X(2)         201-202
                        sb.Append(ManifestZone.PadLeft(2,'0').Substring(0,2));
//SL-FILLER	        Spaces	                               X(54)	    203-256
            return sb.ToString().PadRight(54,' ');
        }
    }

}
