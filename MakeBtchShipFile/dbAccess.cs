﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;


namespace ShipInEcometry
{
    class dbAccess
    {
        private System.Data.SqlClient.SqlConnectionStringBuilder _csEcometry;

        private System.Data.SqlClient.SqlConnectionStringBuilder _csConnectShip;

        public dbAccess(string sqlAccount)
        {
            _csEcometry = new System.Data.SqlClient.SqlConnectionStringBuilder(Properties.Settings.Default.dbEcometry);
            _csConnectShip = new System.Data.SqlClient.SqlConnectionStringBuilder(Properties.Settings.Default.dbConnectShip);
            _csEcometry.InitialCatalog = sqlAccount;
        }

        public dbAccess()
        {
            _csConnectShip = new System.Data.SqlClient.SqlConnectionStringBuilder(Properties.Settings.Default.dbConnectShip);
        }


        public void VerifyAgainstShipFile(Int64 msn, string trackingNo)
        {
string sql =
       "Select TrackingNo from ShippingTransactions where Msn = @Msn ";


            using (System.Data.SqlClient.SqlConnection cn = new SqlConnection())
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                cn.ConnectionString = _csConnectShip.ToString();
                cn.Open();
                cmd.Connection = cn;
                cmd.CommandText = sql;
                cmd.Parameters.Add("@Msn", System.Data.SqlDbType.BigInt);
                cmd.Parameters["@Msn"].Value = msn;
                using (SqlDataReader rd = cmd.ExecuteReader())
                {
                    if (rd.HasRows)
                    {
                        rd.Read();
                        if (rd[0].ToString().Trim() != trackingNo.Trim())
                        {
                            string message = "MSN:" + msn.ToString() + " have missmatched tracking numbers Connectship:" + trackingNo + " Ecometry:" + rd[0].ToString();
                            SendMessage(message);
                        }
                    }
                    else
                    {
                        string message = "Shipfile MSN:" + msn.ToString() + " not found in ShippingTransactions ";
                        SendMessage(message);
                    }
                }


            }
        
        }

        /// <summary>
        /// Compare backup ShippingTransactions an create missing ones
        /// </summary>
        public void VerifyShippingTransactions()
        {
            string sql =
       " select "
       + " [STB].[Account] "
      + " ,[STB].[Carrier] "
      + " ,[STB].[Shipper] "
      + " ,[STB].[Document] "
      + " ,[STB].[ShipFile] "
      + " ,[STB].[Msn] "
      + " ,[STB].[FullOrderNo] "
      + " ,[STB].[Weight] "
      + " ,[STB].[TrackingNo] "
      + " ,[STB].[Service] "
      + " ,[STB].[EcometryShipMethod] "
      + " ,[STB].[Cost] "
      + " ,[STB].[ShipDate] "
      + " ,[STB].[CreateDate] "
      + " ,[STB].[StatusDate] "
      + " ,[STB].[Status] "
      + " ,[STB].[BarCode1] "
      + " ,[STB].[BarCode2] "
      + " ,[STB].[BarCode3] "
      + " from ShippingTransactionsBackup STB "
      + " left join ShippingTransactions ST on STB.Msn = ST.Msn "
      + " where isNull(ST.FullOrderNo,'Choose') = 'Choose' "
      + " and STB.Msn <> 0 ";


            using (System.Data.SqlClient.SqlConnection cn = new SqlConnection())
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                cn.ConnectionString = _csConnectShip.ToString();
                cn.Open();
                cmd.Connection = cn;
                cmd.CommandText = sql;
                using (SqlDataReader rd = cmd.ExecuteReader())
                {
                    if (rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Int64 msn = Convert.ToInt64(rd[rd.GetOrdinal("MSN")]);
                            string message = " Missing ShippingTransaction for MSN:" + msn.ToString("######");
                            SendMessage(message);
                            CreateShippingTransActionFromBackup(msn);
                        }

                    }
                }
            }

        }

        public void CreateShippingTransActionFromBackup(Int64 msn)
        {
            string sql =
      " Insert into ShippingTransactions "
      + " select "
      + " [STB].[Account] "
      + " ,[STB].[Carrier] "
      + " ,[STB].[Shipper] "
      + " ,[STB].[Document] "
      + " ,[STB].[ShipFile] "
      + " ,[STB].[Msn] "
      + " ,[STB].[FullOrderNo] "
      + " ,[STB].[Weight] "
      + " ,[STB].[TrackingNo] "
      + " ,[STB].[Service] "
      + " ,[STB].[EcometryShipMethod] "
      + " ,[STB].[Cost] "
      + " ,[STB].[ShipDate] "
      + " ,[STB].[CreateDate] "
      + " ,[STB].[StatusDate] "
      + " ,[STB].[Status] "
      + " ,[STB].[BarCode1] "
      + " ,[STB].[BarCode2] "
      + " ,[STB].[BarCode3] "
      + " from ShippingTransactionsBackup STB "
      + " WHERE MSN = @MSN ";

            using (System.Data.SqlClient.SqlConnection cn = new SqlConnection())
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                cn.ConnectionString = _csConnectShip.ToString();
                cn.Open();
                cmd.Connection = cn;
                cmd.CommandText = sql;
                cmd.Parameters.Add("@MSN",System.Data.SqlDbType.BigInt);
                cmd.Parameters["@MSN"].Value = msn;
                cmd.ExecuteNonQuery();
            }


        }
        private void SendMessage(string message)
        {

            Logger.ErrorLog.ErrorRoutine(false, message);
            SendEmail.SendEmail.EmailFrom = Properties.Settings.Default.EmailFrom;
            SendEmail.SendEmail.EmailTo = Properties.Settings.Default.EmailTo;
            SendEmail.SendEmail.SMTPPORT = Properties.Settings.Default.SMTPPort;
            SendEmail.SendEmail.SMTPServer = Properties.Settings.Default.SMPTServer;
            SendEmail.SendEmail.SMTPUSER = Properties.Settings.Default.SMTPUser;
            SendEmail.SendEmail.sendMessage(message, "Warning from ShipInEcometry");
        }

        /// <summary>
        /// New logic to create a pickticket from a control block
        /// </summary>
        /// <returns></returns>
        public string getPickTicket()
        {
            string pickTicket;
            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cn.ConnectionString = _csEcometry.ToString();
                    cmd.Connection = cn;
                    cmd.CommandText =
                        "select CTLDATA from CTLMAST where CTLID = '0000THLL-PTIKET'";
                    cn.Open();
                    using (SqlDataReader rd = cmd.ExecuteReader())
                    {
                        if (rd.HasRows)
                        {
                            rd.Read();
                            pickTicket = rd[0].ToString();
                        }
                        else
                        {
                            return "";
                        }
                    }

                    Int64 tempInt;
                    bool result = Int64.TryParse(pickTicket, out tempInt);
                    if (!result)
                    {
                        return "";
                    }

                    tempInt += 1;
                    pickTicket = tempInt.ToString("00000000");
                    cmd.CommandText = "UPDATE CTLMAST SET CTLDATA = '" + tempInt.ToString("00000000") +
                                      "' WHERE CTLID = '0000THLL-PTIKET'";

                    cmd.ExecuteNonQuery();
                }
            }

            return pickTicket;
        }


        /// <summary>
        /// Get Error Message</summary>
        /// <param name="MessageNo"> MessageNo</param>
        /// </summary>         
        public List<ShippingInformation> GetOrdersToShip(string account)
        {
            List<ShippingInformation> shippingInformations = new List<ShippingInformation>();

                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " Update ShippingTransactions set Status = 'Shipping' where Status = 'Pending'"

                            + " and Account = '" + account + "'";
                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        cmd.ExecuteNonQuery();
                    }
                }

                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " SELECT * from ShippingTransactions where Status = 'Shipping' "
                            + " and Account = '" + account + "'"
                            + " Order by FullOrderNo ";
                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();
                        ShippingInformation shippingInformation = null;
                        string holdFullOrderNo = "";
                        while (rd.Read())
                        {



                            if (rd[rd.GetOrdinal("FullOrderNo")].ToString() != holdFullOrderNo)
                            {
                                shippingInformation = new ShippingInformation();
                                shippingInformation.Packages = new List<package>();
                               
                                holdFullOrderNo = rd[rd.GetOrdinal("FullOrderNo")].ToString();
                                shippingInformation.SerialNumbers = GetSerialNumbers(account, holdFullOrderNo);

                                shippingInformations.Add(shippingInformation);
                                shippingInformation.Account = rd[rd.GetOrdinal("Account")].ToString();
                                shippingInformation.Carrier = rd[rd.GetOrdinal("Carrier")].ToString();
                                shippingInformation.Service = rd[rd.GetOrdinal("Service")].ToString();
                                shippingInformation.FullOrderNo = rd[rd.GetOrdinal("FullOrderNo")].ToString().PadRight(13, ' ');

                                shippingInformation.ShippingDate = rd[rd.GetOrdinal("ShipDate")].ToString().PadRight(13, ' ');
                                shippingInformation.TrackingNumber = rd[rd.GetOrdinal("TrackingNo")].ToString().PadRight(50, ' ');
                                shippingInformation.Cost = 0;
                                shippingInformation.Weight = 0;
                                string shipMethod = TranslateFromConnectShip(shippingInformation.Account, shippingInformation.Service, "SMTranslation");
                                shippingInformation.EcometryShipMethod = shipMethod;
                                //Verify that shipdate is a date, otherwise replace it with today's date
                                DateTime dt;
                                bool result = DateTime.TryParse(shippingInformation.ShippingDate, out dt);
                                if (!result)
                                {
                                    shippingInformation.ShippingDate = DateTime.Now.ToString("yyyyMMdd");
                                }

                            }
                            shippingInformation.Cost += (Convert.ToInt64(Convert.ToDecimal(rd[rd.GetOrdinal("COST")]) * 100));
                            //jtr 08/13/15 Multiplicatation now done in the shipping program
                            //shippingInformation.Weight += (Convert.ToInt64(rd[rd.GetOrdinal("Weight")]) * 10000);
                            shippingInformation.Weight += (Convert.ToInt64(rd[rd.GetOrdinal("Weight")]));

                            package shippingPackage = new package();
                            shippingInformation.Packages.Add(shippingPackage);
                            shippingPackage.msn = rd[rd.GetOrdinal("MSN")].ToString().PadRight(20, ' ');
                            shippingPackage.Cost = (Convert.ToDecimal(rd[rd.GetOrdinal("COST")]) * 100).ToString("000000000");
                            shippingPackage.TrackingNumber = rd[rd.GetOrdinal("TrackingNo")].ToString().PadRight(50, ' ');
                            shippingPackage.Weight = (Convert.ToDecimal(rd[rd.GetOrdinal("Weight")]) ).ToString("000000000");

                            

                        }
                    }
                }
                return shippingInformations;

            }

        

        public List<SerialNumber> GetSerialNumbers(string account, string fullOrderNo)
        {
            List<SerialNumber> serialNumbers = null;


            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = " SELECT * from SerialNumbers where Account = @Account and FullOrderNo = @FullOrderNo ";
                    cn.ConnectionString = _csConnectShip.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@FullOrderNo", SqlDbType.Char);
                    cmd.Parameters["@FullOrderNo"].Value = fullOrderNo;

                    cmd.Parameters.Add("@Account", SqlDbType.Char);
                    cmd.Parameters["@Account"].Value = account;


                    SqlDataReader rd = cmd.ExecuteReader();
                    while (rd.Read())
                    {
                        if (serialNumbers == null)
                        {
                            serialNumbers = new List<SerialNumber>();
                        }

                        SerialNumber sn = new SerialNumber();
                        sn.LineNo = Convert.ToDecimal(rd[rd.GetOrdinal("Line")]);
                        sn.EdpNo = Convert.ToDecimal(rd[rd.GetOrdinal("EdpNo")]);
                        sn.SerialNo = rd[rd.GetOrdinal("SerialNumber")].ToString();
                        serialNumbers.Add(sn);

                   }
                }
            }

            return serialNumbers;
        }

        /// <summary>
        /// Deletes manifest records after new ones are created
        /// </summary>
        public void CleanUpManifest()
        {
            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " Delete from MANIFESTENTRIES "
                                            + " where SHIPDATE = convert(char(8),getdate(),112) "
                                            + " and LTRIM(RTRIM(substring(MISCDATA110,28,22))) = '' ";

                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        cmd.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }

        }

        /// <summary>
        /// Finds new manifest records to create
        /// </summary>
        /// <param name="Account"></param>
        public void FixManifest(string Account)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " SELECT distinct ST.SLOTID, FullOrderNo "
                                        + " from CONNECTSHIP..ShippingTransactions ST "
                                        + " join MANIFESTENTRIES M on ST.FullOrderNo = M.FULLORDERNO  "
                                        + " where substring(M.MISCDATA110,28,22)= '' AND SHIPDATE = CONVERT(CHAR(8),GETDATE(),112) "
                                        + " and Account = '" + Account + "'"
                                        + " ORDER BY FullOrderNo";
                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();
                        ShippingInformation shippingInformation = null;
                        string holdFullOrderNo = "";
                        Int64 packageId = 1;
                        while (rd.Read())
                        {
                            if (rd[1].ToString() != holdFullOrderNo)
                            {
                                holdFullOrderNo = rd[1].ToString();
                                packageId = 1;
                            }
                            else
                            {
                                packageId += 1;
                            }
                            string slotId = rd[0].ToString();

                            CreateManifestRecord(slotId, packageId.ToString("00"));
                        }
                    }
                }

                CleanUpManifest();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Creates the new manifest records
        /// </summary>
        /// <param name="slotId"></param>
        /// <param name="packageId"></param>
        public void CreateManifestRecord(string slotId, string packageId)
        {
            string sql = " INSERT INTO MANIFESTENTRIES "
                + "select Distinct "
                        + " M.FULLORDERNO, "
                        + " M.MANIFESTNO, "
                        + " M.MANIFESTFLAGS, "
                        + " M.SHIPMETHOD, "
                        + " M.STATUS, "
                        + " M.MANIFESTZONE, "
                        + " M.WAREHOUSE, "
                        + "'" + packageId + "', "
                        + " M.COMPANY, "
                        + " M.DIVISION, "
                        + " M.NAMEX, "
                        + " M.REF1, "
                        + " M.REF2, "
                        + " M.STREET, "
                        + " M.CITY, "
                        + " M.STATE, "
                        + " M.ZIP, "
                        + " M.COUNTRYCODE, "
                        //jtr 08/13/15 Multiplication now done in the shipping program
                        + " cast(ST.Weight as numeric(18,0)), "
                        + " cast(ST.Weight as numeric(18,0)), "
                        + " M.DV, "
                        + " cast(ST.Cost*100 as numeric(18,0)) POSTAGE, "
                        + " M.INSURANCE, "
                        + " M.TSURCHARGE, "
                        + " M.CODAMOUNT, "
                        + " M.DOLLARSALES, "
                        + " M.SHIPDATE, "
                        + " M.DATECHG, "
                        + " M.TIMECHG, "
                        + " M.PROGRAMID, "
                        + " M.USERID, "
                        + " substring(M.MISCDATA110,1,28) + TrackingNo, "
                        + " M.STATUSMANIFEST, "
                        + " M.COMPANYNAME "
                        + " from CONNECTSHIP..ShippingTransactions ST "
                        + " join MANIFESTENTRIES M on ST.FullOrderNo = M.FULLORDERNO  "
                        + " where ST.SLOTID = " + slotId + " and substring(M.MISCDATA110,28,22)= '' ";
            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = sql;

                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        cmd.ExecuteNonQuery();
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }

        }

        /// <summary>
        /// Verifies that orders have been shipped
        /// </summary>
        /// <param name="account"></param>
        public void ValidateOrders(string account)
        {

            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " SELECT * from ShippingTransactions ST "
                        + " left join " + account + "..MANIFESTENTRIES  M on ST.FullOrderNo = M.FULLORDERNO "
                        + " where Status = 'Shipping1' "
                            + " and Account = '" + account + "'"
                            + " and (isnull(M.FULLORDERNO,'MISSING') <> ST.FullOrderNo "
                            + " or ST.TrackingNo <> substring(M.MISCDATA110,28,22)"
                            + " Order by FullOrderNo ";
                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();
                        ShippingInformation shippingInformation = null;
                        string holdFullOrderNo = "";
                        while (rd.Read())
                        {

                        }
                    }
                }


            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Before creating a new pick ticket, see if it already exists No longer used
        /// </summary>
        /// <param name="FullOrderNo"></param>
        /// <returns></returns>
        public bool DoesPickTicketExist(string FullOrderNo)
        {
            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cn.ConnectionString = _csEcometry.ToString();
                    cmd.Connection = cn;
                    cmd.CommandText = "select count(*) from ORDERXREF where FULLORDERNO = '" + FullOrderNo + "' and XREFNO like 'PT%'";
                    cn.Open();
                    Int64 count = Convert.ToInt64(cmd.ExecuteScalar());
                    if (count == 0)
                    { return false; }
                    else
                    { return true; }
                }
            }
        }

        /// <summary>
        /// Creates Pickticket and FSM records, Calls the update Shipping1 status process        /// </summary>
        /// <param name="shippingInformation"></param>
        public void ProcessShippment(ShippingInformation shippingInformation)
        {
            try
            {
                //Each order must have a PickTicket number in order Xref
                string pickTicket = getPickTicket();
                if (pickTicket.Trim() == string.Empty)
                {
                    throw new System.Exception("Blank PickTicket, check control 0000THLL-PTIKET");
                }
                Int64 tempInt;
                bool result = Int64.TryParse(pickTicket.Trim(), out tempInt);
                if (!result)
                {
                    throw new System.Exception("NonNumeric PickTicket, check control 0000THLL-PTIKET");
                }

                //*********************************************************
                //Store PickTicket in ORDERXREF
                //*********************************************************
                pickTicket = pickTicket.Trim().PadLeft(10, '0');
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {

                        cmd.CommandText = " Insert into ORDERXREF Values ( "
                            + "'" + shippingInformation.FullOrderNo + "' "
                            + ",'" + "PT" + pickTicket + "' "
                            + ",'PT'"
                            + ",'" + pickTicket + "' "
                            + " )";

                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;

                        //Only create pick ticket once
                        cmd.ExecuteNonQuery();

                        //*********************************************************
                        //Ship Confirmation Header
                        //*********************************************************
                        string fmsData_SC = "SC"
                            + pickTicket.PadRight(10, ' ').Substring(0, 10)
                            + shippingInformation.FullOrderNo.PadRight(13, ' ').Substring(0, 13)
                            + shippingInformation.ShippingDate.PadRight(8, ' ').Substring(0, 8)
                            + shippingInformation.EcometryShipMethod.PadLeft(2, ' ').Substring(0, 2)
                            + shippingInformation.Weight.ToString("000000000").PadLeft(9, '0').Substring(0, 9)
                            + shippingInformation.Cost.ToString("000000000").PadLeft(9, '0')
                            + "1";

                        //*********************************************************
                        //Create fmsRecord SC record
                        //*********************************************************
                        cmd.CommandText = "Insert into  FMSMSG values ("
                            //rectype
                            + " 'SC' "
                            //Process switch
                            + " ,'N' "
                            //Process Date
                            + " , '" + DateTime.Now.ToString("yyyyMMdd") + "' "
                            //FMS data
                            + " , '" + fmsData_SC + "'"
                        + ")";
                        cmd.ExecuteNonQuery();
                        //*********************************************************
                        //Create Carton Tracking record for each package
                        //*********************************************************
                        foreach (var package in shippingInformation.Packages)
                        {
                         string fmsData_CT = "CT"
                        + pickTicket.PadRight(10, ' ')
                        + shippingInformation.FullOrderNo.PadRight(13, ' ')
                        + package.TrackingNumber.PadRight(50, ' ')
                        + "08"
                        + shippingInformation.EcometryShipMethod.PadLeft(2, '0')
                        + package.Weight.PadLeft(9, '0')
                        + package.msn.PadRight(20, ' ');

                            //Create fmsRecord CT record
                            cmd.CommandText = "Insert into  FMSMSG values ("
                                //rectype
                                + " 'CT' "
                                //Process switch
                                + " ,'N' "
                                //Process Date
                                + " , '" + DateTime.Now.ToString("yyyyMMdd") + "' "
                                //FMS data
                                + " , '" + fmsData_CT + "'"
                            + ")";
                            cmd.ExecuteNonQuery();
                        }

                        if (shippingInformation.SerialNumbers != null)
                        {
                            foreach (var serialNumber in shippingInformation.SerialNumbers)
                            {
                                CreateSerailNumberRecord(serialNumber, pickTicket, shippingInformation.FullOrderNo);
                            }
                        }

                        ChangeStatusToShipping1(shippingInformation.FullOrderNo);
                    }


                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                throw;
            }
        }

        private bool CreateSerailNumberRecord(SerialNumber sn,string pickTicket, string fullOrderNo)
        {
            //    Field Name			Data Element			Attribute	Position
            //1	Record ID			Value ‘SN’				X(2)	1
            string message = "SN";

            //2	Pickticket Number		From pickticket download		X(10)	3
            message += pickTicket.PadRight(10,' ').Substring(0,10);
            //3	Ecometry Full Order Number	From pickticket download		X(13)	13
            message += fullOrderNo.PadRight(13, ' ').Substring(0,13);
            //4	Ecometry Order Line Number	From pickticket download		X(5)	26
            message += sn.LineNo.ToString("00000");
            //5	Item EDP Number	From pickticket download				9(9)	31
            message += sn.EdpNo.ToString("000000000");
            //6	Assigned Serial Number		Assigned Serial Number			X(30)	40
            message += sn.SerialNo.PadRight(30,' ').Substring(0,30);

            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cn.ConnectionString = _csEcometry.ToString();
                    cmd.Connection = cn;
                    cmd.CommandText = "Insert into FMSMSG "
                        + " Values ( "
                        + " 'SN',"  
                        + " 'N', "
                        + " '" + DateTime.Now.ToString("yyyyMMdd") + "',"
                        + "'" + message + "'"
                        + ")";

                    cn.Open();
                    cmd.ExecuteNonQuery();
                }
            }

            return true;
        }
        /// <summary>
        /// Updates the status to Shipping1
        /// </summary>
        /// <param name="fullOrderNo"></param>
        private void ChangeStatusToShipping1(string fullOrderNo)
        {
            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    cn.ConnectionString = _csConnectShip.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    cmd.CommandText = "Update ShippingTransactions set Status = 'Shipping1' "
                        + " where FullOrderNo = '" + fullOrderNo + "'";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Translates the Connectship Ship method back to the Ecometry ship method.
        /// </summary>
        /// <param name="Account"></param>
        /// <param name="ConnectShip"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
        public string TranslateFromConnectShip(string Account, string ConnectShip, string Type)
        {
            string result;
            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {

                        cmd.CommandText = " SELECT Ecometry from Translations where ConnectShip = '" + ConnectShip + "'"
                            + " and Account = '" + Account + "'"
                            + " and Type = '" + Type + "' ";

                        Console.WriteLine(cmd.CommandText);
                        //cn.ConnectionString = _csEcometry.ToString();
                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();

                        if (!rd.HasRows)
                        {
                            if (Account != "GLOBAL")
                            {
                                return TranslateFromConnectShip("GLOBAL", ConnectShip, Type);

                            }
                        }
                        rd.Read();

                        return rd[0].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }



    }


}
