﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using ShipInEcometry.AmpServices;

namespace ShipInEcometry
{
    class Program
    {
        static int Main(string[] args)
        {
            try
            {
 

                if (args.Length < 1)
                {
                    Console.WriteLine("Missing AccountName on run line **** Aborting");
                    return 10;
                }

                //Look for TEST if found throw a divide by zero error
                foreach (var item in args)
	            {
		             if (item.Trim().ToUpper()=="TEST")
                     {
                        int c = 0;
                         int d = 10/c;
                     }
	            }

                string account = args[0];
                string program = System.AppDomain.CurrentDomain.FriendlyName;
                string version = "2014-09-11";
                Console.WriteLine("Starting:" + program + " Version:" + version + " at " + DateTime.Now);
                
                //Validate backup
                //VerifyShippingTransacitons();

                ////Validate against Connectship
                //DateTime date;
                //string shipper;
                //string carrier;
                //carrier = "TANDATA_FEDEXFSMS.FEDEX";
                //shipper = "TMAB";
                //date = Convert.ToDateTime("09/23/2013");
                //ValidateAgainstShipFile( date,  shipper,  carrier);
                

                processOrders(account);

                //Give time for FMSMSG to process orders
                Console.WriteLine("Pausing 10 times for 59 seconds.");
                for (int i = 0; i < 10; i++)
                {
                    Console.WriteLine("Pausing for 59 seconds:" + DateTime.Now.ToString());
                    int pauseTime1 = 59000;
                    System.Threading.Thread.Sleep(pauseTime1);
                    Console.WriteLine("End Pause:" + DateTime.Now.ToString());
                }

                
                CleanUp( account);

                return 0;
               }
            catch (Exception ex)
            {
                ProcessError(ex);
                return 10;
            }
        }
        
        static void VerifyShippingTransacitons()
        {
            dbAccess db = new dbAccess();
            db.VerifyShippingTransactions();
        }

        static void CleanUp(string sqlAccount )
        {
            dbAccess db = new dbAccess(sqlAccount);
            db.FixManifest(sqlAccount);

            //db.ValidateOrders(sqlAccount);
        }

        //Get orders to ship
        static void processOrders(string sqlAccount )
        {
            dbAccess db = new dbAccess(sqlAccount);

        List<ShippingInformation> shippingInformations = db.GetOrdersToShip(sqlAccount);

        Console.WriteLine(DateTime.Now.ToString() + " Number of ORders to Ship:" + shippingInformations.Count().ToString("###0").Trim());
            foreach (var shippingInformation in shippingInformations)
            {
                Console.WriteLine("Shipping:" + shippingInformation.FullOrderNo);

                //Insert into FMSMSG
                db.ProcessShippment(shippingInformation);
                
            }


        }


        static void ProcessError(Exception ex)
        {
            Logger.ErrorLog.ErrorRoutine(false, ex);
            SendEmail.SendEmail.EmailFrom = Properties.Settings.Default.EmailFrom;
            SendEmail.SendEmail.EmailTo = Properties.Settings.Default.EmailTo;
            SendEmail.SendEmail.SMTPPORT = Properties.Settings.Default.SMTPPort;
            SendEmail.SendEmail.SMTPServer = Properties.Settings.Default.SMPTServer;
            SendEmail.SendEmail.SMTPUSER = Properties.Settings.Default.SMTPUser;
            SendEmail.SendEmail.sendError(ex);
        }

        public static String Version()
        {
            string response;
            // This variable holds the amount of indenting that  
            // should be used when displaying each line of information.
            int indent = 0;
            // Display information about the EXE assembly.
            Assembly a = System.Reflection.Assembly.GetExecutingAssembly();
            response = " ".PadLeft(indent, ' ') + "Assembly identity=" + a.FullName;
            response += Environment.NewLine;
            response += " ".PadLeft(indent, ' ') + "Codebase=" + a.CodeBase;
            response += Environment.NewLine;
            // Display the set of assemblies our assemblies reference.

            response += " ".PadLeft(indent, ' ') + "Referenced assemblies:";
            response += Environment.NewLine;
            foreach (AssemblyName an in a.GetReferencedAssemblies())
            {
                response += response = " ".PadLeft(indent, ' ') + "Name=" + an.Name + " Version=" + an.Version.ToString();
                response += Environment.NewLine;
            }
            return response;
        } 

        static void ValidateAgainstShipFile(DateTime date, string shipper, string carrier)

        {
            dbAccess db = new dbAccess();
            CoreXmlPortClient client = new CoreXmlPortClient();
            SearchRequest searchRequest = new SearchRequest();
            searchRequest.shipper = shipper;
            searchRequest.carrier = carrier;
            searchRequest.filters = new DataDictionary();
            searchRequest.filters.shipdate = date;
            SearchResponse  searchResponse = client.Search(searchRequest);

            foreach (var item in searchResponse.result.resultData)
            {
                db.VerifyAgainstShipFile(item.resultData.msn, item.resultData.trackingNumber);
                Console.WriteLine("Msn:" + item.resultData.msn.ToString() + " " + item.resultData.consignee.postalCode);
            }
        }
    }
}
