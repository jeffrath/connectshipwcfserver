// EncryptPassword.h : main header file for the EncryptPassword DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CEncryptPasswordApp
// See EncryptPassword.cpp for the implementation of this class
//

class CEncryptPasswordApp : public CWinApp
{
public:
	CEncryptPasswordApp();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
