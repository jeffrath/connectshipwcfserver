﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShippingAudit
{
   public class Shipment
    {
        public int msn;
        public string shipMethod;
        public string service;
        public decimal cost;
        public string shipper;
    }
}
