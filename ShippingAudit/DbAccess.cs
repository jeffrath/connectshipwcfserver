﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace ShippingAudit
{
    public class dbAccess
    {
        private System.Data.SqlClient.SqlConnectionStringBuilder _csEcometry;

        private System.Data.SqlClient.SqlConnectionStringBuilder _csConnectShip;

        public dbAccess(string sqlAccount)
        {
            _csEcometry = new System.Data.SqlClient.SqlConnectionStringBuilder(Properties.Settings.Default.dbEcometry);
            _csConnectShip = new System.Data.SqlClient.SqlConnectionStringBuilder(Properties.Settings.Default.dbConnectShip);
            _csEcometry.InitialCatalog = sqlAccount;
        }

        public dbAccess()
        {
            _csConnectShip = new System.Data.SqlClient.SqlConnectionStringBuilder(Properties.Settings.Default.dbConnectShip);
        }
        public Decimal CarrierPickupFee(string Account, string Company, string Division, string Configuration)
        {
            decimal carrierPickup = 0;
 
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " SELECT ValueX from ConfigurationSettings where NameX = '" + Configuration + "'"
                          + " and Account = '" + Account + "' "
                          + " and Company = '" + Company + "' "
                          + " and Division = '" + Division + "'";


                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();

                        if (!rd.HasRows)
                        {
                            if (Division != "00")
                            {
                                Division = "00";
                                return CarrierPickupFee(Account, Company, Division, Configuration);
                            }

                            if (Company != "00")
                            {
                                Company = "00";
                                return CarrierPickupFee(Account, Company, Division, Configuration);
                            }
                            if (Account != "GLOBAL")
                            {
                                Account = "GLOBAL";
                                return CarrierPickupFee(Account, Company, Division, Configuration);
                            }

                        }
                        rd.Read();
                        string tempCarrierPickup = rd[0].ToString();
                        decimal tempDecimal;
                        if (Decimal.TryParse(tempCarrierPickup, out tempDecimal))
                        {
                            return tempDecimal;
                        }
                        return 0;
                }
            }
        }


        public List<Shipment> GetShipments(string searchDate, string searchAccount)
        {
            List<Shipment> shipments = null;

            using (SqlConnection cn = new SqlConnection())
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Parameters.Add("@SearchDate", SqlDbType.Char);
                cmd.Parameters["@SearchDate"].Value = searchDate;
                cmd.Parameters.Add("@Account", SqlDbType.Char);
                cmd.Parameters["@Account"].Value = searchAccount;

                cn.ConnectionString = _csConnectShip.ToString();
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "Select Msn,Service,ShipMethod, Cost, Shipper "
                + " from ShippingTransactions where convert(char(8),CreateDate,112) = @SearchDate and Account = @Account";
               SqlDataReader rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    if (shipments == null)
                    {
                        shipments = new List<Shipment>();
                    }

                    Shipment shipment = new Shipment();
                    shipments.Add(shipment);
                    shipment.msn = Convert.ToInt32(rd[0]);
                    shipment.service = rd[1].ToString();
                    shipment.shipMethod = rd[2].ToString();
                    shipment.cost = Convert.ToDecimal(rd[3]);
                    shipment.shipper = rd[4].ToString();
                }

            }
            return shipments;
        }
    }
}
