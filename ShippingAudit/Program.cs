﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ShippingAudit.AmpServices;
using System.Data.SqlClient;


namespace ShippingAudit
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Must enter 2 parms: 1. Account 2: Date Offset");
                return;
            }

            string account = args[0];

            int tempInt;
            if (!(Int32.TryParse(args[1], out tempInt)))
            {
                Console.WriteLine("Date Offset(2nd parm) must be numeric):{0}", args[1]);
                return;
            }

            ProcessReport(tempInt, account);
        }

        static void ProcessReport(int dateOffset, string searchAccount)
        {
            dbAccess db = new dbAccess();
            DateTime date = DateTime.Now.AddDays(dateOffset);
            List<Shipment> shipments =  db.GetShipments(date.ToString("yyyyMMdd"), searchAccount);
            CoreXmlPortClient client = new CoreXmlPortClient();
            SearchRequest searchRequest = new SearchRequest();

            int lenght = shipments.Count();
            foreach (var shipment in shipments)
            {

                    searchRequest.shipper = shipment.shipper;
                    searchRequest.carrier = shipment.service;
                    searchRequest.filters = new DataDictionary();
                    searchRequest.filters.msn = shipment.msn;
                    SearchResponse searchResponse = client.Search(searchRequest);
                    string shipMethod = shipment.shipMethod;
                    decimal cost = shipment.cost;
                    decimal apportionedBase= searchResponse.result.resultData[0].resultData.apportionedBase.amount;
                    decimal fuelSurcharge = searchResponse.result.resultData[0].resultData.fuelSurcharge.amount;
                    decimal residentialFee = searchResponse.result.resultData[0].resultData.residentialDeliveryFee.amount;
                    decimal extendedFee = searchResponse.result.resultData[0].resultData.extendedAreaFee.amount;
                    decimal carrierPickup = db.CarrierPickupFee(searchAccount, "00", "00", shipMethod);
 
            }

        }

        static public void LogMessage(Exception ex)
        {

            SendEmail.SendEmail se = new SendEmail.SendEmail();
            Logger.ErrorLog.ErrorRoutine(false, ex);
            SendEmail.SendEmail.EmailFrom = Properties.Settings.Default.EmailFrom;
            SendEmail.SendEmail.EmailTo = Properties.Settings.Default.EmailTo;
            SendEmail.SendEmail.SMTPPORT = Properties.Settings.Default.SMTPPort;
            SendEmail.SendEmail.SMTPPWD = Properties.Settings.Default.SMTPPwd;
            SendEmail.SendEmail.SMTPServer = Properties.Settings.Default.SMPTServer;
            SendEmail.SendEmail.SMTPUSER = Properties.Settings.Default.SMTPUser;
            Console.WriteLine(SendEmail.SendEmail.sendError(ex));
        }


        static public void LogMessage(string message, int severity)
        {
            string subject = "Informational Message from " + Environment.GetCommandLineArgs()[0];
            if (severity == 1)
            {
                subject = "Warning Message from " + Environment.GetCommandLineArgs()[0];
            }
            Console.WriteLine(message);
            Logger.ErrorLog.ErrorRoutine(false, message);
            SendEmail.SendEmail se = new SendEmail.SendEmail();
            SendEmail.SendEmail.EmailFrom = Properties.Settings.Default.EmailFrom;
            SendEmail.SendEmail.EmailTo = Properties.Settings.Default.EmailTo;
            SendEmail.SendEmail.SMTPPORT = Properties.Settings.Default.SMTPPort;
            SendEmail.SendEmail.SMTPPWD = Properties.Settings.Default.SMTPPwd;
            SendEmail.SendEmail.SMTPServer = Properties.Settings.Default.SMPTServer;
            SendEmail.SendEmail.SMTPUSER = Properties.Settings.Default.SMTPUser;
            SendEmail.SendEmail.sendMessage(message, subject);

        }

    }
}
