﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EndOfDayManifesting.AmpService;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace EndOfDayManifesting
{

    class Program
    {
        static void Main(string[] args)
        {
            //CoreXmlPortClient client = new CoreXmlPortClient();
            //client.

            // Get/loop thru all the Shippers to close out
            // Get/loop thru all the Carriers for Each shipper
            // Get/loop thru all the ShipFiles 
            // Close out Each Shipfile
             StringBuilder sb = new StringBuilder();
            try
            {
                DBAccess db = new DBAccess("TESTSUPP");

                string shipDate = DateTime.Now.ToString("yyyyMMdd");
                List<ShipperCarrier> shipperCarriers = db.GetShippers(shipDate);

                ConnectShip cs = new ConnectShip();
                cs.GetCountries();
               
                foreach (var shipperCarrier in shipperCarriers)
                {
                    shipperCarrier.Documents = cs.GetCarrierDocuments(shipperCarrier.Shipper, shipperCarrier.Carrier);
                    if (shipperCarrier.Documents.Count() == 0)
                    {
                        Console.WriteLine(" No documents for:" + shipperCarrier.Shipper + " " +  shipperCarrier.Carrier );
                        continue;
                    }
                    Console.WriteLine("Closing:" + shipperCarrier.Shipper + " " + shipperCarrier.Carrier + " # of documents:" + shipperCarrier.Documents.Count());
                    cs.CloseOutDocuments(shipperCarrier);
                    string message = shipperCarrier.Shipper + " " + shipperCarrier.Carrier + " ";
                        foreach ( string  document in shipperCarrier.Documents)
	                    {
		                     message += document + " has been closed out" + Environment.NewLine;
	                    }  
                    sb.Append(message);
                }
                sb.Append(Environment.NewLine + "PROGRAM ENDED NORMALY");
            }
               
            catch (Exception ex)
            {
                LogMessage(ex);
            }

            finally 
            {
            LogMessage( sb.ToString());
            }
        }
        static public void LogMessage(Exception ex)
        {

            SendEmail.SendEmail se = new SendEmail.SendEmail();
            Logger.ErrorLog.ErrorRoutine(false, ex);
            SendEmail.SendEmail.EmailFrom = Properties.Settings.Default.EmailFrom;
            SendEmail.SendEmail.EmailTo = Properties.Settings.Default.EmailTo;
            SendEmail.SendEmail.SMTPPORT = Properties.Settings.Default.SMTPPort;
            SendEmail.SendEmail.SMTPPWD = Properties.Settings.Default.SMTPPwd;
            SendEmail.SendEmail.SMTPServer = Properties.Settings.Default.SMTPHost;
            SendEmail.SendEmail.SMTPUSER = Properties.Settings.Default.SMTPUser;
            Console.WriteLine(SendEmail.SendEmail.sendError(ex));
        }


        static public void LogMessage(string message, int severity = 0)
        {
            string subject = "Informational Message from " + Environment.GetCommandLineArgs()[0];
            if (severity == 1)
            {
                subject = "Warning Message from " + Environment.GetCommandLineArgs()[0];
            }
            Console.WriteLine(message);
            Logger.ErrorLog.ErrorRoutine(false, message);
            SendEmail.SendEmail se = new SendEmail.SendEmail();
            SendEmail.SendEmail.EmailFrom = Properties.Settings.Default.EmailFrom;
            SendEmail.SendEmail.EmailTo = Properties.Settings.Default.EmailTo;
            SendEmail.SendEmail.SMTPPORT = Properties.Settings.Default.SMTPPort;
            SendEmail.SendEmail.SMTPPWD = Properties.Settings.Default.SMTPPwd;
            SendEmail.SendEmail.SMTPServer = Properties.Settings.Default.SMTPHost;
            SendEmail.SendEmail.SMTPUSER = Properties.Settings.Default.SMTPUser;
            SendEmail.SendEmail.sendMessage(message, subject);

        }


    }
}
