﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EndOfDayManifesting
{
    class ShipperCarrier
    {
        public string Shipper;
        public string Carrier;
        public List<string> Documents;
    }
}
