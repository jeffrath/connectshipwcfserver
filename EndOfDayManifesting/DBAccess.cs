﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EndOfDayManifesting
{
    class DBAccess
    {

        private System.Data.SqlClient.SqlConnectionStringBuilder _csEcometry;

        private System.Data.SqlClient.SqlConnectionStringBuilder _csConnectShip;



        /// <summary>
        /// Construction for DBEcometryAccess</summary>
        /// <param name="sqlAccount"> Ecometry Account / SQL Database</param>
        /// </summary> 
        public DBAccess(string sqlAccount)
        {

            _csEcometry = new System.Data.SqlClient.SqlConnectionStringBuilder(Properties.Settings.Default.dbEcometry);
            _csEcometry.InitialCatalog = sqlAccount;
            _csConnectShip = new System.Data.SqlClient.SqlConnectionStringBuilder(Properties.Settings.Default.dbConnectShip);
        }

        public List<ShipperCarrier> GetShippers(string shipDate)
        {

            List<ShipperCarrier> shipperCarriers = new List<ShipperCarrier>();
            ShipperCarrier shipperCarrier = new ShipperCarrier();

            using (System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection())
            {
                using ( System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
                {
                    cn.ConnectionString = _csConnectShip.ToString();
                    cmd.Connection = cn;
                    cn.Open();
                    cmd.CommandText = "select distinct Shipper, Carrier from ShippingTransactions where convert(char(8),CreateDate,112) >= '" + shipDate + "'";
                     using ( System.Data.SqlClient.SqlDataReader dr = cmd.ExecuteReader())
                     {
                         while (dr.Read())
                         {
                             shipperCarrier = new ShipperCarrier();
                             shipperCarriers.Add(shipperCarrier);
                             shipperCarrier.Shipper = dr[0].ToString();
                             shipperCarrier.Carrier = dr[1].ToString();
                         }
                     }
                }
            }
            return shipperCarriers;
        }
    }
}
