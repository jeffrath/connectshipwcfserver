﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EndOfDayManifesting.AmpService;

namespace EndOfDayManifesting
{
 
    public class ResultMessage
    {
 
        public Identity[] Result { get; set; }

 
        public Boolean Success { get; set; }
 
        public string Message { get; set; }
 
        public List<string> Messages { get; set; }
 
        public byte[] binaryMessages { get; set; }
    }
}
