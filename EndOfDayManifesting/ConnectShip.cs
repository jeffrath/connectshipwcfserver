﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EndOfDayManifesting.AmpService;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace EndOfDayManifesting
{
    class ConnectShip
    {
        public ResultMessage GetCountries()
        {
            List<string> listOfCountries = new List<string>();

            // create new client proxy
            CoreXmlPortClient client = new CoreXmlPortClient();

            ListCountriesRequest listCountriesRequest = new ListCountriesRequest();
            ListCountriesResponse listCountriesResponse;
            listCountriesResponse = client.ListCountries(listCountriesRequest);
            ResultMessage rm = new ResultMessage();
            rm.Result = listCountriesResponse.result.resultData;
            rm.Success = true;
            rm.Message = "Success";

            return rm;
        }

        public List<string> GetCarrierDocuments(string shipper, string carrier)
        {
            CoreXmlPortClient client = new CoreXmlPortClient();

            List<string> documents = new List<string>();
            
            ListCloseOutItemsRequest cir = new ListCloseOutItemsRequest();
            cir.carrier = carrier;
            cir.shipper = shipper;

            ListCloseOutItemsResponse listCloseOutItemsResponse = client.ListCloseOutItems(cir);
            ResultMessage rm = new ResultMessage();
            rm.Result = listCloseOutItemsResponse.result.resultData;
            rm.Success = true;
            rm.Message = "Success";

            foreach (var item in rm.Result)
            {
                Console.WriteLine("      Closing:" + item.name);
                documents.Add(item.symbol);
            }

            return documents;
        }

        public void CloseOutDocuments(ShipperCarrier shipperCarrier)
        {
            CoreXmlPortClient client = new CoreXmlPortClient();
            CloseOutRequest closeOutRequest = new CloseOutRequest();
            closeOutRequest.carrier = shipperCarrier.Carrier;
            closeOutRequest.shipper = shipperCarrier.Shipper;

            foreach (var document in shipperCarrier.Documents)
            {
                closeOutRequest.closeOutItem = document;
                CloseOutResponse cop = client.CloseOut(closeOutRequest);

                if (cop.result.message.ToUpper() != "NO ERROR")
                {
                    Console.WriteLine(cop.result.message);
                    Logger.ErrorLog.ErrorRoutine(false, cop.result.message);
                    //Console.ReadLine();
                }
            }
        
        }

    }
}
