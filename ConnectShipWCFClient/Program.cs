﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConnectShipWCFClient.ThillConnectShip;

namespace ConnectShipWCFClient
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                System.Net.ServicePointManager.CertificatePolicy = new TrustAll.TrustAllCertificatePolicy();

                //pRequest.output = "pdf";
                //pRequest.destination = @"file:///c:/data/data";
                string destination = @"file:///c:/data/data";
                destination = "response";
                decimal[] weights = new decimal[2];
                weights[0] = 10.0M;
                weights[1] = 10.1M;

                //ThillFulFillmentHTTP.ThillFulfillmentClient clientWebHttp = new ThillFulFillmentHTTP.ThillFulfillmentClient();
                //ThillFulFillmentHTTP.ResultMessage webLogonHTTP = clientWebHttp.LogOn("TESTSUPP", "JEFFRATH", "JEFFRATH"); ;


                //// **** WEB HTTPS Connection

                //ThillFulfillmentWeb.ThillFulfillmentClient clientWeb = new ThillFulfillmentWeb.ThillFulfillmentClient();
                ////ThillFulfillmentWeb.ResultMessage webLogon = clientWeb.LogOn("TESTSUPP", "JEFFRATH","JEFFRATH");
                //ThillFulfillmentWeb.ShipAPackage sapWeb = clientWeb.ShipPackage("TESTSUPP", "", "S00002940101", weights, "BWI","png",destination);




                //**** Directly To Backend 

                ThillConnectShip.ShippingClient client = new ThillConnectShip.ShippingClient();
                CollateRequest cr = new CollateRequest();
                cr.FullOrderNo = "T00072650001";
                cr.Account = "TESTSUPP";
                cr.Shipper = "BWI";
                CollateResponce collateResponce = client.GetCollate(cr);

        
                //ThillConnectShip.ShipAPackage sp = client.ShipPackage("TESTSUPP", "", "S00002940101", weights, "BWI", "png", destination);
           
                //ThillConnectShip.ResultMessage OrdersInWarehouse = client.InWarehouseReport("TESTSUPP");
                    ThillConnectShip.ResultMessage logon = client.LogOn("TESTSUPP", "JEFFRATH", "JEFFRATH");
                // ThillConnectShip.ResultMessage documentFormats = client.GetDocumentFormats("TANDATA_UPS.UPS", "TANDATA_UPS_MAXICODE_US_DOMESTIC");

                //client.ShipPackage("ECOMTEST", "P00000010001");

                //  ResultMessage countries = client.GetCountries();
                // ThillConnectShip.ResultMessage services = client.GetServices("BWI");
                // ThillConnectShip.ResultMessage carriers = client.GetCarriers();
                //  ThillConnectShip.ResultMessage documents = client.GetDocuments("TANDATA_UPS.UPS", "ldtPackage");
                //  ThillConnectShip.ResultMessage documentFormats = client.GetDocumentFormats("TANDATA_UPS.UPS", "TANDATA_UPS_MAXICODE_US_DOMESTIC");
                //  ThillConnectShip.ResultMessage shippers = client.GetShippers();
                decimal d = 1.2M;

                //ThillConnectShip.ResultMessage pl = client.PrintALabel("SUPPLE","0"," ", "BWI", "", "PDF","Responce");
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
            }


        }

        static char[] Encrypt(char[] password)
        {
            //char *pass_pntr;
            char[] pass_pntr = new char[9];
            //   char encryptedpass[9], encryptedpassback[9], XXXX[100];
            char[] encryptedpass = new char[9];
            char[] encryptedpassback = new char[9];
            int key = 0;
            int idx;
            int chr;
            int rlen;
            int len = 0;

            //   cTrace("ENCRYPT - Enter: ", "1");
            //   memset(encryptedpass, 0, 9);
            //   memset(encryptedpassback, ' ', 9);
            for (int i = 0; i < 9; i++)
            {
                encryptedpass[i] = ' ';
                encryptedpassback[i] = ' ';
            }
            //encryptedpassback[8] = (char)0;

            password.CopyTo(pass_pntr, 0);
            //   //sprintf(XXXX,"pass_pntr = %s", pass_pntr);
            //   //cTrace("ENCRYPT: ", XXXX);
            len = 8;
            len = 0;
            rlen = 0;

            while (rlen < 8 && (int)pass_pntr[rlen] != 32)
            {
                rlen++;
                //pass_pntr++;
            }


            password.CopyTo(pass_pntr, 0);

            idx = 0;
            while (idx < 8)
            {
                if (idx == 0)
                    key = 12;
                if (idx == 1)
                    key = -7;
                if (idx == 2)
                    key = 9;
                if (idx == 3)
                    key = 15;
                if (idx == 4)
                    key = 2;
                if (idx == 5)
                    key = -5;
                if (idx == 6)
                    key = -11;
                if (idx == 7)
                    key = 6;

                chr = (int)pass_pntr[idx];

                chr = chr + key + rlen;

                if (chr <= 32)
                {
                    chr = chr + 48;
                }
                if (chr >= 126)
                {
                    chr = chr - 45;
                }

                encryptedpass[idx] = (char)chr;
                idx++;
                //pass_pntr++;
            }

            //   //sprintf(XXXX,"encryptedpass = %s", encryptedpass);
            //   //cTrace("ENCRYPT: ", XXXX);
            if (rlen == 2)
            {
                encryptedpassback[7] = encryptedpass[7];
                encryptedpassback[6] = encryptedpass[6];
                encryptedpassback[5] = encryptedpass[5];
                encryptedpassback[4] = encryptedpass[4];
                encryptedpassback[3] = encryptedpass[3];
                encryptedpassback[2] = encryptedpass[2];
                encryptedpassback[1] = encryptedpass[0];
                encryptedpassback[0] = encryptedpass[1];
            }
            if (rlen == 3)
            {
                encryptedpassback[7] = encryptedpass[7];
                encryptedpassback[6] = encryptedpass[6];
                encryptedpassback[5] = encryptedpass[5];
                encryptedpassback[4] = encryptedpass[4];
                encryptedpassback[3] = encryptedpass[3];
                encryptedpassback[2] = encryptedpass[0];
                encryptedpassback[0] = encryptedpass[2];
            }
            if (rlen == 4)
            {
                encryptedpassback[7] = encryptedpass[7];
                encryptedpassback[6] = encryptedpass[6];
                encryptedpassback[5] = encryptedpass[5];
                encryptedpassback[4] = encryptedpass[4];
                encryptedpassback[3] = encryptedpass[0];
                encryptedpassback[2] = encryptedpass[1];
                encryptedpassback[1] = encryptedpass[2];
                encryptedpassback[0] = encryptedpass[3];
            }
            if (rlen == 5)
            {
                encryptedpassback[7] = encryptedpass[7];
                encryptedpassback[6] = encryptedpass[6];
                encryptedpassback[5] = encryptedpass[5];
                encryptedpassback[4] = encryptedpass[0];
                encryptedpassback[3] = encryptedpass[1];
                encryptedpassback[1] = encryptedpass[3];
                encryptedpassback[0] = encryptedpass[4];
            }
            if (rlen == 6)
            {
                encryptedpassback[7] = encryptedpass[7];
                encryptedpassback[6] = encryptedpass[6];
                encryptedpassback[5] = encryptedpass[0];
                encryptedpassback[4] = encryptedpass[1];
                encryptedpassback[3] = encryptedpass[2];
                encryptedpassback[2] = encryptedpass[3];
                encryptedpassback[1] = encryptedpass[4];
                encryptedpassback[0] = encryptedpass[5];
            }
            if (rlen == 7)
            {
                encryptedpassback[7] = encryptedpass[7];
                encryptedpassback[6] = encryptedpass[0];
                encryptedpassback[5] = encryptedpass[1];
                encryptedpassback[4] = encryptedpass[2];
                encryptedpassback[2] = encryptedpass[4];
                encryptedpassback[1] = encryptedpass[5];
                encryptedpassback[0] = encryptedpass[6];
            }
            if (rlen == 8)
            {
                encryptedpassback[7] = encryptedpass[0];
                encryptedpassback[6] = encryptedpass[1];
                encryptedpassback[5] = encryptedpass[2];
                encryptedpassback[4] = encryptedpass[3];
                encryptedpassback[3] = encryptedpass[4];
                encryptedpassback[2] = encryptedpass[5];
                encryptedpassback[1] = encryptedpass[6];
                encryptedpassback[0] = encryptedpass[7];
            }

            //encryptedpassback[8] = (char)0;
            //encryptedpass[8] = (char)0;
            string returnstr = new string(encryptedpassback);

            return encryptedpassback;
            //            JEFFRATH	VQD\]WF^
            //MGR     	^ \2%NH)
        }
    }
}
