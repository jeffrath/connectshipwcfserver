﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.269
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConnectShipWCFClient.ThillFulFillmentHTTP {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ResultMessage", Namespace="http://schemas.datacontract.org/2004/07/ServiceLib")]
    [System.SerializableAttribute()]
    public partial class ResultMessage : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string[] MessagesField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool SuccessField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string[] Messages {
            get {
                return this.MessagesField;
            }
            set {
                if ((object.ReferenceEquals(this.MessagesField, value) != true)) {
                    this.MessagesField = value;
                    this.RaisePropertyChanged("Messages");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool Success {
            get {
                return this.SuccessField;
            }
            set {
                if ((this.SuccessField.Equals(value) != true)) {
                    this.SuccessField = value;
                    this.RaisePropertyChanged("Success");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ShipAPackage", Namespace="http://schemas.datacontract.org/2004/07/ServiceLib")]
    [System.SerializableAttribute()]
    public partial class ShipAPackage : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool SuccessField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool Success {
            get {
                return this.SuccessField;
            }
            set {
                if ((this.SuccessField.Equals(value) != true)) {
                    this.SuccessField = value;
                    this.RaisePropertyChanged("Success");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="EatMe", ConfigurationName="ThillFulFillmentHTTP.IThillFulfillment")]
    public interface IThillFulfillment {
        
        [System.ServiceModel.OperationContractAttribute(Action="EatMe/IThillFulfillment/GetItems", ReplyAction="EatMe/IThillFulfillment/GetItemsResponse")]
        ConnectShipWCFClient.ThillFulFillmentHTTP.ResultMessage GetItems(string sqlAccount, string fullOrderNo);
        
        [System.ServiceModel.OperationContractAttribute(Action="EatMe/IThillFulfillment/InWarehouseReport", ReplyAction="EatMe/IThillFulfillment/InWarehouseReportResponse")]
        ConnectShipWCFClient.ThillFulFillmentHTTP.ResultMessage InWarehouseReport(string sqlAccount, string shipperId);
        
        [System.ServiceModel.OperationContractAttribute(Action="EatMe/IThillFulfillment/LogOn", ReplyAction="EatMe/IThillFulfillment/LogOnResponse")]
        ConnectShipWCFClient.ThillFulFillmentHTTP.ResultMessage LogOn(string sqlAccount, string userId, string password);
        
        [System.ServiceModel.OperationContractAttribute(Action="EatMe/IThillFulfillment/PackCheck", ReplyAction="EatMe/IThillFulfillment/PackCheckResponse")]
        ConnectShipWCFClient.ThillFulFillmentHTTP.ResultMessage PackCheck(string sqlAccount, string fullOrderNo);
        
        [System.ServiceModel.OperationContractAttribute(Action="EatMe/IThillFulfillment/PrintALabel", ReplyAction="EatMe/IThillFulfillment/PrintALabelResponse")]
        ConnectShipWCFClient.ThillFulFillmentHTTP.ResultMessage PrintALabel(string sqlAccount, string msn, string carrier, string shipper, string document, string outputFormat, string destination);
        
        [System.ServiceModel.OperationContractAttribute(Action="EatMe/IThillFulfillment/PutSerialNumber", ReplyAction="EatMe/IThillFulfillment/PutSerialNumberResponse")]
        ConnectShipWCFClient.ThillFulFillmentHTTP.ResultMessage PutSerialNumber(string sqlAccount, string fullOrderNo, string edpNo, string serialNumber);
        
        [System.ServiceModel.OperationContractAttribute(Action="EatMe/IThillFulfillment/ShipPackage", ReplyAction="EatMe/IThillFulfillment/ShipPackageResponse")]
        ConnectShipWCFClient.ThillFulFillmentHTTP.ShipAPackage ShipPackage(string sqlAccount, string shipMethod, string fullOrderNo, decimal[] weight, string shipper, string outputFormat, string destination);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IThillFulfillmentChannel : ConnectShipWCFClient.ThillFulFillmentHTTP.IThillFulfillment, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ThillFulfillmentClient : System.ServiceModel.ClientBase<ConnectShipWCFClient.ThillFulFillmentHTTP.IThillFulfillment>, ConnectShipWCFClient.ThillFulFillmentHTTP.IThillFulfillment {
        
        public ThillFulfillmentClient() {
        }
        
        public ThillFulfillmentClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ThillFulfillmentClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ThillFulfillmentClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ThillFulfillmentClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public ConnectShipWCFClient.ThillFulFillmentHTTP.ResultMessage GetItems(string sqlAccount, string fullOrderNo) {
            return base.Channel.GetItems(sqlAccount, fullOrderNo);
        }
        
        public ConnectShipWCFClient.ThillFulFillmentHTTP.ResultMessage InWarehouseReport(string sqlAccount, string shipperId) {
            return base.Channel.InWarehouseReport(sqlAccount, shipperId);
        }
        
        public ConnectShipWCFClient.ThillFulFillmentHTTP.ResultMessage LogOn(string sqlAccount, string userId, string password) {
            return base.Channel.LogOn(sqlAccount, userId, password);
        }
        
        public ConnectShipWCFClient.ThillFulFillmentHTTP.ResultMessage PackCheck(string sqlAccount, string fullOrderNo) {
            return base.Channel.PackCheck(sqlAccount, fullOrderNo);
        }
        
        public ConnectShipWCFClient.ThillFulFillmentHTTP.ResultMessage PrintALabel(string sqlAccount, string msn, string carrier, string shipper, string document, string outputFormat, string destination) {
            return base.Channel.PrintALabel(sqlAccount, msn, carrier, shipper, document, outputFormat, destination);
        }
        
        public ConnectShipWCFClient.ThillFulFillmentHTTP.ResultMessage PutSerialNumber(string sqlAccount, string fullOrderNo, string edpNo, string serialNumber) {
            return base.Channel.PutSerialNumber(sqlAccount, fullOrderNo, edpNo, serialNumber);
        }
        
        public ConnectShipWCFClient.ThillFulFillmentHTTP.ShipAPackage ShipPackage(string sqlAccount, string shipMethod, string fullOrderNo, decimal[] weight, string shipper, string outputFormat, string destination) {
            return base.Channel.ShipPackage(sqlAccount, shipMethod, fullOrderNo, weight, shipper, outputFormat, destination);
        }
    }
}
