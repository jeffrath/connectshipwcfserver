﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Diagnostics; 
using System.Net;


namespace LoadOrders
{
    class SendEmail
    {
        public void sendMessage(string message, string subject)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(Properties.Settings.Default.SMTPServer);

                mail.From = new MailAddress(Properties.Settings.Default.SMTPUser);
                mail.To.Add(Properties.Settings.Default.EmailTo);
                mail.Subject = subject;
                mail.Body = message;

                SmtpServer.Port = Properties.Settings.Default.SMTPPort;
                SmtpServer.Credentials = new System.Net.NetworkCredential(Properties.Settings.Default.SMTPUser,Properties.Settings.Default.SMTPPassword);
                //SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public void sendError(Exception objException)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Source		: " + objException.Source.ToString().Trim());
            sb.AppendLine("Method		: " + objException.TargetSite.Name.ToString());
            sb.AppendLine("Date		: " + DateTime.Now.ToLongTimeString());
            sb.AppendLine("Time		: " + DateTime.Now.ToShortDateString());
            sb.AppendLine("Computer	: " + Dns.GetHostName().ToString());
            sb.AppendLine("Error		: " + objException.Message.ToString().Trim());
            sb.AppendLine("Stack Trace	: " + objException.StackTrace.ToString().Trim());

            string subject = Environment.GetCommandLineArgs()[0] + " :Application Error";
            sendMessage(sb.ToString(), subject);
        }
    }
}
