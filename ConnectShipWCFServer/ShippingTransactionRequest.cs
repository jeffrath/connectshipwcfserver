﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConnectShipWCFServer.AMPServices;

namespace ConnectShipWCFServer
{
    class ShippingTransactionRequest
    {
        public String FullOrderno { get; set; }
        public string Account { get; set; }
        public String EcometryShipMethod { get; set; }
        public string Service {get;set;}
        public string Carrier { get;set; }
        public string Document { get; set; }
        public string Shipper {get;set; }
        public string ShipFile {get;set;}
        public Int64 Msn { get; set; }

        public Money ShippingCost { get; set; }
        public Weight weight { get; set; }
        public string TrackingNo { get; set; }
        public DateTime ShipDate { get; set; }
        public String BarCode { get; set; }
        public String BarCode2 { get; set; }
        public String BarCode3 { get; set; }
    }
}
