﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ConnectShipWCFServer.AMPServices;


namespace ServiceLib
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.

    [ServiceContract(Namespace = Constants.Namespace)]
    public interface IShipping
    {

        //[OperationContract]
        //string GetData(int value);

        //[OperationContract]
        //CompositeType GetDataUsingDataContract(CompositeType composite);
        ////[OperationContract]
        ////ResultMessage WarehosueBackorder(WarehouseBackorderRequest WBO);

        [OperationContract]
        String Version();

        [OperationContract]
        KeyWordSearchResponce KeyWordSearch(KeyWordSearchRequest request);

        [OperationContract]
        ProductReviewResponse ProductReview(ProductReviewRequest request);

        [OperationContract]
        VoidShipmentResponse VoidShipment(VoidShipmentRequest vsrequest);

        [OperationContract]
        GetBOItemsResponce GetBOItems(GetBOItemsRequest GBI);

        [OperationContract]
        ResultMessage GetCountries();

        [OperationContract]
        ResultMessage GetDocuments(string carrier, string documentType);

        [OperationContract]
        ResultMessage GetDocumentFormats(string carrier, string document);

        [OperationContract]
        ResultMessage GetCarriers();

        [OperationContract]
        ResultMessage GetShippers();


        [OperationContract]
        ResultMessage GetServices(string shipper);

        [OperationContract]
        ResultMessage LogOn(string sqlAccount, string userId, string password);

        [OperationContract]
        ResultMessage PrintALabel(string sqlAccount, int msn, string carrier, string shipper, string document, string outputFormat, string destination);


        [OperationContract]
        ShipAPackage ShipPackage(string sqlAccount, string shipMethod, string fullOrderNo, int isNewShipment, decimal[] weight, string shipper, string outputFormat, string destination);

        [OperationContract]
        CollateResponce GetCollate(CollateRequest cr);


        //In Warehouse Report
        [OperationContract]
        ResultMessage InWarehouseReport(string sqlAccount, string shipperId);

        //get Items for Order Check
        [OperationContract]
        GetItemsResponce GetItems(GetItemsRequest gr);

        //Pack Check an order
        [OperationContract]
        ResultMessage PackCheck(BackorderRequest br);

        //Put a serial Number
        [OperationContract]
        ResultMessage PutSerialnumber(PutSerialNumberRequest request);

        [OperationContract]
        BackorderResponce BackOrderItems(BackorderRequest backorderRequest);

        [OperationContract]
        ShippingRateResponse ShippingRate(ShippingRateRequest shippingRateRequest);

       }

        //// Use a data contract as illustrated in the sample below to add composite types to service operations
        //[DataContract]
        //public class CompositeType
        //{
        //    bool boolValue = true;
        //    string stringValue = "Hello ";

        //    [DataMember]
        //    public bool BoolValue
        //    {
        //        get { return boolValue; }
        //        set { boolValue = value; }
        //    }

        //    [DataMember]
        //    public string StringValue
        //    {
        //        get { return stringValue; }
        //        set { stringValue = value; }
        //    }
        //}
 
   
    }


