﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using ServiceLib;
using System.Data.SqlClient;
using System.Data;
using ConnectShipWCFServer.AMPServices;

namespace ConnectShipWCFServer
{
    class DBAccess
    {
        private System.Data.SqlClient.SqlConnectionStringBuilder _csEcometry;

        private System.Data.SqlClient.SqlConnectionStringBuilder _csConnectShip;

        private string _sqlAccount;


        /// <summary>
        /// Construction for DBEcometryAccess</summary>
        /// <param name="sqlAccount"> Ecometry Account / SQL Database</param>
        /// </summary> 
        public DBAccess(string sqlAccount)
        {
            _sqlAccount = sqlAccount;
            _csEcometry = new System.Data.SqlClient.SqlConnectionStringBuilder(Properties.Settings.Default.dbEcometry);
            _csEcometry.InitialCatalog = sqlAccount;
            _csConnectShip = new System.Data.SqlClient.SqlConnectionStringBuilder(Properties.Settings.Default.dbConnectShip);
        }

        public ResultMessage GetOrderNo(string Account, string trackingNo)
        {
            ResultMessage rm = new ResultMessage();

            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " SELECT FullOrderNo from ShippingTransactions  where "
                        + "( "
                        + " TrackingNo = '" + trackingNo + "' "
                        + " or BarCode1 = '" + trackingNo + "' "
                        + " or BarCode2 = '" + trackingNo + "' "
                        + " or BarCode3 = '" + trackingNo + "' "
                        + " )"
                        + " and Account = '" + Account + "'";


                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();
                        if (rd.HasRows)
                        {
                            rd.Read();
                            rm.Success = true;
                            rm.Message = rd[0].ToString();
                            return rm;
                        }
                    }
                }
                rm = Message("CN035");
                rm.Message = rm.Message.Replace("<TrackingNo>", trackingNo);
                return rm;

            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                rm.Success = false;
                rm.Message = ex.ToString();
                return rm;
            }
        }
        public List<VoidPackage> GetPackagesToVoid(string Account, string FullOrderNo, string TrackingNo)
        {
            List<VoidPackage> vps = new List<VoidPackage>();
            try
            {
                if (TrackingNo.Trim() == "")
                {
                    TrackingNo = "NotSpaces";
                }

                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " Select Msn, Carrier from ShippingTransactions "
                            + " where (TrackingNo = '" + TrackingNo + "'"
                            + " or BarCode1 = '" + TrackingNo + "'"
                            + " or BarCode2 = '" + TrackingNo + "'"
                            + " or BarCode3 = '" + TrackingNo + "'"
                            + " or FullOrderNo = '" + FullOrderNo + "')"
                            + " and Status = 'Pending'  and Account = '" + Account + "'"; ;

                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        using (SqlDataReader rd = cmd.ExecuteReader())
                        {
                            if (!rd.HasRows)
                            {
                                return vps;
                            }

                            while (rd.Read())
                            {
                                VoidPackage vp = new VoidPackage();

                                vps.Add(vp);
                                vp.MSN = Convert.ToInt32(rd[0]);
                                vp.Carrier = rd[1].ToString();
                            }
                        }

                    }
                }

                return vps;
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                //rm.Success = false;
                //rm.Message = ex.ToString();
                //return rm;
                throw;
            }
        }

        public ResultMessage GetEdpNoUsingItemNo(string criteria)
        {
            ResultMessage rm = new ResultMessage();
            try
            {
                criteria = criteria.Trim().ToUpper();
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " Select EDPNO from ITEMMAST where ITEMNO = '" + criteria + "'";

                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        using (SqlDataReader rd = cmd.ExecuteReader())
                        {

                            while (rd.Read())
                            {
                                rm.Success = true;
                                rm.Message = rd[0].ToString();
                                return rm;
                            }
                        }
                        // removes spaces from Criteria and check for an alias
                        cmd.CommandText = " Select EDPNO from EDPITEMXREF where REFITEMNO = 'SYST-" + criteria + "' and STATUS = 'A'";
                        cmd.Connection = cn;
                        using (SqlDataReader rd = cmd.ExecuteReader())
                        {

                            while (rd.Read())
                            {
                                rm.Success = true;
                                rm.Message = rd[0].ToString();
                                return rm;
                            }
                        }

                    }
                }

                //No Item found
                rm.Success = false;
                rm.Message = "No Item matches criteria:" + criteria;
                return rm;
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }
        }


        public string GetItemNoUsingEdpno(string criteria, out string description)
        {

            try
            {
                string itemno = "";
                description = "";
                criteria = criteria.Trim().ToUpper();
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " Select ITEMNO, DESCRIPTION from ITEMMAST where EDPNO = " + criteria;

                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        using (SqlDataReader rd = cmd.ExecuteReader())
                        {

                            while (rd.Read())
                            {
                                itemno = rd[rd.GetOrdinal("ITEMNO")].ToString();
                                description = rd[rd.GetOrdinal("DESCRIPTION")].ToString();
                            }
                        }

                    }
                }

                return itemno;
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }
        }

        public ResultMessage GetEdpNoUsingUpc(string criteria)
        {
            ResultMessage rm = new ResultMessage();
            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " select EDPNO from EDPITEMXREF where REFITEMNO = 'SYST-" + criteria.Trim() + "' and STATUS = 'U'";


                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        using (SqlDataReader rd = cmd.ExecuteReader())
                        {
                            if (!rd.HasRows)
                            {
                                rm.Success = false;
                                rm.Message = "No Item for UPC:" + criteria;
                                return rm;
                            }

                            while (rd.Read())
                            {
                                rm.Success = true;
                                rm.Message = rd[0].ToString();
                            }
                        }

                    }
                }

                return rm;
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }
        }


        public int GetPhysicalInventory(string criteria)
        {
            int response = 0;
            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " select isnull(sum(AVAILABLEINV),0) AVAILABLEINV from INVDETAILS where STATUS like '%U' and EDPNO  = " + criteria;


                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        using (SqlDataReader rd = cmd.ExecuteReader())
                        {


                            while (rd.Read())
                            {
                                response = Convert.ToInt16(rd[rd.GetOrdinal("AVAILABLEINV")]);
                            }
                        }

                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }
        }

        public List<InventoryDetail> GetInventoryDetails(string criteria)
        {
            List<InventoryDetail> response = new List<InventoryDetail>();
            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " select WAREHOUSELOC, STATUS, sum(AVAILABLEINV) AVAILABLEINV from INVDETAILS  where EDPNO  = " + criteria + "GROUP by WAREHOUSELOC, STATUS";


                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        using (SqlDataReader rd = cmd.ExecuteReader())
                        {

                            while (rd.Read())
                            {
                                InventoryDetail iv = new InventoryDetail();
                                response.Add(iv);
                                iv.Location = rd[rd.GetOrdinal("WAREHOUSELOC")].ToString();
                                iv.Quantity = Convert.ToInt32(rd[rd.GetOrdinal("AVAILABLEINV")]);
                                string Status = rd[rd.GetOrdinal("STATUS")].ToString();

                                if (Status.Trim().EndsWith("U"))
                                {
                                    iv.Status = "Usable ";
                                }
                                else
                                {
                                    iv.Status = "Non Usable ";
                                }

                                if (Status.StartsWith("1") || Status.StartsWith("2") || Status.StartsWith("3"))
                                {
                                    iv.Status += "Primary ";
                                }
                                else
                                {
                                    iv.Status += "Bulk ";
                                }



                            }
                        }

                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }
        }

        public string GetCodeDesc(string criteria)
        {
            string response = "Undefined:" + criteria.Trim();
            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " select ABRV from CODEDESC  where CODEKEY  = '" + criteria + "'";


                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        using (SqlDataReader rd = cmd.ExecuteReader())
                        {

                            while (rd.Read())
                            {
                                response = rd[rd.GetOrdinal("ABRV")].ToString();
                            }
                        }

                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }
        }

        public WarehouseLocation GetLocationData(string criteria)
        {
            WarehouseLocation response = new WarehouseLocation();
            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " select * from WHSELOCS  where WAREHOUSELOC  = '" + criteria + "'";


                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        using (SqlDataReader rd = cmd.ExecuteReader())
                        {

                            while (rd.Read())
                            {
                                string testNumber = rd[rd.GetOrdinal("CAPACITY")].ToString();
                                int temp;
                                bool isInt = Int32.TryParse(testNumber, out temp);

                                response.Capacity = temp;
                                response.Location = rd[rd.GetOrdinal("WAREHOUSELOC")].ToString();

                                testNumber = rd[rd.GetOrdinal("RESTOCKPOINT")].ToString();
                                isInt = Int32.TryParse(testNumber, out temp);
                                response.Restock = temp;



                            }
                        }

                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }
        }


        public List<InventoryTrans> GetInventoryTrans(string criteria)
        {
            List<InventoryTrans> response = new List<InventoryTrans>();
            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " select * from INVENTORYTRANS  where EDPNO  = " + criteria + " ORDER BY TRANSACTIONDATE DESC, TRANSACTIONTIME DESC";


                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        using (SqlDataReader rd = cmd.ExecuteReader())
                        {

                            while (rd.Read())
                            {
                                InventoryTrans it = new InventoryTrans();
                                response.Add(it);

                                string CreateDate = rd[rd.GetOrdinal("TRANSACTIONDATE")].ToString();

                                if (CreateDate.Length == 8)
                                {
                                    CreateDate = FormatDate(CreateDate);
                                }
                                it.Date = CreateDate;

                                string time = rd[rd.GetOrdinal("TRANSACTIONTIME")].ToString();
                                if (time.Length == 8)
                                {
                                    time = time.Substring(0, 2) + ":" + time.Substring(3, 2) + ":" + time.Substring(5, 2);
                                }

                                it.Time = time;
                                it.FromLocation = rd[rd.GetOrdinal("FROMREF")].ToString();
                                it.ToLocation = rd[rd.GetOrdinal("TOREF")].ToString();
                                it.Quantity = Convert.ToInt32(rd[rd.GetOrdinal("QTY")]);
                                it.Who = rd[rd.GetOrdinal("USERID")].ToString();
                                it.TransactionNo = rd[rd.GetOrdinal("TRANSACTIONNO")].ToString();

                            }
                        }

                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }
        }

        public List<ItemDescription> GetByKeword(string criteria)
        {
            List<ItemDescription> itemDescriptions = new List<ItemDescription>();

            string sql = "SELECT IM.ITEMNO, IM.DESCRIPTION, IM.STATUS, PRICE, AVAILABLEINV "
                         + "FROM ITEMKEYWORDS IK  "
                         + " JOIN ITEMMAST IM ON IK.EDPNO = IM.EDPNO  "
                         + " WHERE  "
                         + " CONTAINS(IK.DESCRIPTION,'" + criteria + "')  ";

            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = sql;
                    cn.ConnectionString = _csEcometry.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    using (SqlDataReader rd = cmd.ExecuteReader())
                    {
                        while (rd.Read())
                        {
                            ItemDescription id = new ItemDescription();
                            itemDescriptions.Add(id);
                            id.ItemDesc = rd[rd.GetOrdinal("DESCRIPTION")].ToString().Trim();
                            id.ItemNo = rd[rd.GetOrdinal("ITEMNO")].ToString().Trim();
                            id.ItemStatus = rd[rd.GetOrdinal("STATUS")].ToString().Trim();
                            id.Price = Convert.ToDecimal(rd[rd.GetOrdinal("PRICE")]) / 100M;
                            id.AvailableInv = Convert.ToInt64(rd[rd.GetOrdinal("AVAILABLEINV")]);

                        }
                    }
                }
            }
            return itemDescriptions;
        }
        public ProductReviewResponse GetProdReview(string criteria)
        {
            ProductReviewResponse response = new ProductReviewResponse();
            try
            {
                ResultMessage rm = GetEdpNoUsingItemNo(criteria);

                if (!rm.Success)
                {
                    rm = GetEdpNoUsingUpc(criteria);
                }

                if (!rm.Success)
                {
                    response.SetMessage(rm);
                    return response;
                }

                //must have found edpno 
                criteria = rm.Message;

                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " select * from ITEMMAST where EDPNO = " + criteria;


                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        using (SqlDataReader rd = cmd.ExecuteReader())
                        {

                            while (rd.Read())
                            {
                                response.AveCost = 0.0M;
                                response.Backorders = Convert.ToInt32(rd[rd.GetOrdinal("CURRENTBO")]);

                                response.ItemNo = rd[rd.GetOrdinal("ITEMNO")].ToString();

                                response.CurrentlyAvailable = Convert.ToInt32(rd[rd.GetOrdinal("AVAILABLEINV")]);
                                response.Depth = Convert.ToDecimal(rd[rd.GetOrdinal("DEPTH")]) / 100M;
                                response.Height = Convert.ToDecimal(rd[rd.GetOrdinal("LENGTHX")]) / 100M;

                                string description;
                                string itemno = rd[rd.GetOrdinal("ACTUALEDPNO")].ToString();
                                itemno = GetItemNoUsingEdpno(itemno, out description);
                                string CreateDate = rd[rd.GetOrdinal("ADDITIONALDATA")].ToString().PadRight(50, ' ').Substring(38, 8);

                                if (CreateDate.Length == 8)
                                {
                                    CreateDate = CreateDate.Substring(4, 2) + "/" + CreateDate.Substring(6, 2) + "/" + CreateDate.Substring(2, 2);
                                }
                                response.CreateDate = CreateDate;
                                response.HowManyOfItem = itemno;
                                response.HowManyOfItemDescription = description;
                                response.HowManyOfQty = Convert.ToInt32(rd[rd.GetOrdinal("ACTUALEDPNO")]);
                                response.InProcessInventory = 0;
                                response.ItemDescription = rd[rd.GetOrdinal("DESCRIPTION")].ToString();

                                response.ItemStatus = rd[rd.GetOrdinal("STATUS")].ToString();
                                response.ItemStatus = GetCodeDesc("IT" + response.ItemStatus.Trim());

                                response.ReOrder = 0;

                                response.Locations = new List<WarehouseLocation>();
                                WarehouseLocation location;
                                if (rd[rd.GetOrdinal("WAREHOUSELOCS_001")].ToString().Trim() != "")
                                {
                                    location = GetLocationData(rd[rd.GetOrdinal("WAREHOUSELOCS_001")].ToString().Trim());
                                    response.Locations.Add(location);
                                }

                                if (rd[rd.GetOrdinal("WAREHOUSELOCS_002")].ToString().Trim() != "")
                                {
                                    location = GetLocationData(rd[rd.GetOrdinal("WAREHOUSELOCS_002")].ToString().Trim());
                                    response.Locations.Add(location);
                                }
                                if (rd[rd.GetOrdinal("WAREHOUSELOCS_003")].ToString().Trim() != "")
                                {
                                    location = GetLocationData(rd[rd.GetOrdinal("WAREHOUSELOCS_003")].ToString().Trim());
                                    response.Locations.Add(location);
                                }
                                if (rd[rd.GetOrdinal("WAREHOUSELOCS_004")].ToString().Trim() != "")
                                {
                                    location = GetLocationData(rd[rd.GetOrdinal("WAREHOUSELOCS_004")].ToString().Trim());
                                    response.Locations.Add(location);
                                }
                                if (rd[rd.GetOrdinal("WAREHOUSELOCS_005")].ToString().Trim() != "")
                                {
                                    location = GetLocationData(rd[rd.GetOrdinal("WAREHOUSELOCS_005")].ToString().Trim());
                                    response.Locations.Add(location);
                                }
                                if (rd[rd.GetOrdinal("WAREHOUSELOCS_006")].ToString().Trim() != "")
                                {
                                    location = GetLocationData(rd[rd.GetOrdinal("WAREHOUSELOCS_006")].ToString().Trim());
                                    response.Locations.Add(location);
                                }
                                if (rd[rd.GetOrdinal("WAREHOUSELOCS_007")].ToString().Trim() != "")
                                {
                                    location = GetLocationData(rd[rd.GetOrdinal("WAREHOUSELOCS_007")].ToString().Trim());
                                    response.Locations.Add(location);
                                }
                                if (rd[rd.GetOrdinal("WAREHOUSELOCS_008")].ToString().Trim() != "")
                                {
                                    location = GetLocationData(rd[rd.GetOrdinal("WAREHOUSELOCS_008")].ToString().Trim());
                                    response.Locations.Add(location);
                                }
                                if (rd[rd.GetOrdinal("WAREHOUSELOCS_009")].ToString().Trim() != "")
                                {
                                    location = GetLocationData(rd[rd.GetOrdinal("WAREHOUSELOCS_009")].ToString().Trim());
                                    response.Locations.Add(location);
                                }
                                if (rd[rd.GetOrdinal("WAREHOUSELOCS_010")].ToString().Trim() != "")
                                {
                                    location = GetLocationData(rd[rd.GetOrdinal("WAREHOUSELOCS_010")].ToString().Trim());
                                    response.Locations.Add(location);
                                }

                                response.MinOrderQty = Convert.ToInt32(rd[rd.GetOrdinal("MINORDERQTY")]); ;

                                response.PhysicalInventory = GetPhysicalInventory(criteria);
                                response.Price = Convert.ToInt32(rd[rd.GetOrdinal("PRICE")]);
                                //response.Status = rd[rd.GetOrdinal("STATUS")].ToString();



                                itemno = rd[rd.GetOrdinal("ALTEDPNO")].ToString();
                                itemno = GetItemNoUsingEdpno(itemno, out description);
                                response.SubWith = itemno;
                                response.SubWithDescription = description;
                                response.Weight = Convert.ToDecimal(rd[rd.GetOrdinal("WEIGHT")]) / 10000M; ;
                                response.Width = Convert.ToDecimal(rd[rd.GetOrdinal("WIDTH")]) / 100M;

                                response.InventoryDetails = GetInventoryDetails(criteria);
                                response.InventoryTrans = GetInventoryTrans(criteria);
                                //response.Cost = Convert.ToDecimal(rd[rd.GetOrdinal("INVCOST_001")]) / 100M;


                            }
                        }

                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }
        }


        /// <summary>
        /// Get Error Message</summary>
        /// <param name="MessageNo"> MessageNo</param>
        /// </summary>         
        public ResultMessage Message(string MessageNo)
        {

            ResultMessage rm = new ResultMessage();

            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " SELECT * from Messages where MessageNo = '" + MessageNo + "'";
                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();
                        if (!rd.HasRows)
                        {
                            rm.Success = false;
                            rm.Message = MessageNo + ": Not Defined!";
                            return rm;
                        }
                        rd.Read();

                        if ((int)rd[rd.GetOrdinal("Success")] == 1)
                        {
                            rm.Success = true;
                        }
                        else
                        {
                            rm.Success = false;
                        }
                        rm.Message = rd[rd.GetOrdinal("MessageNo")].ToString().Trim() + ":" + rd[rd.GetOrdinal("MessageX")].ToString();
                        return rm;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                rm.Success = false;
                rm.Message = ex.ToString();
                return rm;
            }
        }


        /// <summary>
        /// Get Return Backorder Items</summary>
        /// <param name="gBIRequest"> Class that contains all the calling code</param>
        /// </summary>         
        public GetBOItemsResponce GetBOItems(GetBOItemsRequest gBIRequest)
        {
            GetBOItemsResponce gboiResponce = new GetBOItemsResponce();


            string orderNo = gBIRequest.FullOrderNo.PadRight(8, ' ').Substring(0, 8);


            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText =
                        " SELECT I.EDPNO, ITEMNO, DESCRIPTION, A.QTY , [LINENO]"
                        + " FROM "
                        + "("
                        + " Select  EDPNOS_001 EDPNO , ITEMQTYS_001 QTY, LINENOS_001 [LINENO] FROM ORDERSUBHEAD WHERE ORDERNO = '" + orderNo + "'"
                        + " AND EDPNOS_001 <> 0 AND SUBSTRING(BIGSTATUS,2,1) IN ('B', 'K')"
                        + "UNION ALL"
                        + " Select  EDPNOS_002, ITEMQTYS_002, LINENOS_002 [LINENO]  FROM ORDERSUBHEAD WHERE ORDERNO = '" + orderNo + "'"
                        + " AND EDPNOS_002 <> 0 AND SUBSTRING(BIGSTATUS,3,1) IN ('B', 'K')"
                        + "UNION ALL"
                        + " Select  EDPNOS_003, ITEMQTYS_003, LINENOS_003 [LINENO]  FROM ORDERSUBHEAD WHERE ORDERNO = '" + orderNo + "'"
                        + " AND EDPNOS_003 <> 0 AND SUBSTRING(BIGSTATUS,4,1) IN ('B','K')"
                        + ") A"
                        + " JOIN ITEMMAST I ON A.EDPNO = I.EDPNO ";

                    cn.ConnectionString = _csEcometry.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    SqlDataReader rd = cmd.ExecuteReader();
                    while (rd.Read())
                    {
                        if (gboiResponce.Items == null)
                        {
                            gboiResponce.Items = new List<Item>();
                        }
                        Item i = new Item();
                        gboiResponce.Items.Add(i);
                        i.edpNo = Convert.ToInt32(rd[0]);
                        i.ItemNo = rd[1].ToString();
                        i.Description = rd[2].ToString();
                        i.Qty = Convert.ToInt32(rd[3]);
                        i.lineNo = Convert.ToInt16(rd[4]);
                    }
                }
            }

            return gboiResponce;
        }



        /// <summary>
        /// Get Customer information for the person recieving information</summary>
        /// <param name="fullOrderNo"> Fullordenro for the customer to get ifnormation for</param>
        /// </summary>         
        public Customer GetShipShipTo(string fullOrderNo)
        {
            string custEdp = "";

            fullOrderNo = fullOrderNo.PadRight(10, ' ').Substring(0, 10) + "00";

            string shipToLevel = fullOrderNo.PadRight(8, ' ').Substring(8, 2);

            if (shipToLevel != "00" && shipToLevel != "50")
            {
                custEdp = GetOrderXrefShipTo(fullOrderNo);
            }
            else
            {
                custEdp = GetBuyer(fullOrderNo);
            }
            if (custEdp.Trim() == string.Empty)
            {
                //If no customer just return it empty
                return new Customer();
            }
            Customer customer = GetCustomer(custEdp);
            return customer;

        }


        /// <summary>
        /// Get BUYER </summary>
        /// <param name="fullOrderNo"> fullOrderNo related to the ORDERHEADER record</param>
        /// </summary> 
        public string GetBuyer(string fullOrderNo)
        {
            string custEdp = "";
            fullOrderNo = fullOrderNo.Substring(0, 8) + "0000";
            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cn.ConnectionString = _csEcometry.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    cmd.CommandText = " Select  convert(char(9),CUSTEDP) from ORDERHEADER where FULLORDERNO = '" + fullOrderNo + "'";
                    SqlDataReader rd = cmd.ExecuteReader();
                    while (rd.Read())
                    {
                        custEdp = rd[0].ToString();
                    }
                }
            }

            return custEdp;
        }


        /// <summary>
        /// Get ORDERXREF record.</summary>
        /// <param name="fullOrderNo"> fullOrderNo related to the ORDERXREF record</param>
        /// </summary> 
        public string GetOrderXrefShipTo(string fullOrderNo)
        {
            string custEdp = "";

            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = " Select  substring(XREFNO,3,9) from ORDERXREF where FULLORDERNO = '" + fullOrderNo + "'";
                    cn.ConnectionString = _csEcometry.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    SqlDataReader rd = cmd.ExecuteReader();
                    while (rd.Read())
                    {
                        custEdp = rd[0].ToString();
                    }
                }
            }

            return custEdp;
        }

        /// <summary>
        /// Get ORDERXREF record.</summary>
        /// <param name="fullOrderNo"> fullOrderNo related to the ORDERXREF record</param>
        /// </summary> 
        public ResultMessage PackCheck(string fullOrderNo)
        {
            ResultMessage rm = new ResultMessage();
            try
            {

                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        //12/20/13 update date for pack check
                        cmd.CommandText = " UPDATE ORDERSUBHEAD set BIGSTATUS = 'Z' + substring(BIGSTATUS,2,3), SHIPDATE = convert(char(8),getdate(),112) where FULLORDERNO = '" + fullOrderNo + "'";
                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        cmd.ExecuteNonQuery();
                    }
                }
                rm.Success = true;
                rm.Message = "Success";
                return rm;
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                rm.Success = false;
                rm.Message = ex.ToString();
                return rm;
            }
        }

        public AMPServices.Commodity[] GetCommidtyContents(string FullOrderNo, decimal weight)
        {
            List<AMPServices.Commodity> commodities = new List<AMPServices.Commodity>();
            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = " select * "
                    + "from CommodityItemMaster C "
                    + " join " + _sqlAccount + "..ITEMMAST I on C.EdpNo = I.EDPNO "
                    + " join ( "
                       + " select EDPNOS_001 EDPNO,ITEMQTYS_001 QTY,  EXTPRICES_001/ITEMQTYS_001 UnitPrice "
                                    + " from " + _sqlAccount + "..ORDERSUBHEAD OSH "
                                    + " where FULLORDERNO = @FullOrderNo "
                                    + " and substring(BIGSTATUS,2,1) = 'W' "
                                    + " and EDPNOS_001 <> 0 "
                                    + " UNION all "
                                    + " select EDPNOS_002 EDPNO,ITEMQTYS_002 QTY,  EXTPRICES_002/ITEMQTYS_002 UnitPrice "
                                    + " from " + _sqlAccount + "..ORDERSUBHEAD OSH "
                                    + " where FULLORDERNO = @FullOrderNo "
                                    + " and substring(BIGSTATUS,3,1) = 'W' "
                                    + " and EDPNOS_002 <> 0 "
                                    + " UNION all "
                                    + " select EDPNOS_003 EDPNO,ITEMQTYS_003 QTY,  EXTPRICES_003/ITEMQTYS_003 UnitPrice "
                                    + " from " + _sqlAccount + "..ORDERSUBHEAD OSH "
                                    + " where FULLORDERNO = @FullOrderNo "
                                    + " and substring(BIGSTATUS,4,1) = 'W' "
                                    + " and EDPNOS_003 <> 0"
                                    + " ) E on C.EdpNo = E.EDPNO "
                                    + " where Account = '" + _sqlAccount + "'";


                    AMPServices.Commodity commodity;

                    cn.ConnectionString = _csConnectShip.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    SqlParameter parm = new System.Data.SqlClient.SqlParameter("@FullOrderNo", FullOrderNo);
                    cmd.Parameters.Add(parm);

                    SqlDataReader rd = cmd.ExecuteReader();
                    while (rd.Read())
                    {
                        if (commodities.Count > 4)
                        {
                            break;
                        }
                        commodity = new AMPServices.Commodity();
                        commodities.Add(commodity);
                        commodity.description = rd[rd.GetOrdinal("description")].ToString().Trim();
                        if (commodity.description.Trim() == String.Empty)
                        {
                            commodity.description = rd[rd.GetOrdinal("DESCRIPTION")].ToString().Trim();
                        }

                        commodity.productCode = rd[rd.GetOrdinal("ITEMNO")].ToString().Trim();
                        //***********************************************
                        //quantity
                        //***********************************************
                        commodity.quantity = Int32.Parse(rd[rd.GetOrdinal("QTY")].ToString().Trim()); ;
                        commodity.quantitySpecified = true;

                        //***********************************************
                        //weight
                        //First value from Commodity table is the unit, 
                        //Second is the unit of measure
                        //***********************************************
                        commodity.unitWeight = new AMPServices.Weight();
                        decimal tempDecimal = 0;
                        string sWeight = rd[rd.GetOrdinal("unitWeight")].ToString().Trim();
                        String[] aWeight = sWeight.Split('|');
                        if (!Decimal.TryParse(aWeight[0], out tempDecimal))
                        {
                            tempDecimal = 0;
                        }

                        //If weight is not overwritten from the Commodity Table
                        //Use the value from the Itemmast
                        if (tempDecimal == 0)
                        {
                            string ecometryWeight = rd[rd.GetOrdinal("unitWeight")].ToString().Trim();
                            if (decimal.TryParse(ecometryWeight, out tempDecimal))
                            {
                                tempDecimal = tempDecimal / 1000M;
                            }
                        }

                        //If the value isn't in the Commodity Table or ITEMMASTER then try and calculate it
                        if (tempDecimal == 0)
                        {
                            tempDecimal = weight / commodity.quantity;
                        }


                        commodity.unitWeight.amount = tempDecimal;
                        if (aWeight.Length > 1)
                        {
                            commodity.unitWeight.unit = aWeight[1].Trim();
                        }
                        else
                        {
                            commodity.unitWeight.unit = "LB";
                        }


                        //TODO Fix the following:
                        // 1. Aborts when unitValue is not set
                        // 2. aWeight is mixed in with unit value if (aWeight.Length > 1)

                        //***********************************************
                        //Unit Value
                        //First value from Commodity table is the unit, 
                        //Second is the unit of measure
                        //***********************************************
                        //If not overridden use value from order
                        string sUnitValue = rd[rd.GetOrdinal("unitValue")].ToString();
                        string[] aUnitValue = sUnitValue.Split('|');
                        if (!Decimal.TryParse(aUnitValue[0], out tempDecimal))
                        {
                            tempDecimal = 0;
                        }

                        if (tempDecimal == 0)
                        {
                            tempDecimal = Decimal.Parse(rd[rd.GetOrdinal("unitPrice")].ToString().Trim());
                        }


                        commodity.unitValue = new AMPServices.Money();
                        commodity.unitValue.amount = tempDecimal;

                        //commodity.unitValue.amount = 89.95M;

                        if (aUnitValue.Length > 1)
                        {
                            commodity.unitValue.currency = aUnitValue[1];
                        }
                        else
                        {
                            commodity.unitValue.currency = "USD";
                        }

                        //default from Connectship UPS sampel    
                        commodity.quantityUnitMeasure = rd[rd.GetOrdinal("quantityUnitMeasure")].ToString().Trim();
                        if (commodity.quantityUnitMeasure.Trim() == String.Empty)
                        {
                            commodity.quantityUnitMeasure = "PC";
                        }

                        commodity.harmonizedCode = rd[rd.GetOrdinal("exportHarmonizedCode")].ToString().Trim();

                        commodity.originCountry = rd[rd.GetOrdinal("originCountry")].ToString().Trim();

                    }
                }
            }
            return commodities.ToArray();
        }
        //public AMPServices.Commodity[] GetCommidtyContents(string FullOrderNo)
        //{
        //    List<AMPServices.Commodity> commodities = new List<AMPServices.Commodity>();
        //    AMPServices.Commodity commodity = new AMPServices.Commodity();

        //    //Is defualt commodity information set up
        //    ResultMessage rm = GetConfiguration(_csEcometry.InitialCatalog, "00", "00", "CommodityInfo");
        //    if (rm.Success)
        //    {
        //        string[] stringArray = rm.Message.Split('|');

        //        if (stringArray.Length != 5)
        //        {
        //            throw new Exception("Commodity Info not set up correctly, must be Desc|HarmonizationCode|Country|Weight|Value");
        //        }
        //        commodity.description = stringArray[0];
        //        // commodity.harmonizedCode = stringArray[1];
        //        commodity.originCountry = stringArray[2];

        //        //weight
        //        commodity.unitWeight = new AMPServices.Weight();
        //        decimal tempDecimal;
        //        if (!Decimal.TryParse(stringArray[3], out tempDecimal))
        //        {
        //            tempDecimal = 0;
        //        }
        //        commodity.unitWeight.amount = tempDecimal;
        //        commodity.unitWeight.unit = "lb";

        //        //Value
        //        if (!Decimal.TryParse(stringArray[4], out tempDecimal))
        //        {
        //            tempDecimal = 0;
        //        }
        //        commodity.unitValue = new AMPServices.Money();
        //        commodity.unitValue.amount = tempDecimal;
        //        commodity.unitValue.currency = "USD";
        //        commodity.quantity = 1;
        //        commodity.quantitySpecified = true;
        //        commodity.quantityUnitMeasure = "EA";
        //        commodity.originCountry = "US";

        //        commodities.Add(commodity);
        //        return commodities.ToArray();
        //    }

        //    else
        //    {
        //        return GetOrderCommodities(FullOrderNo);
        //    }

        //}

        private AMPServices.Commodity[] GetOrderCommodities(string FullOrderNo)
        {
            List<AMPServices.Commodity> commodities = new List<AMPServices.Commodity>();
            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT A.*, DESCRIPTION,  SUBSTRING(TICKETNO,5,4) COO, PRICE, WEIGHT "
                    + " FROM "
                    + Environment.NewLine
                    + " ( "
                    + Environment.NewLine
                    + " Select EDPNOS_001 EDPNO, ITEMQTYS_001 QTY from ORDERSUBHEAD where FULLORDERNO = @FullOrderNo and  EDPNOS_001 <> 0 "
                    + Environment.NewLine
                    + " UNION ALL "
                    + " Select EDPNOS_002 EDPNO, ITEMQTYS_002 from ORDERSUBHEAD where FULLORDERNO = @FullOrderNo and EDPNOS_002 <> 0 "
                    + Environment.NewLine
                    + " UNION ALL "
                    + " Select EDPNOS_003 EDPNO, ITEMQTYS_003 from ORDERSUBHEAD where FULLORDERNO = @FullOrderNo and EDPNOS_003 <> 0 "
                    + Environment.NewLine
                    + " ) A "
                    + Environment.NewLine
                    + " JOIN ITEMMAST I ON A.EDPNO = I.EDPNO ";

                    cmd.Parameters.Add("@FullOrderNo", SqlDbType.Char);
                    cmd.Parameters["@FullOrderNo"].Value = FullOrderNo;
                    cn.ConnectionString = _csEcometry.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    using (SqlDataReader rd = cmd.ExecuteReader())
                    {
                        while (rd.Read())
                        {
                            AMPServices.Commodity commodity = new AMPServices.Commodity();
                            commodity.description = rd[rd.GetOrdinal("DESCRIPTION")].ToString();
                            // commodity.harmonizedCode = stringArray[1];
                            string coo = rd[rd.GetOrdinal("COO")].ToString();
                            commodity.originCountry = coo;

                            //weight
                            commodity.unitWeight = new AMPServices.Weight();
                            decimal tempDecimal;
                            if (!Decimal.TryParse(rd[rd.GetOrdinal("WEIGHT")].ToString(), out tempDecimal))
                            {
                                tempDecimal = 0;
                            }
                            commodity.unitWeight.amount = tempDecimal / 10000;
                            commodity.unitWeight.unit = "lb";

                            //Value
                            if (!Decimal.TryParse(rd[rd.GetOrdinal("PRICE")].ToString(), out tempDecimal))
                            {
                                tempDecimal = 0;
                            }
                            commodity.unitValue = new AMPServices.Money();
                            commodity.unitValue.amount = tempDecimal / 100;
                            commodity.unitValue.currency = "USD";
                            commodity.quantity = Convert.ToInt32(rd[rd.GetOrdinal("QTY")]);
                            commodity.quantitySpecified = true;
                            commodity.quantityUnitMeasure = "EA";


                            commodities.Add(commodity);
                        }
                    }
                }
            }
            return commodities.ToArray();
        }

        /// <summary>
        /// Translate Ecometry Country ID to the ISO value</summary>
        /// <param name="InCountry"> Ecometry Country to translate</param>
        /// </summary>         
        public string TranslateCountry(string InCountry)
        {
            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select substring(ADDITIONALDATA,54,2) from COUNTRIES where COUNTRY = @Country";
                    cmd.Parameters.Add("@Country", SqlDbType.Char);
                    cmd.Parameters["@Country"].Value = InCountry;
                    cn.ConnectionString = _csEcometry.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    using (SqlDataReader rd = cmd.ExecuteReader())
                    {
                        if (rd.HasRows)
                        {
                            rd.Read();
                            return rd[0].ToString();  //ISO country abreviation
                        }
                    }
                }
            }
            return "  ";
        }

        /// <summary>
        /// Get Customer Information</summary>
        /// <param name="custEdp"> Customer to retrieve info for</param>
        /// </summary> 
        public Customer GetCustomer(string custEdp)
        {
            Customer customer = new Customer();

            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = " Select  * from CUSTOMERS where CUSTEDP = " + custEdp;
                    cn.ConnectionString = _csEcometry.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    SqlDataReader rd = cmd.ExecuteReader();
                    while (rd.Read())
                    {
                        customer.company = rd[rd.GetOrdinal("NAMEX")].ToString();
                        customer.firstName = rd[rd.GetOrdinal("FNAME")].ToString();
                        customer.lastName = rd[rd.GetOrdinal("LNAME")].ToString();
                        customer.street = rd[rd.GetOrdinal("STREET")].ToString();
                        customer.city = rd[rd.GetOrdinal("CITY")].ToString();
                        customer.state = rd[rd.GetOrdinal("STATE")].ToString();
                        customer.zip = rd[rd.GetOrdinal("ZIP")].ToString();
                        customer.country = rd[rd.GetOrdinal("COUNTRYCODE")].ToString();
                    }
                }
            }
            string defaultPhone = "";
            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = " Select  DAYPHONE, NIGHTPHONE, FAXPHONE from CUSTOMERPHONE where CUSTEDP = " + custEdp;
                    cn.ConnectionString = _csEcometry.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    SqlDataReader rd = cmd.ExecuteReader();
                    ResultMessage rm = GetConfiguration(_csEcometry.InitialCatalog, "00", "00", "DefaultPhone");

                    //Replace hard coded default with configurable value
                    if (rm.Success)
                    {
                        defaultPhone = rm.Message;
                        customer.dayphone = rm.Message;
                        customer.nightphone = rm.Message;
                        customer.faxphone = rm.Message;
                    }
                    else
                    {
                        customer.dayphone = " ";
                        customer.nightphone = " ";
                        customer.faxphone = " ";
                    }

                    while (rd.Read())
                    {
                        customer.dayphone = rd[rd.GetOrdinal("DAYPHONE")].ToString();
                        customer.nightphone = rd[rd.GetOrdinal("NIGHTPHONE")].ToString();
                        customer.faxphone = rd[rd.GetOrdinal("FAXPHONE")].ToString();
                    }

                }
            }

            customer.CleanPhone("", defaultPhone);
            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = " Select  REF1, REF2 FROM CUSTOMERADDL where CUSTEDP = " + custEdp;
                    cn.ConnectionString = _csEcometry.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    SqlDataReader rd = cmd.ExecuteReader();
                    while (rd.Read())
                    {
                        customer.ref1 = rd[0].ToString();
                        customer.ref2 = rd[1].ToString();
                    }
                }
            }


            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = " Select  substring(XREFNO,3,50) EMAIL from CUSTXREF where CUSTEDP = " + custEdp + " AND XREFNO like 'EM%'";
                    cn.ConnectionString = _csEcometry.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    SqlDataReader rd = cmd.ExecuteReader();
                    while (rd.Read())
                    {
                        customer.email = rd[rd.GetOrdinal("EMAIL")].ToString();
                    }
                }
            }

            return customer;
        }

        public string GetDefaultShipMethod(string shipMethod, string account, string company, string division)
        {
            ResultMessage rm = GetConfiguration(account, company, division, "AltSM" + shipMethod);
            return rm.Message;
        }

        public ResultMessage GetRatingShipMethods(string smIn)
        {
            ResultMessage rm = new ResultMessage();
            rm.Success = false;
            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText =
                        " SELECT ConnectShip from Translations where Type = 'Rating' and Ecometry = '" + smIn + "'";

                    cn.ConnectionString = _csConnectShip.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    SqlDataReader rd = cmd.ExecuteReader();
                    if (rd.HasRows)
                    {
                        rm.Messages = new List<string>();
                    }

                    while (rd.Read())
                    {
                        rm.Success = true;
                        rm.Messages.Add(rd[0].ToString());
                    }
                }
            }

            return rm;
        }

        public GetItemsResponce GetOrderCheckData(string fullOrderNo)
        {
            GetItemsResponce oc = new GetItemsResponce();
            oc.items = new List<Item>();

            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText =
                        " SELECT I.EDPNO, ITEMNO, DESCRIPTION, A.QTY, [A].[LINENO], SUBSTRING(FLAGS,7,1) SerialReq "
                        + " FROM "
                        + "("
                        + " Select  EDPNOS_001 EDPNO , ITEMQTYS_001 QTY, LINENOS_001 [LINENO] FROM ORDERSUBHEAD WHERE FULLORDERNO = '" + fullOrderNo + "'"
                        + " AND EDPNOS_001 <> 0 AND SUBSTRING(BIGSTATUS,2,1) NOT IN ('C')"
                        + "UNION ALL"
                        + " Select  EDPNOS_002, ITEMQTYS_002, LINENOS_002 [LINENO] FROM ORDERSUBHEAD WHERE FULLORDERNO = '" + fullOrderNo + "'"
                        + " AND EDPNOS_002 <> 0 AND SUBSTRING(BIGSTATUS,3,1) NOT IN ('C')"
                        + "UNION ALL"
                        + " Select  EDPNOS_003, ITEMQTYS_003, LINENOS_003 [LINENO] FROM ORDERSUBHEAD WHERE FULLORDERNO = '" + fullOrderNo + "'"
                        + " AND EDPNOS_003 <> 0 AND SUBSTRING(BIGSTATUS,4,1) NOT IN ('C')"
                        + ") A"
                        + " JOIN ITEMMAST I ON A.EDPNO = I.EDPNO "
                        + " WHERE SUBSTRING(I.STATUS,1,1) NOT IN ('K') ";


                    cn.ConnectionString = _csEcometry.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    SqlDataReader rd = cmd.ExecuteReader();
                    while (rd.Read())
                    {
                        Item i = new Item();
                        oc.items.Add(i);
                        i.edpNo = Convert.ToInt32(rd[0]);
                        i.ItemNo = rd[1].ToString().Trim();
                        i.Description = rd[2].ToString().Trim();
                        i.Qty = Convert.ToInt32(rd[3]);
                        i.UPC = GetUPC(i.edpNo);
                        i.Alias = GetAlias(i.edpNo);
                        i.lineNo = Convert.ToInt16(rd[4]);
                        i.SerialNumberRequired = false;
                        string serialNumber = rd[rd.GetOrdinal("SerialReq")].ToString();
                        if (serialNumber.ToUpper() == "Y")
                        {
                            i.SerialNumberRequired = true;
                            oc.NeedSerialNumber = true;
                        }
                    }
                }
            }
            return oc;
        }

        public List<string> GetUPC(Int32 edpNO)
        {
            List<string> upc = new List<string>();

            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = " Select SUBSTRING(REFITEMNO,6,20) from EDPITEMXREF where STATUS = 'U' AND EDPNO = " + edpNO.ToString();
                    cn.ConnectionString = _csEcometry.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    SqlDataReader rd = cmd.ExecuteReader();

                    while (rd.Read())
                    {
                        upc.Add(rd[0].ToString().Trim());
                    }
                }
            }
            return upc;
        }


        public List<string> GetAlias(Int32 edpNO)
        {
            List<string> alias = new List<string>();

            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = " Select SUBSTRING(REFITEMNO,6,20) from EDPITEMXREF where STATUS = 'A' AND EDPNO = " + edpNO.ToString();
                    cn.ConnectionString = _csEcometry.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    SqlDataReader rd = cmd.ExecuteReader();

                    while (rd.Read())
                    {
                        alias.Add(rd[0].ToString().Trim());
                    }
                }
            }
            return alias;
        }

        /// <summary>
        /// Retrieve the Password from Ecometry also find the location from MODACCESS</summary>
        /// <param name="userId"> Ecometry User Id</param>
        /// </summary>         
        public string GetPassWord(string userId)
        {
            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = " Select  PASSWORD FROM MODSECURITY WHERE USERNAME  = '" +
                                      userId + "'";
                    cn.ConnectionString = _csEcometry.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    SqlDataReader rd = cmd.ExecuteReader();


                    while (rd.Read())
                    {
                        return rd[0].ToString();
                    }
                    return "Error:UserId does not exist!";
                }
            }
        }

        /// <summary>
        /// GetConfiguration</summary>
        /// <param name="Account">Ecometry Account</param>
        /// <param name="Configuration">Configuration Name</param>
        /// </summary>         
        public ResultMessage GetConfiguration(string Account, string Company, string Division, string Configuration)
        {
            ResultMessage rm = new ResultMessage();

            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " SELECT ValueX from ConfigurationSettings where NameX = '" + Configuration + "'"
                          + " and Account = '" + Account + "' "
                          + " and Company = '" + Company + "' "
                          + " and Division = '" + Division + "'";


                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();

                        if (!rd.HasRows)
                        {
                            if (Division != "00")
                            {
                                Division = "00";
                                return GetConfiguration(Account, Company, Division, Configuration);
                            }

                            if (Company != "00")
                            {
                                Company = "00";
                                return GetConfiguration(Account, Company, Division, Configuration);
                            }
                            if (Account != "GLOBAL")
                            {
                                Account = "GLOBAL";
                                return GetConfiguration(Account, Company, Division, Configuration);
                            }

                            rm.Success = false;
                            rm.Message = "Configuration " + Configuration + ") could not be found!";
                            return rm;
                        }
                        rd.Read();
                        rm.Messages = new List<string>();
                        rm.Messages = rd[0].ToString().Split('|').ToList();

                        rm.Success = true;
                        rm.Message = rd[0].ToString();
                        return rm;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                rm.Success = false;
                rm.Message = ex.ToString();
                return rm;
            }
        }




        /// <summary>
        /// Has the order been Pack Checked </summary>
        /// <param name="Account">Ecometry Account</param>
        /// <param name="fullOrderNo">fullOrderNo</param>
        /// </summary>         
        public ResultMessage IsPackChecked(string Account, string fullOrderNo, string company, string division)
        {

            ResultMessage rm = new ResultMessage();

            try
            {

                //Is pack check neeeded:
                rm = GetConfiguration(Account, company, division, "PackCheck");
                if (rm.Success)
                {
                    if (rm.Message.Substring(0, 1).ToUpper() == "N")
                    {
                        rm.Success = true;
                        rm.Message = "Pack Check not Required";
                        return rm;
                    }
                }
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "select max(substring(BIGSTATUS,1,1)) from ORDERSUBHEAD where FULLORDERNO = '" + fullOrderNo + "'";

                        cn.ConnectionString = _csEcometry.ToString();
                        //cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();
                        //rm.Message = "Not Pack Checked(no OrderSubHead found!";
                        //rm.Success = false;
                        rm = Message("CN011");

                        if (rd.HasRows)
                        {
                            rd.Read();
                            if (rd[0].ToString() == "Z")
                            {
                                rm.Success = true;
                                rm = Message("CN022");
                                rm.Message.Replace("<FULLORDERNO>", fullOrderNo);
                            }
                            else
                            {
                                rm = Message("CN014");
                            }
                        }

                    }
                }
                return rm;
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                rm.Success = false;
                rm.Message = ex.ToString();
                return rm;
            }
        }
        /// <summary>
        /// Retrieve warehouse where Order will be picked from </summary>
        /// <param name="fullOrderNo">fullorderno</param>
        /// </summary>         
        public ResultMessage GetWarehouse(string fullOrderNo)
        {
            ResultMessage rm = new ResultMessage();

            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " SELECT Max(substring(WAREHOUSELOC,1,2)) from PICKLOTDETAIL  "
                        + " where FULLORDERNO = '" + fullOrderNo + "'";


                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();
                        if (rd.HasRows)
                        {
                            rd.Read();
                            rm.Success = true;
                            rm.Message = rd[0].ToString();
                            return rm;
                        }
                    }
                }
                rm = Message("CN032");
                rm.Message = rm.Message.Replace("<FULLORDERNO>", fullOrderNo);
                return rm;

            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                rm.Success = false;
                rm.Message = ex.ToString();
                return rm;
            }
        }
        /// <summary>
        /// Has the order beeh Shipped(Could be shipped in Ecometry or Pending Shipment in ShippingTransactions </summary>
        /// <param name="Account">Ecometry Account</param>
        /// <param name="fullOrderNo">fullOrderNo</param>
        /// </summary>         
        public ResultMessage IsOrderShipped(string Account, string fullOrderNo)
        {
            ResultMessage rm = new ResultMessage();

            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " SELECT  * from ShippingTransactions  where   FullOrderNo = '" + fullOrderNo + "'"
                            + " and Account = '" + Account + "'";


                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();

                        if (rd.HasRows)
                        {
                            rm = Message("CN023");
                            return rm;
                        }
                    }
                }
                rm.Success = true;
                rm.Message = "Success";
                return rm;
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                rm.Success = false;
                rm.Message = ex.ToString();
                return rm;
            }
        }

        /// <summary>
        /// Verify that the Warehouse it related to the ConnectShip Shipper </summary>
        /// <param name="Account">Ecometry Account</param>
        /// <param name="shipper">ConnectShip Shipper</param>
        /// <param name="warehouse"> Ecometry Warehouse where the order is to be shipped from</param>
        /// </summary>         
        public ResultMessage IsCorrectWarehouse(string Account, string shipper, string warehouse)
        {
            ResultMessage rm = new ResultMessage();

            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " SELECT count(*) from Translations  "
                        + " where ConnectShip = '" + shipper + "'"
                        + " and Ecometry = '" + warehouse + "'"
                            + " and Account = '" + Account + "'"
                            + " and UPPER(Type) = 'WAREHOUSE' ";


                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();
                        rd.Read();
                        if ((int)rd[0] == 0)
                        {
                            if (Account != "GLOBAL")
                            {
                                return IsCorrectWarehouse("GLOBAL", shipper, warehouse);
                            }
                            else
                            {
                                rm = Message("CN031");
                                rm.Message = rm.Message.Replace("<SHIPPER>", shipper);
                                rm.Message = rm.Message.Replace("<WAREHOUSE>", warehouse);
                                return rm;
                            }
                        }
                        else
                        {
                            rm.Success = true;
                            rm.Message = "Success";
                            return rm;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                rm.Success = false;
                rm.Message = ex.ToString();
                return rm;
            }
        }



        /// <summary>
        /// Translate From Connectship value returning Ecometry value</summary>
        /// <param name="Account">Ecometry Account</param>
        /// <param name="ConnectShip">ConnectShip Value</param>
        /// <param name="Type">Type of Translation</param>
        /// </summary>         
        public ResultMessage TranslateFromConnectShip(string Account, string ConnectShip, string Type)
        {
            ResultMessage rm = new ResultMessage();

            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {

                        cmd.CommandText = " SELECT Ecometry from Translations where ConnectShip = '" + ConnectShip + "'"
                            + " and Account = '" + Account + "'"
                            + " and Type = '" + Type + "' ";

                        //cn.ConnectionString = _csEcometry.ToString();
                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();

                        if (!rd.HasRows)
                        {
                            if (Account != "GLOBAL")
                            {
                                return TranslateFromConnectShip("GLOBAL", ConnectShip, Type);

                            }
                            //rm.Success = false;
                            //rm.Message = "ConnectShip value(" + ConnectShip + ") could not be Translated!";
                            rm = Message("CN033");
                            rm.Message = rm.Message.Replace("<CONNNECTSHIP", ConnectShip);
                            return rm;
                        }
                        rd.Read();
                        rm.Messages = new List<string>();
                        rm.Messages = rd[0].ToString().Split('|').ToList();

                        rm.Success = true;
                        rm.Message = "Success";
                        return rm;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                rm.Success = false;
                rm.Message = ex.ToString();
                return rm;
            }
        }

        /// <summary>
        /// Translate From Ecometry value returning Connectship value</summary>
        /// <param name="Account">Ecometry Account</param>
        /// <param name="Ecometry">Ecometry Value</param>
        /// <param name="Type">Type of Translation</param>
        /// </summary>         
        public ResultMessage TranslateFromEcometry(string Account, string Ecometry, string Type)
        {
            ResultMessage rm = new ResultMessage();

            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        //cmd.CommandText = " Select  DESCX FROM CODEDESC WHERE CODEKEY = 'T1" + shipMethod + "'";
                        cmd.CommandText = " SELECT ConnectShip from Translations where Ecometry = '" + Ecometry + "'"
                            + " and Account = '" + Account + "'"
                            + " and Type = '" + Type + "' ";

                        //cn.ConnectionString = _csEcometry.ToString();
                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();

                        if (!rd.HasRows)
                        {
                            if (Account != "GLOBAL")
                            {
                                return TranslateFromEcometry("GLOBAL", Ecometry, Type);

                            }
                            //rm.Success = false;
                            //rm.Message = "Ecometry Ship method(" + Ecometry + ") could not be Translated!";
                            rm.Success = false;
                            rm = Message("CN034");
                            rm.Message = rm.Message.Replace("<ECOMETRY>", Ecometry);
                            return rm;
                        }
                        rd.Read();
                        rm.Messages = new List<string>();
                        rm.Messages = rd[0].ToString().Split('|').ToList();

                        rm.Success = true;
                        rm.Message = rd[0].ToString();
                        return rm;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                rm.Success = false;
                rm.Message = ex.ToString();
                return rm;
            }
        }

        /// <summary>
        /// Translate Ecometry ship method to Connectship, first try countryspecific lookup, then try lookup with no country code
        /// </summary>
        /// <param name="Account"></param>
        /// <param name="ShipMethod"></param>
        /// <param name="Country"></param>
        /// <returns></returns>
        public ResultMessage TranslateShipMethod(string Account, string ShipMethod, string Country = " ")
        {
            ResultMessage rm = new ResultMessage();

            //First try and find Country specific translation
            rm = TranslateShipMethodInternal(Account, ShipMethod.Trim() + Country.Trim());
            if (rm.Success == false)
            {
                //This time try without a country;
                rm = TranslateShipMethodInternal(Account, ShipMethod.Trim());
            }
            return rm;
        }

        //jtr 5/20/14 Renamed from TranslateShipMethod to TranslateShipMethodInternal to allow for country specific lookup
        /// <summary>
        /// This routine will first try and find a translation based on specific account, if not found the account will change to Global and 
        /// the search will repeat itself.
        /// </summary>
        /// <param name="Account">Specific Account to look for</param>
        /// <param name="shipMethod">Ecometry Ship Method </param>
        /// <returns></returns>
        public ResultMessage TranslateShipMethodInternal(string Account, string shipMethod)
        {
            ResultMessage rm = new ResultMessage();

            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        //cmd.CommandText = " Select  DESCX FROM CODEDESC WHERE CODEKEY = 'T1" + shipMethod + "'";
                        cmd.CommandText = " SELECT ConnectShip from Translations where Ecometry = '" + shipMethod + "'"
                            + " and Account = '" + Account + "'"
                            + " and Type = 'ShipMethod' ";

                        //cn.ConnectionString = _csEcometry.ToString();
                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();

                        if (!rd.HasRows)
                        {
                            if (Account != "GLOBAL")
                            {
                                return TranslateShipMethodInternal("GLOBAL", shipMethod);

                            }
                            rm = Message("CN005");
                            rm.Message = rm.Message.Replace("<ShipMethod>", shipMethod);
                            return rm;
                        }
                        rd.Read();
                        rm.Messages = new List<string>();
                        rm.Messages = rd[0].ToString().Split('|').ToList();

                        if (rm.Messages.Count != 3)
                        {
                            rm = Message("CN006");
                            rm.Message = rm.Message.Replace("<shipMethod>", shipMethod);
                            return rm;
                        }

                        rm.Success = true;
                        rm.Message = "Success";
                        return rm;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                rm.Success = false;
                rm.Message = ex.ToString();
                return rm;
            }
        }


        /// <summary>
        /// Get get users location from MODACCESS, Connectship shipper id is stored in MODACCESS</summary>
        /// <param name="userId"> Ecometry User Id</param>
        /// </summary>         
        public ResultMessage GetShipper(string userId)
        {
            ResultMessage rm = new ResultMessage();

            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " Select  * FROM MODACCESS WHERE USERID  = '" +
                                          userId + "'";
                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();

                        if (!rd.HasRows)
                        {
                            rm.Success = false;
                            rm.Message = "MODACCESS: User Not found!";
                            return rm;
                        }
                        rm.Messages = new List<string>();
                        while (rd.Read())
                        {
                            for (int i = 0; i < 40; i++)
                            {
                                rm.Messages.Add(rd[i + 2].ToString());
                            }
                        }

                        rm.Success = true;
                        rm.Message = "Success";
                        return rm;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                rm.Success = false;
                rm.Message = ex.ToString();
                return rm;
            }


        }

        /// <summary>
        /// Get shipmethod on order</summary>
        /// <param name="fullOrderId"> Ecometry 12 character Order Number</param>
        /// </summary>         
        public string GetShipMethod(string fullOrderNo)
        {

            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " Select  distinct SHIPMETHOD  FROM ORDERSUBHEAD WHERE FULLORDERNO  = '" +
                                          fullOrderNo + "'";
                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();

                        if (rd.HasRows)
                        {
                            rd.Read();
                            return rd[0].ToString();
                        }

                        return "";
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                return "Failure:" + ex.ToString();
            }
        }

        /// <summary>
        /// Verify that the order number exists and is in the warehouse</summary>
        /// <param name="fullOrderNo"> Ecometry 12 character Order Number</param>
        /// </summary>         
        public ResultMessage ValidateFullOrderNo(string fullOrderNo, out string company, out string division)
        {
            ResultMessage rm = new ResultMessage();
            company = "";
            division = "";
            try
            {


                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = " Select  BIGSTATUS, DATEX, COMPANY, DIVISION from FINANCIALORDER WHERE FULLORDERNO = '" +
                                          fullOrderNo + "'";
                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();


                        if (!rd.HasRows)
                        {

                            rm = Message("CN012");
                            rm.Message = rm.Message.Replace("<FULLORDERNO>", fullOrderNo);
                            return rm;
                        }
                        while (rd.Read())
                        {
                            company = rd[rd.GetOrdinal("COMPANY")].ToString();
                            division = rd[rd.GetOrdinal("DIVISION")].ToString();
                            if (rd[0].ToString().CompareTo("N 11") >= 0 && rd[0].ToString().CompareTo("N 19") <= 0)
                            {
                                rm = Message("CN013");
                                rm.Message = rm.Message.Replace("<DATE>", rd[rd.GetOrdinal("DATEX")].ToString());
                                return rm;
                            }
                            if (rd[0].ToString().CompareTo("P 11") >= 0 && rd[0].ToString().CompareTo("P 19") <= 0)
                            {
                                rm = Message("CN013");
                                rm.Message = rm.Message.Replace("<DATE>", rd[rd.GetOrdinal("DATEX")].ToString());
                                return rm;
                            }

                            if (rd[0].ToString().CompareTo("  11") >= 0 && rd[0].ToString().CompareTo("  19") <= 0)
                            {

                                rm.Success = true;
                                rm.Message = "Order is Valid";
                                return rm;
                            }
                        }


                        rm = Message("CN015");
                        rm.Message = rm.Message.Replace("<FULLORDERNO>", fullOrderNo);
                        return rm;

                    }
                }

            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                rm.Success = false;
                rm.Message = ex.ToString();
                return rm;
            }
        }
        /// <summary>
        /// Retrieve orders in the warehouse for each location </summary>
        /// <param name="shippId"> Connectship shipper Id tied to the user</param>
        /// </summary>         
        public ResultMessage GetOrdersInWarehouse(string shipperId)
        {
            ResultMessage rm = new ResultMessage();
            rm.Messages = new List<string>();
            try
            {
                //string strSql =
                //    "Select * "
                //    + "FROM  "
                //    + "( "
                //    + "SELECT  "
                //    + "OSH.FULLORDERNO, "
                //    + "I.ITEMNO, "
                //    + "I.DESCRIPTION, "
                //    + "I.STATUS, "
                //    + "OSH.ITEMQTYS_001 "
                //    + "FROM PROCESSING P "
                //    + "JOIN ORDERSUBHEAD OSH ON P.FULLORDERNO = OSH.FULLORDERNO "
                //    + "JOIN ITEMMAST I ON OSH.EDPNOS_001 = I.EDPNO "
                //    + "WHERE ACTIONSEARCH IN ('P7','C9','PX','PN') "
                //    + "AND EDPNOS_001 <> 0 "
                //    + "AND SUBSTRING(OSH.BIGSTATUS,2,1) <> 'C' "
                //    + "UNION ALL "
                //    + "SELECT  "
                //    + "OSH.FULLORDERNO, "
                //    + "I.ITEMNO, "
                //    + "I.DESCRIPTION, "
                //    + "I.STATUS, "
                //    + "OSH.ITEMQTYS_002 "
                //    + "FROM PROCESSING P "
                //    + "JOIN ORDERSUBHEAD OSH ON P.FULLORDERNO = OSH.FULLORDERNO "
                //    + "JOIN ITEMMAST I ON OSH.EDPNOS_002 = I.EDPNO "
                //    + "WHERE ACTIONSEARCH IN ('P7','C9','PX','PN') "
                //    + "AND EDPNOS_002 <> 0 "
                //    + "AND SUBSTRING(OSH.BIGSTATUS,3,1) <> 'C' "
                //    + "UNION ALL "
                //    + "SELECT  "
                //    + "OSH.FULLORDERNO, "
                //    + "I.ITEMNO, "
                //    + "I.DESCRIPTION, "
                //    + "I.STATUS, "
                //    + "OSH.ITEMQTYS_003 "
                //    + "FROM PROCESSING P "
                //    + "JOIN ORDERSUBHEAD OSH ON P.FULLORDERNO = OSH.FULLORDERNO "
                //    + "JOIN ITEMMAST I ON OSH.EDPNOS_003 = I.EDPNO "
                //    + "WHERE ACTIONSEARCH IN ('P7','C9','PX','PN') "
                //    + "AND EDPNOS_003 <> 0 "
                //    + "AND SUBSTRING(OSH.BIGSTATUS,4,1) <> 'C' "
                //    + ") A "
                //    + "ORDER BY FULLORDERNO, ITEMNO ";

                string strSql = "Select distinct OSH.FULLORDERNO, substring(WAREVEND,3,2) "
     + "from " + _sqlAccount.Trim() + "..ORDERSUBHEAD as OSH  "
     + "join " + _sqlAccount.Trim() + "..PICKLOTDETAIL as PLD  "
     + "on OSH.FULLORDERNO = PLD.FULLORDERNO  "
     + "left join Translations T1 on substring(PLD.WAREVEND,3,2) = T1.Ecometry and T1.Type = 'Warehouse' and T1.Account = 'SUPPLE' "
     + "left join Translations T2 on substring(PLD.WAREVEND,3,2) = T2.Ecometry and T2.Type = 'Warehouse' and T2.Account = 'GLOBAL' "
     + "where  substring(OSH.BIGSTATUS,1,1) in ('W','Z')";
                if (shipperId.Trim() != string.Empty)
                {
                    strSql += "and isNull(T1.ConnectShip, T2.ConnectShip) = '" + shipperId + "' ";
                }

                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = strSql;
                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();
                        while (rd.Read())
                        {
                            //string result = rd[0].ToString().Trim() + "|"
                            //    + rd[1].ToString().Trim()
                            //    + "|" + rd[2].ToString().Trim()
                            //    + "|" + rd[3].ToString().Trim()
                            //    + "|" + rd[4].ToString().Trim();
                            string result = rd[0].ToString();
                            rm.Messages.Add(result);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                rm.Success = false;
                rm.Message = ex.ToString();
                return rm;
            }
            rm.Success = true;
            rm.Message = "Success";
            return rm;
        }

        /// <summary>
        /// Retrieve orders in the warehouse for each location </summary>
        /// <param name="shippId"> Connectship shipper Id tied to the user</param>
        /// </summary>         
        public ResultMessage AddShippingTransaction(ShippingTransactionRequest str)
        {

            ResultMessage rm = new ResultMessage();
            rm.Messages = new List<string>();
            try
            {
                string strSql = "insert into ShippingTransactions "
                    + "Values("
                    //Account
                    + "'" + str.Account + "',"
                    //Carrier
                 + "'" + str.Carrier + "',"

                    //Shipper
                    + "'" + str.Shipper + "',"
                    + "'" + str.Document + "',"

                    //ShipFile
                    + str.ShipFile + "'',"
                    //Msn
                    + str.Msn.ToString() + ","
                    //Fullorderno
                + "'" + str.FullOrderno + "',"
                    //Weight
                + str.weight.amount.ToString() + ","

                    //TrackingNo
                    + "'" + str.TrackingNo + "',"
                    //Carrier
                    + "'" + str.Service + "',"
                    //EcometryShipmethod
                    + "'" + str.EcometryShipMethod + "',"
                    //Cost
                    + str.ShippingCost.amount + ","
                    //ShipDate
                    + "'" + str.ShipDate.ToString("yyyyMMdd") + "',"
                    //CreateDate
                    + "GETDATE(), "
                    //StatusDate
                    + "GETDATE(), "
                    //Status
                    + "'Pending',"
                    + "'" + str.BarCode + "',"
                    + "'" + str.BarCode2 + "',"
                    + "'" + str.BarCode3 + "'"
                    + ")";

                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = strSql;
                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        int result = cmd.ExecuteNonQuery();
                        if (result < 1)
                        {
                            throw new Exception("Insert into Shipping Ttransactions error");
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }
            rm.Success = true;
            rm.Message = "Success";
            return rm;

        }

        public ResultMessage AddShippingTransactionBackup(ShippingTransactionRequest str)
        {

            ResultMessage rm = new ResultMessage();
            rm.Messages = new List<string>();
            try
            {
                string strSql = "insert into ShippingTransactionsBackup "
                    + "Values("
                    //Account
                    + "@Account,"
                    //Carrier
                 + "@Carrier,"

                    //Shipper
                    + "@Shipper,"
                    + "@Document,"

                    //ShipFile
                    + "@ShipFile,"
                    //Msn
                    + "@Msn,"
                    //Fullorderno
                + "@FullOrderNo,"
                    //Weight
                + "@Weight,"

                    //TrackingNo
                    + "@TrackingNo,"
                    //Carrier
                    + "@Service,"
                    //EcometryShipmethod
                    + "@ShipMethod,"
                    //Cost
                    + "@Cost,"
                    //ShipDate
                    + "@ShipDate,"
                    //CreateDate
                    + "GETDATE(), "
                    //StatusDate
                    + "GETDATE(), "
                    //Status
                    + "'Pending',"
                    + "@BarCode,"
                    + "@BarCode2,"
                    + "@BarCode3"
                    + ")";

                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Parameters.Add("@Account", SqlDbType.Char);
                        cmd.Parameters["@Account"].Value = str.Account;

                        cmd.Parameters.Add("@Carrier", SqlDbType.Char);
                        cmd.Parameters["@Carrier"].Value = str.Carrier;

                        cmd.Parameters.Add("@Shipper", SqlDbType.Char);
                        cmd.Parameters["@Shipper"].Value = str.Shipper;

                        cmd.Parameters.Add("@Document", SqlDbType.Char);
                        cmd.Parameters["@Document"].Value = str.Document;

                        str.ShipFile = str.ShipFile == null ? " " : str.ShipFile;
                        cmd.Parameters.Add("@ShipFile", SqlDbType.Char);
                        cmd.Parameters["@ShipFile"].Value = str.ShipFile;

                        cmd.Parameters.Add("@Msn", SqlDbType.BigInt);
                        cmd.Parameters["@Msn"].Value = str.Msn;

                        cmd.Parameters.Add("@FullOrderNo", SqlDbType.Char);
                        cmd.Parameters["@FullOrderNo"].Value = str.FullOrderno;

                        cmd.Parameters.Add("@Weight", SqlDbType.Char);
                        cmd.Parameters["@Weight"].Value = str.weight.amount.ToString();

                        cmd.Parameters.Add("@TrackingNo", SqlDbType.Char);
                        cmd.Parameters["@TrackingNo"].Value = str.TrackingNo;

                        cmd.Parameters.Add("@Service", SqlDbType.Char);
                        cmd.Parameters["@Service"].Value = str.Service;

                        cmd.Parameters.Add("@ShipMethod", SqlDbType.Char);
                        cmd.Parameters["@ShipMethod"].Value = str.EcometryShipMethod;

                        cmd.Parameters.Add("@Cost", SqlDbType.Money);
                        cmd.Parameters["@Cost"].Value = str.ShippingCost.amount;

                        cmd.Parameters.Add("@ShipDate", SqlDbType.Char);
                        cmd.Parameters["@ShipDate"].Value = str.ShipDate.ToString("yyyyMMdd");

                        str.BarCode2 = str.BarCode == null ? " " : str.BarCode;
                        cmd.Parameters.Add("@BarCode", SqlDbType.Char);
                        cmd.Parameters["@BarCode"].Value = str.BarCode;

                        str.BarCode2 = str.BarCode2 == null ? " " : str.BarCode2;
                        cmd.Parameters.Add("@BarCode2", SqlDbType.Char);
                        cmd.Parameters["@BarCode2"].Value = str.BarCode2;

                        str.BarCode3 = str.BarCode3 == null ? " " : str.BarCode3;
                        cmd.Parameters.Add("@BarCode3", SqlDbType.Char);
                        cmd.Parameters["@BarCode3"].Value = str.BarCode3;


                        cmd.CommandText = strSql;
                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        int result = cmd.ExecuteNonQuery();
                        if (result < 1)
                        {
                            throw new Exception("Insert into Shipping Ttransactions error");
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }
            rm.Success = true;
            rm.Message = "Success";
            return rm;
        }

        public Boolean ValidateShippingTransaction(Int64 msn)
        {
            try
            {
                using (System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection())
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
                {
                    cmd.Parameters.Add("@Msn", SqlDbType.BigInt);
                    cmd.Parameters["@Msn"].Value = msn;
                    cmd.CommandText = "select count(*) from ShippingTransactions where Msn = @Msn ";
                    cn.ConnectionString = _csConnectShip.ToString();
                    cn.Open();
                    cmd.Connection = cn;

                    int result = (int)cmd.ExecuteScalar();
                    if (result < 1)
                    {
                        throw new Exception("ShippingTransaction can not be verified");
                    }

                }

                using (System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection())
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
                {
                    cmd.Parameters.Add("@Msn", SqlDbType.BigInt);
                    cmd.Parameters["@Msn"].Value = msn;
                    cmd.CommandText = "select count(*) from ShippingTransactionsBackup where Msn = @Msn ";
                    cn.ConnectionString = _csConnectShip.ToString();
                    cn.Open();
                    cmd.Connection = cn;

                    int result = (int)cmd.ExecuteScalar();
                    if (result < 1)
                    {
                        throw new Exception("ShippingTransaction can not be verified");
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }

            return true;
        }
        private ShipFinancials getShipFinancials(string fullOrderNo)
        {
            ShipFinancials shipFinancials = new ShipFinancials();
            shipFinancials.FullOrderNo = "NotFound";

            try
            {
                string strSql =
                    "SELECT AMOUNTS_001 Postage, AMOUNTS_002 Tax, AMOUNTS_003 ProductDollars, "
                    + "AMOUNTS_004 Cost, AMOUNTS_005 Credits, "
                + "(AMOUNTS_001 + AMOUNTS_002 + AMOUNTS_004 - AMOUNTS_005) Total "
                    + "FROM FINANCIALORDER "
                    + "WHERE FULLORDERNO = '" + fullOrderNo + "'"
                    + " AND BIGSTATUS BETWEEN '  11' AND '  19'";

                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = strSql;
                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();
                        while (rd.Read())
                        {
                            shipFinancials.FullOrderNo = fullOrderNo;
                            shipFinancials.Cost = Convert.ToInt64(rd[rd.GetOrdinal("Cost")]);
                            shipFinancials.Credits = Convert.ToInt64(rd[rd.GetOrdinal("Credits")]);
                            shipFinancials.PostageHandling = Convert.ToInt64(rd[rd.GetOrdinal("Postage")]);
                            shipFinancials.Tax = Convert.ToInt64(rd[rd.GetOrdinal("Tax")]);
                            shipFinancials.ProductDollars = Convert.ToInt64(rd[rd.GetOrdinal("ProductDollars")]);
                            shipFinancials.Total = Convert.ToInt64(rd[rd.GetOrdinal("Total")]);

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }




            return shipFinancials;
        }


        private List<string> GetOrderComments(string fullOrderNo)
        {
            ShipFinancials shipFinancials = new ShipFinancials();
            shipFinancials.FullOrderNo = "NotFound";

            try
            {
                List<string> comments = new List<string>();
                string strSql =
                    "SELECT COMMENTX "
                    + "FROM ORDERCOMMENTS "
                    + "WHERE FULLORDERNO = '" + fullOrderNo.Substring(0, 8) + "0000" + "'"
                   + " AND SUBSTRING(PRINTON,1,1) ='Y' "
                + " UNION ALL "
                + "SELECT COMMENTX "
                + "FROM ORDERCOMMENTS "
                + "WHERE FULLORDERNO = '" + fullOrderNo + "'"
                + " AND SUBSTRING(PRINTON,1,1)= 'Y' ";

                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = strSql;
                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();
                        while (rd.Read())
                        {
                            comments.Add(rd[0].ToString());

                        }
                    }
                }

                return comments;
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }

        }

        public ResultMessage DeletePackage(string account, string msn)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        cmd.CommandText = "Delete from ShippingTransactions where Account = '" + account + "' and Msn = " + msn;
                        cmd.ExecuteNonQuery();
                    }
                }

                ResultMessage rm = new ResultMessage();
                rm.Success = true;
                rm.Message = "Success";
                return rm;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OrderItem> GetCollateItems(string fullOrderNo, out string ShipMethod)
        {
            List<OrderItem> orderItems = new List<OrderItem>();

            try
            {
                ShipMethod = "Undefined";
                string strSql =
                    " SELECT ITEMNO, DESCRIPTION, EXTPRICES_001, ITEMQTYS_001, WAREHOUSELOC, ISNULL(CD.ABRV, 'Undefined:' + PD.SHIPMETHOD)  "
                    + " ,A.EDPNO, LINENOS_001 "
                    + " FROM "
                    + " ( "
                    + " SELECT EDPNOS_001 EDPNO , EXTPRICES_001, ITEMQTYS_001, PICKLOT, LINENOS_001 "
                    + " FROM ORDERSUBHEAD OSH "
                    + " WHERE FULLORDERNO = '" + fullOrderNo + "'"
                    + " AND SUBSTRING(OSH.BIGSTATUS,2,1) IN ('W','Z') "
                    + " UNION ALL "
                    + " SELECT EDPNOS_002, EXTPRICES_002, ITEMQTYS_002, PICKLOT, LINENOS_002 "
                    + " FROM ORDERSUBHEAD OSH "
                    + " WHERE FULLORDERNO = '" + fullOrderNo + "'"
                    + " AND SUBSTRING(OSH.BIGSTATUS,3,1) IN ('W','Z') "
                    + " UNION ALL "
                    + " SELECT EDPNOS_003, EXTPRICES_003, ITEMQTYS_003, PICKLOT, LINENOS_003 "
                    + " FROM ORDERSUBHEAD OSH "
                    + " WHERE FULLORDERNO = '" + fullOrderNo + "'"
                    + " AND SUBSTRING(OSH.BIGSTATUS,4,1) IN ('W','Z') "
                    + " ) A"
                    + " JOIN PICKLOTDETAIL PD ON A.PICKLOT = PD.LOTNO AND A.EDPNO = PD.EDPNO AND A.LINENOS_001 = PD.ITMLINENO"
                + " LEFT JOIN CODEDESC CD ON 'SM' + PD.SHIPMETHOD = CD.CODEKEY ";

                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = strSql;
                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();
                        while (rd.Read())
                        {
                            OrderItem oi = new OrderItem();
                            oi.Description = rd[1].ToString();
                            oi.ItemNo = rd[0].ToString();
                            oi.price = Convert.ToInt64(rd[2]);
                            oi.qty = Convert.ToInt64(rd[3]);
                            oi.WarehouseLoc = rd[4].ToString();
                            oi.EdpNo = rd[6].ToString();
                            oi.LineNo = rd[7].ToString();
                            orderItems.Add(oi);
                            ShipMethod = rd[5].ToString();
                        }
                    }
                }

                return orderItems;
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }


        }



        public CollateResponce GetCollate(string fullOrderno)
        {
            CollateResponce collateResponce = new CollateResponce();
            ResultMessage rm = new ResultMessage();
            rm.Success = true;
            rm.Message = "Success";
            try
            {
                string buyerCustEdp = GetBuyer(fullOrderno);
                collateResponce.BillTo = GetCustomer(buyerCustEdp);
                collateResponce.BillTo.CleanPhone("", "9999999999");

                collateResponce.ShipTo = GetShipShipTo(fullOrderno);
                collateResponce.ShipTo.CleanPhone("", "9999999999");

                ShipFinancials shipFinancials = getShipFinancials(fullOrderno);
                collateResponce.Postage = shipFinancials.PostageHandling.ToString();
                collateResponce.Tax = shipFinancials.Tax.ToString();
                collateResponce.ProductDollars = shipFinancials.ProductDollars.ToString();
                collateResponce.Credits = shipFinancials.Credits.ToString();
                collateResponce.Total = shipFinancials.Total.ToString();
                string shipMethod;
                collateResponce.OrderItems = GetCollateItems(fullOrderno, out shipMethod);
                collateResponce.ShipMethod = shipMethod;

                collateResponce.Comments = GetOrderComments(fullOrderno);
                collateResponce.EntryDate = GetEntryDate(fullOrderno);


            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }

            collateResponce.SetMessage(rm);

            return collateResponce;

        }

        public bool DoesPickTicketExist(string FullOrderNo)
        {
            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cn.ConnectionString = _csEcometry.ToString();
                    cmd.Connection = cn;
                    cmd.CommandText = "select count(*) from ORDERXREF where FULLORDERNO = '" + FullOrderNo + "' and XREFNO like 'PT%'";
                    cn.Open();
                    Int64 count = Convert.ToInt64(cmd.ExecuteScalar());
                    if (count == 0)
                    { return false; }
                    else
                    { return true; }
                }
            }
        }

        public Boolean CreateBackorder(BackorderRequest br, string warehouse)
        {
            string entryDate = "";
            try
            {
                string pickTicket = br.FullOrderNo.Substring(0, 10);
                //SC9001440071T01584260101 2013040102                  B 


                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        //create pick ticket
                        bool result = DoesPickTicketExist(br.FullOrderNo);
                        if (!result)
                        {

                            cmd.CommandText = " Insert into ORDERXREF Values ( "
                                             + "'" + br.FullOrderNo + "' "
                                             + ",'" + "PT" + pickTicket + "' "
                                             + ",'PT'"
                                             + ",'" + pickTicket + "' "
                                             + " )";

                            cmd.ExecuteNonQuery();
                        }

                        string sql = "";


                        //Insert fmsmgg backorder data
                        sql = "Insert into FMSMSG  Values ";
                        foreach (var item in br.BackorderItems)
                        {
                            //Example SS9001440071T01584260101 000010000000000000001000303667 
                            //Insert fmsmgg backorder data
                            sql = "Insert into FMSMSG  Values ";
                            sql += " ( "
                                + "'SS','N','" + DateTime.Now.ToString("yyyyMMdd") + "',"
                                //1	Record ID	Value ‘SS’		X(2)	1
                                + "'SS"
                                //2	Pickticket Number	From pickticket download		X(10)	3
                                + pickTicket.PadRight(10, ' ')
                                //3	Ecometry Full Order Number	From pickticket download		X(13)	13
                                + br.FullOrderNo.PadRight(13, ' ')
                                //4	Order Line Number Shorted	From pickticket download		9(5)	26
                                + item.LineNo.Trim().PadLeft(5, '0')
                                //5	Actual Packed Quantity			9(8)	34 
                                + item.PackedQty.PadLeft(8, '0')
                                //6	Quantity from pickticket	From pickticket download		9(8)	36
                                + item.OriginalQty.PadLeft(8, '0')
                                //7	Ecometry Item EDP Number	Nine digit, zero filled on the left.		9(9)	45
                                + item.EdpNo.PadLeft(9, '0') + "'); " + Environment.NewLine;

                            cmd.CommandText = sql;
                            cmd.ExecuteNonQuery();

                        }

                        sql = "Insert into FMSMSG  Values ";
                        sql += " ("
                            + "'SC','N','" + DateTime.Now.ToString("yyyyMMdd") + "',"
                            //Record ID	Value ‘SC’		X(2)	1
                            + " 'SC"
                            //Pickticket Number	From pickticket download		X(10)	3
                            + pickTicket.PadRight(10, ' ')
                            //Ecometry Full Order Number	From pickticket download		X(13)	13
                            + br.FullOrderNo.PadRight(13, ' ')
                            //Date Shipped	00000000		X(8)	26
                            + DateTime.Now.ToString("yyyyMMdd")
                            //Ecometry 2 digit Ship Method			X(2)	34
                            + "02".PadRight(2, ' ')
                            //Shipping Weight	Shipping Weight		9(5)V9(4)	36
                            + " ".PadLeft(9, ' ')
                            //Shipping Charges	Shipping Charges		9(7)V99	45
                            + " ".PadLeft(9, ' ')
                            //Ship conf header flag	“B” for WBO by line		X(1)	54
                            //"C" for WBO by level - header record only.	
                            + "B'"
                            + " ) ";


                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();

                        //cmd.CommandText = "SELECT SCOPE_IDENTITY() ";
                        //Int64 SlotId = Convert.ToInt64(cmd.ExecuteScalar());

                        //cmd.CommandText = "Select PROCESS_SW from FMSMSG where SLOTID = " + SlotId.ToString();
                        //string process_SW;
                        //for (int i = 0; i < 5; i++)
                        //{
                        //    process_SW = cmd.ExecuteScalar().ToString();
                        //    if (process_SW == "Y")
                        //    {
                        //        break;
                        //    }
                        //    System.Threading.Thread.Sleep(500);
                        //}
                    }
                }


                return true;

            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }

        }
        private string GetEntryDate(string fullOrderNo)
        {
            string entryDate = "";
            try
            {
                fullOrderNo = fullOrderNo.Substring(0, 8) + "0000";
                string strSql =
                    " SELECT ENTRYDATE  "
                    + " FROM  ORDERHEADER "
                    + " WHERE FULLORDERNO = '" + fullOrderNo + "'";


                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = strSql;
                        cn.ConnectionString = _csEcometry.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();
                        while (rd.Read())
                        {
                            entryDate = rd[0].ToString();
                        }
                    }
                }


                return entryDate;
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }
        }

        public Box GetBoxSize(string fullOrderNo, string account)
        {
            Box result = null;

            try
            {

                //05/04/14 jtr set a default box size 
                string boxSize1 = GetDefualtBoxId(account);
                if (!boxSize1.Equals(string.Empty))
                {
                    result = new Box();
                    Boolean answer1 = IsBoxOverriden(account, boxSize1, out result.Length, out result.Width, out result.depth);
                    if (answer1)
                    {
                        return result;
                    }
                }

                string strSql =
                    " select FULLORDERNO, P.COMPANY, BOXID, LENGTHX, WIDTH, DEPTH "
                    + " from PICKLOTDETAIL P "
                    + " LEFT JOIN BOXTBL B ON SUBSTRING(P.MISCDATA40,15,4) = B.BOXID and (P.COMPANY = B.COMPANY) AND (P.DIVISION = B.DIVISION) "
                    + " WHERE P.FULLORDERNO = @FullOrderNo ";
                using (SqlConnection cn = new SqlConnection())
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = strSql;
                    cn.ConnectionString = _csEcometry.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@FullOrderNo", SqlDbType.Char);
                    cmd.Parameters["@FullOrderNo"].Value = fullOrderNo;

                    using (SqlDataReader rd = cmd.ExecuteReader())
                    {
                        if (rd.HasRows)
                        {
                            rd.Read();

                            result = new Box();
                            //jtr check for defualt box size
                            string boxSize = rd[2].ToString();
                            Boolean answer = IsBoxOverriden(account, boxSize, out result.Length, out result.Width, out result.depth);
                            if (answer)
                            {
                                return result;
                            }


                            Int64 tempIntDepth;
                            if (!Int64.TryParse(rd[5].ToString(), out tempIntDepth))
                            {
                                tempIntDepth = 0;
                            }


                            Int64 tempIntWidth;
                            if (!Int64.TryParse(rd[4].ToString(), out tempIntWidth))
                            {
                                tempIntWidth = 0;
                            }


                            Int64 tempIntLength;
                            if (!Int64.TryParse(rd[3].ToString(), out tempIntLength))
                            {
                                tempIntLength = 0;
                            }


                            result.depth = tempIntDepth / 100M;
                            result.Width = tempIntWidth / 100M;
                            result.Length = tempIntLength / 100M;
                            return result;
                        }
                    }
                }


                // Division == 00
                strSql =
               " select FULLORDERNO, BOXID, LENGTHX, WIDTH, DEPTH "
               + " from PICKLOTDETAIL P "
               + " JOIN BOXTBL B ON SUBSTRING(P.MISCDATA40,15,4) = B.BOXID and (P.COMPANY = B.COMPANY) "
               + " WHERE P.FULLORDERNO = @FullOrderNo and B.COMPANY = '01' and DIVISION = '00' ";
                using (SqlConnection cn = new SqlConnection())
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = strSql;
                    cn.ConnectionString = _csEcometry.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@FullOrderNo", SqlDbType.Char);
                    cmd.Parameters["@FullOrderNo"].Value = fullOrderNo;
                    using (SqlDataReader rd = cmd.ExecuteReader())
                    {
                        if (rd.HasRows)
                        {
                            result = new Box();
                            //jtr check for defualt box size
                            string boxSize = rd[2].ToString();
                            Boolean answer = IsBoxOverriden(account, boxSize, out result.Length, out result.Width, out result.depth);
                            if (answer)
                            {
                                return result;
                            }

                            Int64 tempIntDepth;
                            if (!Int64.TryParse(rd[3].ToString(), out tempIntDepth))
                            {
                                tempIntDepth = 0;
                            }


                            Int64 tempIntWidth;
                            if (!Int64.TryParse(rd[4].ToString(), out tempIntWidth))
                            {
                                tempIntWidth = 0;
                            }


                            Int64 tempIntLength;
                            if (!Int64.TryParse(rd[5].ToString(), out tempIntLength))
                            {
                                tempIntLength = 0;
                            }


                            result.depth = tempIntDepth / 100M;
                            result.Width = tempIntWidth / 100M;
                            result.Length = tempIntLength / 100M;
                            return result;
                        }
                    }
                }

                //Company and Division doesn't matter
                strSql =
                    " select FULLORDERNO, BOXID, LENGTHX, WIDTH, DEPTH "
                    + " from PICKLOTDETAIL P "
                    + " JOIN BOXTBL B ON SUBSTRING(P.MISCDATA40,15,4) = B.BOXID "
                    + " WHERE P.FULLORDERNO = @FullOrderNo ";
                using (SqlConnection cn = new SqlConnection())
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = strSql;
                    cn.ConnectionString = _csEcometry.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    cmd.Parameters.Add("@FullOrderNo", SqlDbType.Char);
                    cmd.Parameters["@FullOrderNo"].Value = fullOrderNo;
                    using (SqlDataReader rd = cmd.ExecuteReader())
                    {
                        if (rd.HasRows)
                        {
                            result = new Box();
                            result = new Box();
                            //jtr check for defualt box size
                            string boxSize = rd[2].ToString();
                            Boolean answer = IsBoxOverriden(account, boxSize, out result.Length, out result.Width, out result.depth);
                            if (answer)
                            {
                                return result;
                            }
                            Int64 tempIntDepth;
                            if (!Int64.TryParse(rd[3].ToString(), out tempIntDepth))
                            {
                                tempIntDepth = 0;
                            }


                            Int64 tempIntWidth;
                            if (!Int64.TryParse(rd[4].ToString(), out tempIntWidth))
                            {
                                tempIntWidth = 0;
                            }


                            Int64 tempIntLength;
                            if (!Int64.TryParse(rd[5].ToString(), out tempIntLength))
                            {
                                tempIntLength = 0;
                            }


                            result.depth = tempIntDepth / 100M;
                            result.Width = tempIntWidth / 100M;
                            result.Length = tempIntLength / 100M;
                            return result;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }


            return result;
        }

        public Int32 CheckSerialData(string fullOrderNo)
        {
            string strSql =
            " Select Count(*) "
            + " From SerialNumbers "
            + " where FullOrderNo = @FullOrderNo ";

            using (SqlConnection cn = new SqlConnection())
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSql;
                cn.ConnectionString = _csConnectShip.ToString();
                cn.Open();
                cmd.Connection = cn;

                cmd.Parameters.Add("@FullOrderNo", SqlDbType.Char);
                cmd.Parameters["@FullOrderNo"].Value = fullOrderNo;


                int rows = Convert.ToInt32(cmd.ExecuteScalar());
                return rows;
            }

            return 0;
        }

        public Int32 CheckSerialData(SerialNumber sn, string fullOrderNo)
        {
            string strSql =
            " Select Count(*) "
            + " From SerialNumbers "
            + " where FullOrderNo = @FullOrderNo "
            + " and Line = @LineNo "
            + " and SerialNumber = @SerialNumber ";

            using (SqlConnection cn = new SqlConnection())
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSql;
                cn.ConnectionString = _csConnectShip.ToString();
                cn.Open();
                cmd.Connection = cn;

                cmd.Parameters.Add("@FullOrderNo", SqlDbType.Char);
                cmd.Parameters["@FullOrderNo"].Value = fullOrderNo;

                cmd.Parameters.Add("@LineNo", SqlDbType.Int);
                cmd.Parameters["@LineNo"].Value = sn.LineNo;

                cmd.Parameters.Add("@SerialNumber", SqlDbType.Char);
                cmd.Parameters["@SerialNumber"].Value = sn.SerialNo;

                int rows = Convert.ToInt32(cmd.ExecuteScalar());
                return rows;
            }

            return 0;
        }


        public void DeleteExtraTrackingNumber(string account, string fullOrderNo)
        {

            string strSql =
            " DELETE from ORDERACTIONS "
           + " where FULLORDERNO = '" + fullOrderNo + "'"
           + " and MESSAGECD = 'TRACK'";

            using (SqlConnection cn = new SqlConnection())
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSql;
                cn.ConnectionString = _csEcometry.ToString();
                cn.Open();
                cmd.Connection = cn;

                int rows = cmd.ExecuteNonQuery();
            }
        }

        public Decimal isNullMoney(ConnectShipWCFServer.AMPServices.Money moneyIn)
        {
            Decimal tempDecimal = moneyIn == null ? 0 : moneyIn.amount;

            return tempDecimal;

        }

        public Decimal isNullWeight(ConnectShipWCFServer.AMPServices.Weight weightIn)
        {
            Decimal tempDecimal = weightIn == null ? 0 : weightIn.amount;

            return tempDecimal;

        }


        public String isNullString(Object objectIn)
        {
            String tempString = objectIn == null ? " " : objectIn.ToString();

            return tempString;

        }

        public void PutShippinglData(string account, string fullOrderNo, string type, ConnectShipWCFServer.AMPServices.ShipResponse shipResponse, string service, Int64 shippingMSN)
        {

            string strSql =
            " INSERT INTO ShippingData "
            + " Values ( "
            + System.Environment.NewLine
                //[Account]
             + "'" + account + "'"
             + System.Environment.NewLine
                //     ,[Msn]
             + ", " + shipResponse.result.packageResults[0].resultData.msn.ToString()
             + System.Environment.NewLine
                //     ,[FullOrderNo]
             + ",'" + fullOrderNo + "'"
             + System.Environment.NewLine
                // ,[Shipper]
             + ", '" + shipResponse.result.packageResults[0].resultData.shipper + "'"
             + System.Environment.NewLine
                //,[Service]
                //+ ", " + shipResponse.result.packageResults[0].resultData.service + "'"+ System.Environment.NewLine
              + ",'" + service + "'"
              + System.Environment.NewLine
                //,[Type]
                + ",'" + type + "'";

            //,[ShipFile]
            //+ ", ' ' ";
            strSql += System.Environment.NewLine
                //,[AdditionalHandlingFee]
            + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.additionalHandlingFee)
            + System.Environment.NewLine
                //,[AirFreightFee]
            + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.airFreightFee)
            + System.Environment.NewLine
                //,[ApportionedBase]
            + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.apportionedBase)
            + System.Environment.NewLine
                //,[ApportionedDiscount]
            + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.apportionedDiscount)
            + System.Environment.NewLine
                //,[ApportionedSpecial]
            + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.apportionedSpecial)
            + System.Environment.NewLine
                //,[ApportionedTotal]
            + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.apportionedTotal);
            strSql += System.Environment.NewLine
                //,[BillingFee]
             + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.billingFee);
            strSql += System.Environment.NewLine
                //,[BorderProcessFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.borderProcessingFee);
            strSql += System.Environment.NewLine
                //,[CallTagFee]
            + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.calltagFee);
            strSql += System.Environment.NewLine
                //,[CargonNeutralFee]
            + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.carbonNeutralFee);
            strSql += System.Environment.NewLine
                //,[CarrierCostSurcharge]
            + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.carrierCostSurcharge);
            strSql += System.Environment.NewLine
                //,[CarrierMonitoringFee]
            + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.carrierMonitoringFee);
            strSql += System.Environment.NewLine
                //,[CarrierName]
                //TODO Remove CarrierName
                //,[CertifiedMailFee]
            + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.certifiedMailFee);
            strSql += System.Environment.NewLine
                //,[ChainOfSignatureFee]
            + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.chainOfSignatureFee);
            strSql += System.Environment.NewLine
                //,[CodAmount]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.codAmount);
            strSql += System.Environment.NewLine
                //,[CodFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.codFee);
            strSql += System.Environment.NewLine
                //,[ConsigneeThirdPartyBilling]
           + ", '" + isNullString(shipResponse.result.packageResults[0].resultData.consigneeThirdPartyBilling) + "'";
            strSql += System.Environment.NewLine
                //,[ConsingeeThirdPartyBillingAccount]
           + ", '" + isNullString(shipResponse.result.packageResults[0].resultData.consigneeThirdPartyBillingAccount) + "'";
            strSql += System.Environment.NewLine
                //,[DeclarefValueAmount]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.declaredValueAmount);
            strSql += System.Environment.NewLine
                //,[DeclaredValueCustoms]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.declaredValueCustoms);
            strSql += System.Environment.NewLine
                //,[DeclaredValueFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.declaredValueFee);
            strSql += System.Environment.NewLine
                //,[DeliveryWindowFee]
            + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.deliveryWindowFee);
            strSql += System.Environment.NewLine
                //,[DirectDeliveryFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.directDeliveryFee);
            strSql += System.Environment.NewLine
                //,[Discount]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.discount);
            strSql += System.Environment.NewLine
                //,[DocumentationFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.documentationFee)
           + System.Environment.NewLine
                //,[DryIceFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.dryIceFee);
            strSql += System.Environment.NewLine
                //,[DutyFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.dutyFee);
            strSql += System.Environment.NewLine
                //,[EveningDeliveryFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.eveningDeliveryFee);
            strSql += System.Environment.NewLine
                //,[ExtendedAreaFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.extendedAreaFee);
            strSql += System.Environment.NewLine
                //,[ForkLiftDeliveryFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.forkliftDeliveryFee);
            strSql += System.Environment.NewLine
                //,[FuelSurcharge]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.fuelSurcharge);
            strSql += System.Environment.NewLine
                //,[HazMatFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.hazmatFee);
            strSql += System.Environment.NewLine
                //,[HealthInsuranceFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.healthInsuranceFee);
            strSql += System.Environment.NewLine
                //,[HelperDeliveryFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.helperDeliveryFee);
            strSql += System.Environment.NewLine
                //,[HelperPickupFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.helperPickupFee);

            strSql += System.Environment.NewLine
                //,[HoldAtLocationFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.holdAtLocationFee);
            strSql += System.Environment.NewLine
                //,[HolidayDeliveryFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.holidayDeliveryFee);
            strSql += System.Environment.NewLine
                //,[HolidayPickupFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.holidayPickupFee);
            strSql += System.Environment.NewLine
                //,[IMPBNonComplieanceFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.impbNonComplianceFee);
            strSql += System.Environment.NewLine
                //,[InsideDeliveryFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.insideDeliveryFee);
            strSql += System.Environment.NewLine
                //,[InsidePickupFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.insideDeliveryFee);
            strSql += System.Environment.NewLine
                //,[InvoiceDiscount]
            + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.invoiceDiscount);
            strSql += System.Environment.NewLine
                //,[InvoiceFreight]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.invoiceFreight);
            strSql += System.Environment.NewLine
                //,[InvoiceInsurance]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.invoiceInsuranceFee);
            strSql += System.Environment.NewLine
                //,[InvoiceOtherFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.invoiceOtherFee);
            strSql += System.Environment.NewLine
                //,[LargePackageFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.largePackageFee);
            strSql += System.Environment.NewLine
                //,[LiftGateDeliveryFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.liftgateDeliveryFee);
            strSql += System.Environment.NewLine
                //,[LiftGatePickupFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.liftgatePickupFee);
            strSql += System.Environment.NewLine
                //,[MultiPieceFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.multiPieceFee);
            strSql += System.Environment.NewLine
                //,[NeutralDeliveryServiceFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.neutralDeliveryServiceFee);
            strSql += System.Environment.NewLine
                //,[NonMachineableMailFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.nonmachinableMailFee);
            strSql += System.Environment.NewLine
                //,[NonStandardMailFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.nonstandardMailFee)
           + System.Environment.NewLine
                //,[OffShoreFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.offshoreFee)
           + System.Environment.NewLine
                //,[OverSizeFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.oversizeFee)
           + System.Environment.NewLine
                //,[OverDimensionFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.overDimensionFee)
           + System.Environment.NewLine
                //,[PalletFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.palletFee)
           + System.Environment.NewLine
                //,[PalletJackPickupFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.palletJackDeliveryFee)
           + System.Environment.NewLine
                //,[ParcelAirLiftFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.parcelAirliftFee)
           + System.Environment.NewLine
                //,[PieceCountFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.pieceCountFee)
           + System.Environment.NewLine
                //,[PriorDeliviveryFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.priorDeliveryNotificationFee)
           + System.Environment.NewLine
                //,[PRoactiveRecoveryFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.proactiveRecoveryFee)
           + System.Environment.NewLine
                //,[Proof]
           + ", '" + isNullString(shipResponse.result.packageResults[0].resultData.proof) + "'"
           + System.Environment.NewLine
                //,[ProofFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.proofFee)
           + System.Environment.NewLine
                //,[ProofRequiredSignature]
           + ", '" + isNullString(shipResponse.result.packageResults[0].resultData.proofRequireSignature) + "'"
           + System.Environment.NewLine
                //,[ProofRequiredSigjnatureAdult]
           + ", '" + isNullString(shipResponse.result.packageResults[0].resultData.proofRequireSignatureAdult) + "'"
           + System.Environment.NewLine
                //,[ProofRequiredSignatureConsinee]
           + ", '" + isNullString(shipResponse.result.packageResults[0].resultData.proofRequireSignatureConsignee) + "'"
           + System.Environment.NewLine
                //,[ProofRequiredSignatureFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.proofRequireSignatureFee)
           + System.Environment.NewLine
                //,[ProofOfDocuments]
           + ", '" + isNullString(shipResponse.result.packageResults[0].resultData.proofReturnOfDocuments) + "'"
           + System.Environment.NewLine
                //,[ProofSignatureWeaver]
           + ", '" + isNullString(shipResponse.result.packageResults[0].resultData.proofSignatureWaiver) + "'"
           + System.Environment.NewLine
                //,[RatedWeight]
           + ", " + isNullWeight(shipResponse.result.packageResults[0].resultData.ratedWeight).ToString()
           + System.Environment.NewLine
                //,[RegisteredMailFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.registeredMailFee)
           + System.Environment.NewLine
                //,[RemoteOriginFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.remoteOriginFee)
           + System.Environment.NewLine
                //,[ResidentialDeliveryFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.residentialDeliveryFee)
           + System.Environment.NewLine
                //,[ReturnDeliveryFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.returnDeliveryFee)
           + System.Environment.NewLine
                //,[ReturnNotificationFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.returnDeliveryNotificationFee)
           + System.Environment.NewLine
                //,[SaturdayDeliveryFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.saturdayDeliveryFee)
           + System.Environment.NewLine
                //,[SecurityFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.securityFee)
           + System.Environment.NewLine
                //,[ShipNotificationFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.shipNotificationFee)
           + System.Environment.NewLine
                //,[Special]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.special)
           + System.Environment.NewLine
                //,[SpecialDeliveryFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.specialDeliveryFee)
           + System.Environment.NewLine
                //,[StairDeliveryFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.stairDeliveryFee)
           + System.Environment.NewLine
                //,[StairPickupFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.stairPickupFee)
           + System.Environment.NewLine
                //,[SundayDeliveryFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.sundayDeliveryFee)
           + System.Environment.NewLine
                //,[SundayPickupFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.stairPickupFee)
           + System.Environment.NewLine
                //,[Tax]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.tax)
           + System.Environment.NewLine
                //,[TempatureControlFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.temperatureControlFee)
           + System.Environment.NewLine
                //,[TerminalHandlingFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.terminalHandlingFee)
           + System.Environment.NewLine
                //,[Terms]
           + ", '" + isNullString(shipResponse.result.packageResults[0].resultData.terms) + "'"
           + System.Environment.NewLine
                //,[ThirdParyBilling]
           + ", '" + isNullString(shipResponse.result.packageResults[0].resultData.thirdPartyBilling) + "'"
             + System.Environment.NewLine
                //,[ThirdPartyBillingAccount]
           + ", '" + isNullString(shipResponse.result.packageResults[0].resultData.thirdPartyBillingAccount) + "'"
           + System.Environment.NewLine
                //,[Total]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.total)
           + System.Environment.NewLine
                //,[TrackingNumber]
           + ", '" + isNullString(shipResponse.result.packageResults[0].resultData.trackingNumber) + "'"
           + System.Environment.NewLine

           + ", '" + isNullString(shipResponse.result.packageResults[0].resultData.trackingNumber2) + "'"
           + System.Environment.NewLine
                //,[UnPackFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.unpackFee)
           + System.Environment.NewLine
                //,[UrbanDeliveryFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.urbanDeliveryFee)
           + System.Environment.NewLine
                //,[Weight]
           + ", " + isNullWeight(shipResponse.result.packageResults[0].resultData.weight)
           + System.Environment.NewLine
                //,[WharfageFee]
           + ", " + isNullMoney(shipResponse.result.packageResults[0].resultData.wharfageFee)
           + System.Environment.NewLine
           //ShippingMSN
            + ", " + shippingMSN
            + System.Environment.NewLine
           //Zone
           + ", '" + isNullString(shipResponse.result.packageResults[0].resultData.zone + "'")
            + System.Environment.NewLine
                 + ")";

            using (SqlConnection cn = new SqlConnection())
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSql;
                cn.ConnectionString = _csConnectShip.ToString();
                cn.Open();
                cmd.Connection = cn;

                int rows = cmd.ExecuteNonQuery();
            }
        }

        //public NameAddress GetVendorNA(string VendorNo, out string BillingAccount)
        //{
        //    BillingAccount = string.Empty;
        //    using (SqlConnection cn = new SqlConnection())
        //    {
        //        using (SqlCommand cmd = new SqlCommand())
        //        {
        //            cmd.CommandText = " Select  * from VENDORDFROM where VENDORNO = '" + VendorNo.Trim() + "'";
        //            cn.ConnectionString = _csEcometry.ToString();
        //            cn.Open();
        //            cmd.Connection = cn;
        //            SqlDataReader rd = cmd.ExecuteReader();
        //            while (rd.Read())
        //            {
        //                NameAddress na = new NameAddress();
        //                na.address1 = rd[rd.GetOrdinal("REF2")].ToString(); 
        //                na.address2 = rd[rd.GetOrdinal("STREET")].ToString(); 
        //                na.city = "CITY";
        //                na.company = rd[rd.GetOrdinal("NAMEX")].ToString();
        //                na.countryCode = VendorNo.Substring(0,2);
        //                na.contact = rd[rd.GetOrdinal("STREET")].ToString();
        //                na.stateProvince = rd[rd.GetOrdinal("STATE")].ToString();
        //                na.postalCode = rd[rd.GetOrdinal("ZIP")].ToString();
        //                BillingAccount = rd[rd.GetOrdinal("REF1")].ToString();
        //            }
        //        }
        //    }

        //    return null;
        //}
        public void PutSerialData(string account, SerialNumber sn, string fullOrderNo)
        {
            string strSql =
            " INSERT INTO SerialNumbers "
            + " Values ( "
            + "'" + account + "'"
            + ",'" + fullOrderNo + "'"
            + ", " + sn.LineNo
            + ", " + sn.EdpNo
            + ", '" + sn.SerialNo + "'"
            + ")";

            using (SqlConnection cn = new SqlConnection())
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = strSql;
                cn.ConnectionString = _csConnectShip.ToString();
                cn.Open();
                cmd.Connection = cn;

                int rows = cmd.ExecuteNonQuery();
            }
        }

        private string GetDefualtBoxId(string account)
        {
            string boxId = string.Empty;
            ResultMessage rm = GetConfiguration(account, "00", "00", "DefaultBox");
            if (rm.Success)
            {
                boxId = rm.Message;
            }
            return boxId;
        }

        private Boolean IsBoxOverriden(string account, string box, out decimal length, out decimal width, out decimal depth)
        {
            ResultMessage rm = GetConfiguration(account, "00", "00", "BOX:" + box.Trim());
            if (rm.Success == true)
            {
                string[] dimensions = rm.Message.Split('|');
                if (dimensions.Length > 2)
                {
                    decimal tempDecimal;
                    if (!Decimal.TryParse(dimensions[0], out tempDecimal))
                    {
                        length = 0;
                        width = 0;
                        depth = 0;
                        return false;
                    }

                    length = tempDecimal;

                    if (!Decimal.TryParse(dimensions[1], out tempDecimal))
                    {
                        length = 0;
                        width = 0;
                        depth = 0;
                        return false;
                    }
                    width = tempDecimal;
                    if (!Decimal.TryParse(dimensions[2], out tempDecimal))
                    {
                        length = 0;
                        width = 0;
                        depth = 0;
                        return false;
                    }
                    depth = tempDecimal;

                    return true;
                }
            }

            length = 0;
            width = 0;
            depth = 0;
            return false;
        }

        private string FormatDate(string dateIn)
        {
            return dateIn.Substring(4, 2) + "/" + dateIn.Substring(6, 2) + "/" + dateIn.Substring(2, 2);
        }
    }
}
