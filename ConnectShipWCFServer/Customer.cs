﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServiceLib
{
    public class  Customer
    {
        public string company { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string ref1 { get; set; }
        public string ref2 { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
        public string email { get; set; }
        public string dayphone { get; set; }
        public string nightphone { get; set; }
        public string faxphone { get; set; }
        

        public Customer()
        {
            company = "";
            firstName = "";
            lastName = "";
            ref1 = "";
            ref2 = "";
            street = "";
            city = "";
            state = "";
            zip = "";
            country = "";
            email = "";
            dayphone = "";
            nightphone = "";
            faxphone = "";

        }
        public void CleanPhone(string inPhone, string  outPhone)
        {
            if (dayphone.Trim() == inPhone)
            {
                dayphone = nightphone;
            }
            if (nightphone.Trim() == inPhone)
            {
                nightphone = outPhone;

                if (dayphone.Trim() == inPhone)
                {
                    dayphone = outPhone;
                }
            }
            if (faxphone.Trim() == inPhone)
            {
                faxphone = outPhone;
            }

        }


        
    }
}
