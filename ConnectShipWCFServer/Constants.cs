﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Constants
/// </summary>
public class Constants
{
    // Ensures consistency in the namespace declarations across services
    public const string Namespace = "Backend";
}