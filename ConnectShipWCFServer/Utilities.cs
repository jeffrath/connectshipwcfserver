﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConnectShipWCFServer
{
    class Utilities
    {
        static public string Encrypt(char[] password)
        {
            //char *pass_pntr;
            char[] pass_pntr = new char[9];
            //   char encryptedpass[9], encryptedpassback[9], XXXX[100];
            char[] encryptedpass = new char[9];
            char[] encryptedpassback = new char[9];
            int key = 0;
            int idx;
            int chr;
            int rlen;
            int len = 0;

            //   cTrace("ENCRYPT - Enter: ", "1");
            //   memset(encryptedpass, 0, 9);
            //   memset(encryptedpassback, ' ', 9);
            for (int i = 0; i < 9; i++)
            {
                encryptedpass[i] = ' ';
                encryptedpassback[i] = ' ';
            }
            //encryptedpassback[8] = (char)0;

            password.CopyTo(pass_pntr, 0);
            //   //sprintf(XXXX,"pass_pntr = %s", pass_pntr);
            //   //cTrace("ENCRYPT: ", XXXX);
            len = 8;
            len = 0;
            rlen = 0;

            while (rlen < 8 && (int)pass_pntr[rlen] != 32)
            {
                rlen++;
                //pass_pntr++;
            }


            password.CopyTo(pass_pntr, 0);

            idx = 0;
            while (idx < 8)
            {
                if (idx == 0)
                    key = 12;
                if (idx == 1)
                    key = -7;
                if (idx == 2)
                    key = 9;
                if (idx == 3)
                    key = 15;
                if (idx == 4)
                    key = 2;
                if (idx == 5)
                    key = -5;
                if (idx == 6)
                    key = -11;
                if (idx == 7)
                    key = 6;

                chr = (int)pass_pntr[idx];

                chr = chr + key + rlen;

                if (chr <= 32)
                {
                    chr = chr + 48;
                }
                if (chr >= 126)
                {
                    chr = chr - 45;
                }

                encryptedpass[idx] = (char)chr;
                idx++;
                //pass_pntr++;
            }

            //   //sprintf(XXXX,"encryptedpass = %s", encryptedpass);
            //   //cTrace("ENCRYPT: ", XXXX);
            if (rlen == 2)
            {
                encryptedpassback[7] = encryptedpass[7];
                encryptedpassback[6] = encryptedpass[6];
                encryptedpassback[5] = encryptedpass[5];
                encryptedpassback[4] = encryptedpass[4];
                encryptedpassback[3] = encryptedpass[3];
                encryptedpassback[2] = encryptedpass[2];
                encryptedpassback[1] = encryptedpass[0];
                encryptedpassback[0] = encryptedpass[1];
            }
            if (rlen == 3)
            {
                encryptedpassback[7] = encryptedpass[7];
                encryptedpassback[6] = encryptedpass[6];
                encryptedpassback[5] = encryptedpass[5];
                encryptedpassback[4] = encryptedpass[4];
                encryptedpassback[3] = encryptedpass[3];
                encryptedpassback[2] = encryptedpass[0];
                encryptedpassback[0] = encryptedpass[2];
            }
            if (rlen == 4)
            {
                encryptedpassback[7] = encryptedpass[7];
                encryptedpassback[6] = encryptedpass[6];
                encryptedpassback[5] = encryptedpass[5];
                encryptedpassback[4] = encryptedpass[4];
                encryptedpassback[3] = encryptedpass[0];
                encryptedpassback[2] = encryptedpass[1];
                encryptedpassback[1] = encryptedpass[2];
                encryptedpassback[0] = encryptedpass[3];
            }
            if (rlen == 5)
            {
                encryptedpassback[7] = encryptedpass[7];
                encryptedpassback[6] = encryptedpass[6];
                encryptedpassback[5] = encryptedpass[5];
                encryptedpassback[4] = encryptedpass[0];
                encryptedpassback[3] = encryptedpass[1];
                encryptedpassback[1] = encryptedpass[3];
                encryptedpassback[0] = encryptedpass[4];
            }
            if (rlen == 6)
            {
                encryptedpassback[7] = encryptedpass[7];
                encryptedpassback[6] = encryptedpass[6];
                encryptedpassback[5] = encryptedpass[0];
                encryptedpassback[4] = encryptedpass[1];
                encryptedpassback[3] = encryptedpass[2];
                encryptedpassback[2] = encryptedpass[3];
                encryptedpassback[1] = encryptedpass[4];
                encryptedpassback[0] = encryptedpass[5];
            }
            if (rlen == 7)
            {
                encryptedpassback[7] = encryptedpass[7];
                encryptedpassback[6] = encryptedpass[0];
                encryptedpassback[5] = encryptedpass[1];
                encryptedpassback[4] = encryptedpass[2];
                encryptedpassback[2] = encryptedpass[4];
                encryptedpassback[1] = encryptedpass[5];
                encryptedpassback[0] = encryptedpass[6];
            }
            if (rlen == 8)
            {
                encryptedpassback[7] = encryptedpass[0];
                encryptedpassback[6] = encryptedpass[1];
                encryptedpassback[5] = encryptedpass[2];
                encryptedpassback[4] = encryptedpass[3];
                encryptedpassback[3] = encryptedpass[4];
                encryptedpassback[2] = encryptedpass[5];
                encryptedpassback[1] = encryptedpass[6];
                encryptedpassback[0] = encryptedpass[7];
            }

            //encryptedpassback[8] = (char)0;
            //encryptedpass[8] = (char)0;
            string returnstr = new string(encryptedpassback);

            return returnstr;

        }
    }
}
