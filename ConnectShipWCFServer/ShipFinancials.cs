﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConnectShipWCFServer
{
    class ShipFinancials
    {
        public string FullOrderNo;
        public Int64 ProductDollars ;
        public Int64 Credits;
        public Int64 Cost;
        public Int64 Tax;
        public Int64 PostageHandling;
        public Int64 Total;
   }
}
