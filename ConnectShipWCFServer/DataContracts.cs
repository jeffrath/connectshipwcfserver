﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConnectShipWCFServer.AMPServices;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace ServiceLib
{

    [DataContract]
    public class GetItemsRequest
    {
        [DataMember]
        public string Shipper { get; set; }
        [DataMember]
        public string Account { get; set; }
        [DataMember]
        public String OrderNumber { get; set; }
    }

    [DataContract]
    public class GetItemsResponce
    {
        [DataMember]
        public bool NeedSerialNumber;

        [DataMember]
        public List<Item> items { get; set; }

        [DataMember]
        public GetBOItemsResponce BoResponce;

        [DataMember]
        public Boolean Success { get; set; }

        [DataMember]
        public string Message { get; set; }

        public void SetMessage(ResultMessage rm)
        {
            Success = rm.Success;
            Message = rm.Message;
        }
    }

    [DataContract]
    public class Item
    {
        [DataMember]
        public string ItemNo { get; set; }

        [DataMember]
        public int lineNo { get; set; }

        [DataMember]
        public int edpNo { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public Int64 Qty { get; set; }

        [DataMember]
        public List<string> UPC { get; set; }

        [DataMember]
        public List<string> Alias { get; set; }

        [DataMember]
        public bool SerialNumberRequired;


    }

    [DataContract]
    public class ShipAPackage
    {
        [DataMember]
        public Boolean Success { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public List<string> Messages { get; set; }
        [DataMember]
        public byte[][] binaryMessages { get; set; }

        public void SetMessage(ResultMessage rm)
        {
            Success = rm.Success;
            Message = rm.Message;
            Messages = rm.Messages;
        }
    }

    [DataContract]
    public class ResultMessage
    {
        [DataMember]
        public Identity[] Result { get; set; }

        [DataMember]
        public Boolean Success { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public List<string> Messages { get; set; }
        [DataMember]
        public byte[][] binaryMessages { get; set; }
    }
    [DataContract]
    public class GetBOItemsRequest
    {

        [DataMember]
        public string Account { get; set; }

        [DataMember]
        public string FullOrderNo { get; set; }
    }
    [DataContract]
    public class GetBOItemsResponce
    {

        [DataMember]
        public Boolean Success { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public List<Item> Items { get; set; }
    }

    [DataContract]
    public class CollateRequest
    {

        [DataMember]
        public string Account { get; set; }

        [DataMember]
        public string Shipper { get; set; }

        [DataMember]
        public string FullOrderNo { get; set; }
    }


    [DataContract]
    public class CollateResponce
    {
        [DataMember]
        public Boolean Success { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string Account { get; set; }

        [DataMember]
        public Customer BillTo { get; set; }


        [DataMember]
        public Customer ShipTo { get; set; }


        [DataMember]
        public string EntryDate { get; set; }


        [DataMember]
        public string OrderNumber { get; set; }

        [DataMember]
        public string ShipMethod { get; set; }


        [DataMember]
        public string Postage { get; set; }

        [DataMember]
        public string Tax { get; set; }

        [DataMember]
        public string ProductDollars { get; set; }

        [DataMember]
        public string Credits { get; set; }

        [DataMember]
        public string Total { get; set; }

        [DataMember]
        public List<OrderItem> OrderItems { get; set; }

        [DataMember]
        public List<string> Comments { get; set; }


        public void SetMessage(ResultMessage rm)
        {
            Success = rm.Success;
            Message = rm.Message;

        }
    }

    [DataContract]
    public class OrderItem
    {
        [DataMember]
        public String ItemNo { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public Int64 qty { get; set; }

        [DataMember]
        public Int64 price { get; set; }

        [DataMember]
        public string WarehouseLoc { get; set; }
        [DataMember]
        public string LineNo { get; set; }
        [DataMember]
        public string EdpNo { get; set; }
    }

    [DataContract]
    public class BackorderRequest
    {
        [DataMember]
        public string Shipper;
        [DataMember]
        public string SqlAccount;
        [DataMember]
        public string FullOrderNo;
        [DataMember]
        public List<BackorderItem> BackorderItems;
        [DataMember]
        public List<SerialNumber> SerialNumbers;
    }

    [DataContract]
    public class BackorderResponce
    {
        [DataMember]
        public bool Success;
        [DataMember]
        public string Message;

        public void SetMessage(ResultMessage rm)
        {
            Success = rm.Success;
            Message = rm.Message;
        }
    }

    [DataContract]
    public class BackorderItem
    {
        [DataMember]
        public string EdpNo;
        [DataMember]
        public string LineNo;
        [DataMember]
        public string PackedQty;
        [DataMember]
        public string OriginalQty;
    }

    [DataContract]
    public class VoidShipmentRequest
    {
        [DataMember]
        public string Account;
        [DataMember]
        public string Shipper;
        [DataMember]
        public string OrderNo;
        [DataMember]
        public string TrackingNo;
    }
    [DataContract]
    public class VoidShipmentResponse
    {

        [DataMember]
        public Boolean Success;
        [DataMember]
        public string Message;

        public void SetMessage(ResultMessage rm)
        {
            Success = rm.Success;
            Message = rm.Message;
        }
    }

    [DataContract]
    public class ProductReviewRequest
    {

        [DataMember]
        public string Account;

        [DataMember]
        public string Criteria;
    }

    [DataContract]
    public class ProductReviewResponse
    {
        [DataMember]
        public string ItemNo;

        [DataMember]
        public string ItemDescription;

        [DataMember]
        public string CreateDate;

        [DataMember]
        public string ItemStatus;


        [DataMember]
        public string SubWith;

        [DataMember]
        public string SubWithDescription;

        [DataMember]
        public string HowManyOfItem;

        [DataMember]
        public string HowManyOfItemDescription;

        [DataMember]
        public int HowManyOfQty;



        [DataMember]
        public Decimal Price;

        [DataMember]
        public int MinOrderQty;

        [DataMember]
        public int MaxOrderQty;

        [DataMember]
        public int ReOrder;

        [DataMember]
        public decimal Weight;

        [DataMember]
        public decimal Height;

        [DataMember]
        public decimal Width;

        [DataMember]
        public decimal Cost;

        [DataMember]
        public decimal AveCost;

        [DataMember]
        public decimal Depth;

        [DataMember]
        public List<Kit> KitComponents;

        [DataMember]
        public List<Kit> MemberOfKit;

        [DataMember]
        public List<WarehouseLocation> Locations;

        [DataMember]
        public int PhysicalInventory;

        [DataMember]
        public int InProcessInventory;

        [DataMember]
        public int Backorders;

        [DataMember]
        public int CurrentlyAvailable;

        [DataMember]
        public List<InventoryDetail> InventoryDetails;

        [DataMember]
        public List<InventoryTrans> InventoryTrans;

        [DataMember]
        public List<PoData> POData;


        [DataMember]
        List<Sale> Sales;

        [DataMember]
        public Boolean Success;
        [DataMember]
        public string Message;

        public void SetMessage(ResultMessage rm)
        {
            Success = rm.Success;
            Message = rm.Message;
        }
    }

    [DataContract]
    public class KeyWordSearchRequest
    {
        [DataMember]
        public string Account;

        [DataMember]
        public string Criteria;
    }

    [DataContract]
    public class KeyWordSearchResponce
    {
        [DataMember]
        public List<ItemDescription> ItemDescriptions;

        [DataMember]
        public Boolean Success;

        [DataMember]
        public string Message;

        public void SetMessage(ResultMessage rm)
        {
            Success = rm.Success;
            Message = rm.Message;
        }
    }


    [DataContract]
    public class ItemDescription
    {
        [DataMember]
        public string ItemNo;
        [DataMember]
        public string ItemDesc;
        [DataMember]
        public string ItemStatus;
        [DataMember]
        public decimal Price;
        [DataMember]
        public Int64 AvailableInv;


    }


    [DataContract]
    public class PoData
    {
        [DataMember]
        public string PurchaseOrderNumber;
        [DataMember]
        public string VendorNumber;
        [DataMember]
        public string VendorName;
        [DataMember]
        public string IssueDate;
        [DataMember]
        public string DueDate;
        [DataMember]
        public string Reason;
        [DataMember]
        public int OrderQty;
        [DataMember]
        public string LastRecievedDate;
        [DataMember]
        public int RecieveQty;
        [DataMember]
        public Money Cost;
        [DataMember]
        public Money Freight;
    }


    [DataContract]
    public class VendorData
    {
        [DataMember]
        public string VendorNo;
        [DataMember]
        public string VendorName;
        [DataMember]
        public string VendorPreference;
        [DataMember]
        public string Buyer;
        [DataMember]
        public string Lead;
        [DataMember]
        public string ManufacturerPrice;
        [DataMember]
        public Money UnitCost;
        [DataMember]
        public Money UnitFreight;
        [DataMember]
        public string Model;
        [DataMember]
        public int Factor;
        [DataMember]
        public string UPC;
        [DataMember]
        public int UnitOfMeasure;
    }

    [DataContract]
    public class WarehouseLocation
    {
        [DataMember]
        public string Location;
        [DataMember]
        public int Capacity;
        [DataMember]
        public int Restock;
    }

    [DataContract]
    public class InventoryTrans
    {
        [DataMember]
        public string TransactionNo;
        [DataMember]
        public string Who;
        [DataMember]
        public string Type;
        [DataMember]
        public string Date;
        [DataMember]
        public string Time;
        [DataMember]
        public string FromLocation;
        [DataMember]
        public string ToLocation;
        [DataMember]
        public int Quantity;

    }

    [DataContract]
    public class InventoryDetail
    {
        [DataMember]
        public string Location;
        [DataMember]
        public string Status;
        [DataMember]
        public int Quantity;

    }
    [DataContract]
    public class Sale
    {
        [DataMember]
        public string WeekStarting;
        [DataMember]
        public int UnitSales;
    }

    [DataContract]
    public class Kit
    {
        [DataMember]
        public string ItemNo;
        [DataMember]
        public string Description;
        [DataMember]
        public int Quantity;
    }

    [DataContract]
    public class PutSerialNumberRequest
    {
        [DataMember]
        public string Account;

        [DataMember]
        public string FullOrderNo;

        [DataMember]
        public SerialNumber[] SerialNumbers;
    }

    [DataContract]
    public class SerialNumber
    {
        [DataMember]
        public decimal EdpNo;
        [DataMember]
        public int LineNo;
        [DataMember]
        public string SerialNo;
    }

    [DataContract]
    public class Address
    {
        [DataMember]
        public string Street;
        [DataMember]
        public string City;
        [DataMember]
        public string State;
        [DataMember]
        public string Zip;
        [DataMember]
        public string CountryCode;
    }

    [DataContract]
    public class ShippingRateRequest
    {
        [DataMember]
        public string Account;
        [DataMember]
        public string  ShipToZip;
        [DataMember]
        public string ShipToCountry;
        [DataMember]
        public Boolean IsShipToCommercial;
        [DataMember]
        public String ShipFromZip;
        [DataMember]
        public String ShipFromCountry;
        [DataMember]
        public string ShipFromWarehouse;
        [DataMember]
        public String[] ShipMethods;
        [DataMember]
        public Boolean IsSignatureRequired;
        [DataMember]
        public Boolean IsAdultSignatureRequired;
        [DataMember]
        public Boolean IsDeliveryConfirmationRequired;
        [DataMember]
        public decimal Length;
        [DataMember]
        public decimal Width;
        [DataMember]
        public decimal Depth;
        [DataMember]
        public decimal Weight;


    }

    [DataContract]
    public class Rate
    {
        [DataMember]
        public string ShipMethod;
        [DataMember]
        public string Cost;
        [DataMember]
        public int DaysInTransit;
        [DataMember]
        public string Message;
    }

    [DataContract]
    public class ShippingRateResponse
    {
        [DataMember]
        public Rate[] Rates;
        [DataMember]
        public Boolean Success;

        [DataMember]
        public string Message;
    }


}
