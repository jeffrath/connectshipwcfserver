-- Did records get to FMSMSG and waiting to process today:
select Count(*) 
from BERKSHIR..FMSMSG
where PROCESS_DATE = convert(char(8),getdate(),112)
and PROCESS_SW = 'N'

--Did records go to FMSMG and are processed today:
select Count(*) 
from BERKSHIR..FMSMSG
where PROCESS_DATE = convert(char(8),getdate(),112)
and PROCESS_SW = 'Y'

--Number of packages in Connectship that  will/did process today count
SELECT count(*) from CONNECTSHIP..ShippingTransactions where convert(char(8),CreateDate,112) = convert(char(8),getdate(),112) and Account = 'NUGLOW'


t = 
--Delete pending backorder letters for  order not in backorder status:
delete FROM ACTIONS WHERE SLOTID IN 
(
select A.SLOTID
from ACTIONS A
left JOIN BOFILE B on A.FULLORDERNO = B.FULLORDERNO 
where A.CHAINACTION like '900%' 
and A.DATEX = convert(char(8),getdate(),112)
and isnull(B.FULLORDERNO, 'choose') = 'choose'
)

--Validate any order shipped today  has a manifest record with a tracking number
        use BERKSHIR
            SELECT DISTINCT OH.FULLORDERNO, M.FULLORDERNO
            FROM ORDERSUBHEAD OH
            LEFT JOIN MANIFESTENTRIES M on OH.FULLORDERNO = M.FULLORDERNO
            WHERE OH.SHIPDATE > '20130901'
            AND LEFT(OH.BIGSTATUS,1) = 'S'
            AND 
            (
            ISNULL(M.FULLORDERNO, 'CHOOSE') = 'CHOOSE'
            or 
            SUBSTRING(MISCDATA110,29,30) = ' '
            )


-- Rebuild manifiest records Based on fullorderno, get rid of ordernumber in where clause to build all missing records for the day, Make sure you delete manifest records with no tracking
-- run this after the rebuild DELETE NOT TESTED DELETE FROM MANIFESTENTRIES SUBSTRING(MISCDATA110,29,30) = ' '
-- Package number must be unique for an order find out if any of the re-built records have need to have  package number rebuilt
-- Find dup package no
-- ******* REBUILD MANIFEST ENTRIES
INSERT INTO MANIFESTENTRIES
            SELECT  distinct 
                           ST.FullOrderNo FULLORDERNO                 --(<FULLORDERNO, char(12),>     
                           ,'WMS' + convert(char(8),getdate()-1,112)       --,<MANIFESTNO, char(12),>
                              MANIFESTNO
                           ,'         01         ' MANIFESTFLAGES     --,<MANIFESTFLAGS, char(20),
                           ,OSH.SHIPMETHOD SHIPMETHOD                 --,<SHIPMETHOD, char(2),>
                           ,'S'     STATUS                                       --,<STATUS, char(2),>
                           ,'0' MANIFESTZONE                         --,<MANIFESTZONE, char(2),>
                           ,CASE                              
                              WHEN Shipper = 'BWI' then '04'        
                              WHEN Shipper = 'TMAB' then '11'       
                              else '01'                             
                              END WAREHOUSE                                --,<WAREHOUSE, char(2),>
                           ,'01' PACKAGE                                   --,<PACKAGENO, char(2),>
                           ,OSH.COMPANY                                            --,<COMPANY, char(2),>
                           ,OSH.DIVISION                                     --,<DIVISION, char(2),>
                           ,ISNULL(RTRIM(RTRIM(STC.NAMEX) + ' ' + RTRIM(STC.FNAME) + ' ' + RTRIM(STC.LNAME)),RTRIM( RTRIM(BC.NAMEX) + ' ' + RTRIM(BC.FNAME) + ' ' + RTRIM(BC.LNAME))) NAMEX--<NAMEX, char(30),>                              --,<NAMEX, char(30),>
                           ,ISNULL(STA.REF1, ISNULL(BA.REF1,' ')) REF1--<REF1, char(30),>                          --,<REF1, char(30),>
                           ,ISNULL(STA.REF2, ISNULL(BA.REF2,' ')) REF2--<REF2, char(30),>                          --,<REF2, char(30),>
                           ,ISNULL(STC.STREET, BC.STREET) STREET--<STREET, char(30),>                           --,<STREET, char(30),>
                           ,ISNULL(STC.CITY, BC.CITY) CITY--<CITY, char(30),>                           --,<CITY, char(30),>
                           ,ISNULL(STC.STATE, BC.STATE) STATE--<STATE, char(2),>                            --,<STATE, char(2),>
                           ,ISNULL(STC.ZIP, BC.ZIP) ZIP--<ZIP, char(14),>                               --,<ZIP, char(14),>
                           ,ISNULL(STC.COUNTRYCODE, BC.COUNTRYCODE) COUNTRYCODE--<COUNTRYCODE, char(4),>                           --,<COUNTRYCODE, char(4),>
                           ,ST.Weight * 10000 WEIGHT                                   --,<WEIGHT, numeric,>
                           ,ST.Weight * 10000 ACTWEIGHT                                    --,<ACTWEIGHT, numeric,>
                           ,0 DV                                                       --,<DV, numeric,>
                           ,cast (Cost * 100 as numeric(9,0))     POSTAGE                                         --,<POSTAGE, numeric,>
                           ,0 INSURANCE                                                        --,<INSURANCE, numeric,>
                           ,0 TSURCHARGE                                                       --,<TSURCHARGE, numeric,>
                           ,0 CODAMOUNT                                                --,<CODAMOUNT, numeric,>
                           ,ISNULL(FOB.AMOUNTS_003, 0) DOLLARSALES --<DOLLARSALES, numeric,>                      --,<DOLLARSALES, numeric,>
                           ,convert(char(8),getdate(),112)  SHIPDATE               --,<SHIPDATE, char(8),>
                           ,convert(char(8),getdate(),112)  DATECHG                --,<DATECHG, char(8),>
                           ,'16301212'                                    TIMECHG           --,<TIMECHG, char(8),>
                           ,'RECOVERY'                                    PROGRAMID           --,<PROGRAMID, char(8),>
                           ,'RECOVERY'                                    [USER]           --,<USERID, char(8),>
                           , '                            ' + TrackingNo --<MISCDATA110, char(100),>    MISCDATA             --,<MISCDATA110, char(100),>
                           ,'CL'                                      STATUSMANIFEST           --,<STATUSMANIFEST, char(2),
                           ,' '                                             COMPANYNAME           --,<COMPANYNAME, char(30),>
                                                                                    
             
            FROM CONNECTSHIP..ShippingTransactions ST
            join ORDERSUBHEAD OSH on ST.FullOrderNo = OSH.FULLORDERNO 
            JOIN ORDERHEADER  OH  ON OSH.ORDERNO + '0000' = OH.FULLORDERNO
            JOIN CUSTOMERS    BC  ON OH.CUSTEDP = BC.CUSTEDP
            LEFT JOIN  CUSTOMERADDL BA ON BC.CUSTEDP = BA.CUSTEDP
            LEFT JOIN ORDERXREF OX ON SUBSTRING(OSH.FULLORDERNO,10,2) + '00' = OX.FULLORDERNO AND XREFNO LIKE 'OS%'
            LEFT JOIN CUSTOMERS STC ON ISNULL(SUBSTRING(XREFNO,3,9),999999999) = STC.CUSTEDP -- ISNULL LOOKS FOR NON EXISTANT CUSTOMER IF NOT A SHIP TO
            LEFT JOIN  CUSTOMERADDL STA ON ISNULL(STC.CUSTEDP,99999999) = STA.CUSTEDP
            JOIN FINANCIALORDER FOB ON OSH.FULLORDERNO = FOB.FULLORDERNO AND ((FOB.BIGSTATUS BETWEEN 'N 11' AND 'N 19') OR (FOB.BIGSTATUS BETWEEN 'P 11' AND 'N 19'))
            where FullOrderNo in 
            ( 
            SELECT DISTINCT OH.FULLORDERNO 
            FROM ORDERSUBHEAD OH
            LEFT JOIN MANIFESTENTRIES M on OH.FULLORDERNO = M.FULLORDERNO
            WHERE convert(char(8),CreateDate,112) = Convert(char(8),GETDATE(),112)
            AND LEFT(OH.BIGSTATUS,1) = 'S'
            AND         
            (
            ISNULL(M.FULLORDERNO, 'CHOOSE') = 'CHOOSE'
            or 
            SUBSTRING(MISCDATA110,29,30) = ' '
            )
            )




	--delete from MANIFESTENTRIES where SHIPDATE = CONVERT(CHAR(8),GETDATE(),112) 