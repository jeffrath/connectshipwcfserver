﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Diagnostics;
using System.Net;


namespace SendEmail
{
    public class SendEmail
    {
        public static string SMTPUSER { get; set; }
        public static string SMTPServer { get; set; }
        public static string SMTPPWD { get; set; }
        public static int SMTPPORT { get; set; }
        public static string EmailTo { get; set; }
        public static string EmailFrom { get; set; }
public static void sendMessage(string message, string subject)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(SMTPServer);

                mail.From = new MailAddress(EmailFrom);
                mail.To.Add(EmailTo);
                mail.Body = message;
                mail.Subject = subject;

                SmtpServer.Port = SMTPPORT;
                SmtpServer.Credentials = new System.Net.NetworkCredential(SMTPUSER, SMTPPWD);
                //SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

       static  public string sendError(Exception objException)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Source		: " + objException.Source.ToString().Trim());
            sb.AppendLine("Method		: " + objException.TargetSite.Name.ToString());
            sb.AppendLine("Date		: " + DateTime.Now.ToLongTimeString());
            sb.AppendLine("Time		: " + DateTime.Now.ToShortDateString());
            sb.AppendLine("Computer	: " + Dns.GetHostName().ToString());
            sb.AppendLine("Error		: " + objException.Message.ToString().Trim());
            sb.AppendLine("Stack Trace	: " + objException.StackTrace.ToString().Trim());

            string subject = Environment.GetCommandLineArgs()[0] + " :Application Error";

            sendMessage(sb.ToString(), subject);

            return sb.ToString();
        }
    }
}
