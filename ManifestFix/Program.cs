﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ManifestFix
{
    class Program
    {
        static int Main(string[] args)
        {
            try
            {
                if (args.Length < 1)
                {
                    Console.WriteLine("Enter Account as a command line parm!  Example ManifestFix.exe Account");
                    return -1;
                }

                SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder(Properties.Settings.Default.dbConnection);
                sb.InitialCatalog = args[0];
                string connectionString = sb.ToString();

                int recordsToFix = RecordsToFix(connectionString);
                if (recordsToFix != 0)
                {
                    FixManifest(connectionString);
                    FixDuplicatePackageNo(connectionString);
                    DeleteBlankManifestEntry(connectionString);
                }
                else
                {
                    LogMessage("ManifestFix No Records to Fix for" + args[0] + " on " +  DateTime.Now.ToString("mm/dd/yy"), 0 );
                    return 0;
                }

                int remainingRecords = RecordsToFix(connectionString);

                Console.WriteLine("Records to Fix:{0} Records remaining:{1}", recordsToFix, remainingRecords);
                return 0;
            }

            catch (Exception ex)
            {
                LogMessage(ex);
                return -10;
            }
        }

        static public int RecordsToFix(string connectionString)
        {
            int records;
            using (System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection())
            {
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
                {
                    cn.ConnectionString = connectionString;
                    cmd.Connection = cn;
                    cn.Open();
                    cmd.CommandText =
                         "Select COUNT(*)FROM ORDERSUBHEAD OH "
                            + " LEFT JOIN MANIFESTENTRIES M on OH.FULLORDERNO = M.FULLORDERNO "
                            + " WHERE OH.SHIPDATE > '20130901'"
                            + " AND LEFT(OH.BIGSTATUS,1) = 'S' "
                            + " AND "
                            + " ( "
                            + " ISNULL(M.FULLORDERNO, 'CHOOSE') = 'CHOOSE' "
                            + " or "
                            + " SUBSTRING(MISCDATA110,29,30) = ' ' "
                            + " )";

                    records = Convert.ToInt32(cmd.ExecuteScalar());

                }
            }
            return records;
        }

        static public void FixManifest(string connectionString)
        {
            using (System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection())
            {
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
                {
                    cn.ConnectionString = connectionString;
                    cmd.Connection = cn;
                    cn.Open();
                    cmd.CommandText =
                    " INSERT INTO MANIFESTENTRIES "
                    + Environment.NewLine
                    + " SELECT  distinct  "
                    + Environment.NewLine
                    + "   ST.FullOrderNo FULLORDERNO                 --(<FULLORDERNO, char(12),>      "
                    + Environment.NewLine
                    + "   ,'WMS' + convert(char(8),getdate()-1,112) 	   --,<MANIFESTNO, char(12),> "
                    + Environment.NewLine
                    + "   MANIFESTNO "
                    + Environment.NewLine
                    + "   ,'         01         ' MANIFESTFLAGES     --,<MANIFESTFLAGS, char(20), "
                    + Environment.NewLine
                    + "   ,OSH.SHIPMETHOD SHIPMETHOD                 --,<SHIPMETHOD, char(2),> "
                    + Environment.NewLine
                    + "   ,'S'	STATUS       		  	               --,<STATUS, char(2),> "
                    + Environment.NewLine
                    + "   ,'0' MANIFESTZONE	  	               --,<MANIFESTZONE, char(2),> "
                    + Environment.NewLine
                    + "   ,CASE 					   "
                    + Environment.NewLine
                    + "     WHEN Shipper = 'BWI' then '04'	   "
                    + Environment.NewLine
                    + "     WHEN Shipper = 'TMAB' then '11'	   "
                    + Environment.NewLine
                    + "     else '01'					   "
                    + Environment.NewLine
                    + "    END WAREHOUSE    		  	               --,<WAREHOUSE, char(2),> "
                    + Environment.NewLine
                    + "  ,'01' PACKAGE			  	               --,<PACKAGENO, char(2),> "
                    + Environment.NewLine
                    + "  ,OSH.COMPANY					  	           --,<COMPANY, char(2),> "
                    + Environment.NewLine
                    + "  ,OSH.DIVISION				  	           --,<DIVISION, char(2),> "
                    + Environment.NewLine
                    + "  ,ISNULL(RTRIM(RTRIM(STC.NAMEX) + ' ' + RTRIM(STC.FNAME) + ' ' + RTRIM(STC.LNAME)),RTRIM( RTRIM(BC.NAMEX) + ' ' + RTRIM(BC.FNAME) + ' ' + RTRIM(BC.LNAME))) NAMEX--<NAMEX, char(30),>			  	       --,<NAMEX, char(30),> "
                    + Environment.NewLine
                    + "  ,ISNULL(STA.REF1, ISNULL(BA.REF1,' ')) REF1--<REF1, char(30),>			  	       --,<REF1, char(30),> "
                    + Environment.NewLine
                    + "  ,ISNULL(STA.REF2, ISNULL(BA.REF2,' ')) REF2--<REF2, char(30),>			  	       --,<REF2, char(30),> "
                    + Environment.NewLine
                    + "  ,ISNULL(STC.STREET, BC.STREET) STREET--<STREET, char(30),>			  	       --,<STREET, char(30),> "
                    + Environment.NewLine
                    + "  ,ISNULL(STC.CITY, BC.CITY) CITY--<CITY, char(30),>			  	       --,<CITY, char(30),> "
                    + Environment.NewLine
                    + "  ,ISNULL(STC.STATE, BC.STATE) STATE--<STATE, char(2),>			  	       --,<STATE, char(2),> "
                    + Environment.NewLine
                    + "  ,ISNULL(STC.ZIP, BC.ZIP) ZIP--<ZIP, char(14),>			  	       --,<ZIP, char(14),> "
                    + Environment.NewLine
                    + "  ,ISNULL(STC.COUNTRYCODE, BC.COUNTRYCODE) COUNTRYCODE--<COUNTRYCODE, char(4),>			  	   --,<COUNTRYCODE, char(4),> "
                    + Environment.NewLine
                    + "  ,ST.Weight * 10000 WEIGHT				  	       --,<WEIGHT, numeric,> "
                    + Environment.NewLine
                    + "  ,ST.Weight * 10000 ACTWEIGHT				  	       --,<ACTWEIGHT, numeric,> "
                    + Environment.NewLine
                    + "  ,0 DV						  	               --,<DV, numeric,> "
                    + Environment.NewLine
                    + "  ,cast (Cost * 100 as numeric(9,0))	POSTAGE				  	           --,<POSTAGE, numeric,> "
                    + Environment.NewLine
                    + "  ,0 INSURANCE						  	               --,<INSURANCE, numeric,> "
                    + Environment.NewLine
                    + "  ,0 TSURCHARGE						  	               --,<TSURCHARGE, numeric,> "
                    + Environment.NewLine
                    + "  ,0 CODAMOUNT					  	               --,<CODAMOUNT, numeric,> "
                    + Environment.NewLine
                    + "  ,ISNULL(FOB.AMOUNTS_003, 0) DOLLARSALES --<DOLLARSALES, numeric,>			  	   --,<DOLLARSALES, numeric,> "
                    + Environment.NewLine
                    + "  ,convert(char(8), getdate(),112)	SHIPDATE	  	   --,<SHIPDATE, char(8),> "
                    + Environment.NewLine
                    + "  ,convert(char(8), getdate(),112)	DATECHG	  	   --,<DATECHG, char(8),> "
                    + Environment.NewLine
                    + "  ,'16301212'					  	TIMECHG           --,<TIMECHG, char(8),> "
                    + Environment.NewLine
                    + "  ,'RECOVERY'					  	PROGRAMID           --,<PROGRAMID, char(8),> "
                    + Environment.NewLine
                    + "  ,'RECOVERY'					  	[USER]           --,<USERID, char(8),> "
                    + Environment.NewLine
                    + "  , '                            ' + TrackingNo --<MISCDATA110, char(100),>	MISCDATA	  	   --,<MISCDATA110, char(100),> "
                    + Environment.NewLine
                    + "  ,'CL'					   	    STATUSMANIFEST           --,<STATUSMANIFEST, char(2), "
                    + Environment.NewLine
                    + "  ,' '						  	    COMPANYNAME           --,<COMPANYNAME, char(30),> "
                    + Environment.NewLine
                    + "  FROM " + Properties.Settings.Default.ConnectShipDB.Trim() + "..ShippingTransactions ST "
                    + Environment.NewLine
                    + "  JOIN ORDERSUBHEAD OSH on ST.FullOrderNo = OSH.FULLORDERNO  "
                    + Environment.NewLine
                    + "   JOIN ORDERHEADER  OH  ON OSH.ORDERNO + '0000' = OH.FULLORDERNO "
                    + Environment.NewLine
                    + "  JOIN CUSTOMERS    BC  ON OH.CUSTEDP = BC.CUSTEDP "
                    + Environment.NewLine
                    + "  LEFT JOIN  CUSTOMERADDL BA ON BC.CUSTEDP = BA.CUSTEDP "
                    + Environment.NewLine
                    + "  LEFT JOIN ORDERXREF OX ON SUBSTRING(OSH.FULLORDERNO,10,2) + '00' = OX.FULLORDERNO AND XREFNO LIKE 'OS%' "
                    + Environment.NewLine
                    + "  LEFT JOIN CUSTOMERS STC ON ISNULL(SUBSTRING(XREFNO,3,9),999999999) = STC.CUSTEDP -- ISNULL LOOKS FOR NON EXISTANT CUSTOMER IF NOT A SHIP TO "
                    + Environment.NewLine
                    + "  LEFT JOIN  CUSTOMERADDL STA ON ISNULL(STC.CUSTEDP,99999999) = STA.CUSTEDP "
                    + Environment.NewLine
                    + "  JOIN FINANCIALORDER FOB ON OSH.FULLORDERNO = FOB.FULLORDERNO AND ((FOB.BIGSTATUS BETWEEN 'N 11' AND 'N 19') OR (FOB.BIGSTATUS BETWEEN 'P 11' AND 'N 19')) "
                    + Environment.NewLine
                    + "  where FullOrderNo in  "
                    + Environment.NewLine
                    + "  (  "
                    + Environment.NewLine
                    + "     SELECT DISTINCT OH.FULLORDERNO  "
                    + Environment.NewLine
                    + "     FROM ORDERSUBHEAD OH "
                    + Environment.NewLine
                    + "     LEFT JOIN MANIFESTENTRIES M on OH.FULLORDERNO = M.FULLORDERNO "
                    + Environment.NewLine
                    //TODO take out after testing
                    + "     WHERE OH.SHIPDATE > convert(char(8), getdate() - 300,112) "
                    + Environment.NewLine
                    + "      AND LEFT(OH.BIGSTATUS,1) = 'S' "
                    + Environment.NewLine
                    + "     AND 		 "
                    + Environment.NewLine
                    + "     ( "
                    + Environment.NewLine
                    + "        ISNULL(M.FULLORDERNO, 'CHOOSE') = 'CHOOSE' "
                    + Environment.NewLine
                    + "       or  "
                    + Environment.NewLine
                    + "       SUBSTRING(MISCDATA110,29,30) = ' ' "
                    + Environment.NewLine
                    + "      ) "
                    + Environment.NewLine
                    + "  ) ";

                    int recordsChanged = Convert.ToInt16(cmd.ExecuteNonQuery());
                    Console.WriteLine("Records Inserted into MANIFESTENTRIES:{0}", recordsChanged);
                }
            }
        }

        static public void FixDuplicatePackageNo(string connectionString)
        {
            using (System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection())
            {
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
                {
                    cn.ConnectionString = connectionString;
                    cmd.Connection = cn;
                    cn.Open();
                    cmd.CommandText =
                        "SELECT FULLORDERNO, SLOTID "
                        + " FROM MANIFESTENTRIES WHERE  "
                        + " FULLORDERNO In "
                        + " ( "
                        + "Select FULLORDERNO "
                         + " from MANIFESTENTRIES "
                         + " group by FULLORDERNO, PACKAGENO "
                         + " having count(*) > 1"
                         + " )"
                         + " ORDER BY FULLORDERNO, SLOTID ";

                    int packageCounter = 0;
                    string holdFullOrderNo="";
                    using (System.Data.SqlClient.SqlDataReader rd = cmd.ExecuteReader())
                    {
                        while (rd.Read())
                        {
                            if (holdFullOrderNo != rd[0].ToString())
                            {
                                holdFullOrderNo = rd[0].ToString();
                                packageCounter = 0;
                            }
                            packageCounter += 1;
                            Int64 slotId = Convert.ToInt64(rd[1]);
                            UpdateManifestEntry(packageCounter, slotId, connectionString);
                        }
                    }
                }
            }
        }

        private static void DeleteBlankManifestEntry( string connectionString)
        {
            using (System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection())
            using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
            {
                cn.ConnectionString = connectionString;
                cmd.Connection = cn;
                cn.Open();
                cmd.CommandText = "DELETE FROM MANIFESTENTRIES where  SUBSTRING(MISCDATA110,29,30) = ' '";
                int recordsDeleted = Convert.ToInt16(cmd.ExecuteNonQuery());
                Console.WriteLine("Blank Manifest Records Deleted:{0}", recordsDeleted);
            }
        }

        private static void UpdateManifestEntry( int packageCounter,Int64 slotId, string connectionString)
        {
            using (System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection())
               using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
                {
                    cn.ConnectionString = connectionString;
                    cmd.Connection = cn;
                    cn.Open();
                    cmd.Parameters.Add("@SlotId", SqlDbType.BigInt);
                    cmd.Parameters["@SlotId"].Value = slotId;
                    cmd.Parameters.Add("@PackageNo", SqlDbType.Char);
                    cmd.Parameters["@PackageNo"].Value = packageCounter.ToString("00");
                    cmd.CommandText ="UPDATE MANIFESTENTRIES SET PACKAGENO = @PackageNo where SLOTID = @SlotId";
                    cmd.ExecuteNonQuery();
                }
        }

        static public void LogMessage(Exception ex)
        {

            SendEmail.SendEmail se = new SendEmail.SendEmail();
            Logger.ErrorLog.ErrorRoutine(false, ex);
            SendEmail.SendEmail.EmailFrom = Properties.Settings.Default.EmailFrom;
            SendEmail.SendEmail.EmailTo = Properties.Settings.Default.EmailTo;
            SendEmail.SendEmail.SMTPPORT = Properties.Settings.Default.SMTPPort;
            SendEmail.SendEmail.SMTPPWD = Properties.Settings.Default.SMTPPwd;
            SendEmail.SendEmail.SMTPServer = Properties.Settings.Default.SMTPHost;
            SendEmail.SendEmail.SMTPUSER = Properties.Settings.Default.SMTPUser;
            Console.WriteLine(SendEmail.SendEmail.sendError(ex));
        }


        static public void LogMessage(string message, int severity)
        {
            string subject = "Informational Message from " + Environment.GetCommandLineArgs()[0];
            if (severity == 1)
            {
                subject = "Warning Message from " + Environment.GetCommandLineArgs()[0];
            }
            Console.WriteLine(message);
            Logger.ErrorLog.ErrorRoutine(false, message);
            SendEmail.SendEmail se = new SendEmail.SendEmail();
            SendEmail.SendEmail.EmailFrom = Properties.Settings.Default.EmailFrom;
            SendEmail.SendEmail.EmailTo = Properties.Settings.Default.EmailTo;
            SendEmail.SendEmail.SMTPPORT = Properties.Settings.Default.SMTPPort;
            SendEmail.SendEmail.SMTPPWD = Properties.Settings.Default.SMTPPwd;
            SendEmail.SendEmail.SMTPServer = Properties.Settings.Default.SMTPHost;
            SendEmail.SendEmail.SMTPUSER = Properties.Settings.Default.SMTPUser;
            SendEmail.SendEmail.sendMessage(message, subject);

        }
    }
}
