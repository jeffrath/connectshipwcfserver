﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using log4net.Config;
using System.Reflection;
using System.IO;

namespace SuppleCollates
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static string _account;
        static int Main(string[] args)
        {

            XmlConfigurator.Configure(); //only once
            
            try
            {

                log.Info("SuppleCollates starting at " + DateTime.Now.ToString());
                string version = Version();
                Console.WriteLine(version);

                if (args.Length < 1)
                {
                    log.Fatal("Missing AccountName on run line **** Aborting");
                    return 10;
                }

                _account = args[0];

                string remotePath = Properties.Settings.Default.collatePath + "\\archive";
                string[] files = System.IO.Directory.GetFiles(Properties.Settings.Default.collatePath, "cl??????.");
                if (!System.IO.Directory.Exists(remotePath))
                {
                    System.IO.Directory.CreateDirectory(Properties.Settings.Default.collatePath + "\\archive");
                }

                //dbAccess db = new dbAccess(account);
                List<Collate> collates = new List<Collate>();
                foreach (var file in files)
                {
                    collates.AddRange(ProcessFile(file));
                    System.IO.FileInfo tf = new FileInfo(file);
                    tf.MoveTo(tf.DirectoryName + "\\archive\\" + tf.Name);
                }

                CreateCollate(collates);

                return 0;
            }
            catch (Exception ex)
            {
                log.Fatal(ex.Message, ex);
                return 10;
            }
        }

        private static void SFTP(string fileName)
        {
            FTPUtilities.ftpUtilities sftp = new FTPUtilities.ftpUtilities();
            sftp.ChilKatFTPKey = Properties.Settings.Default.ChilKatSFTPKey;
            sftp.ChilKatSFTPKey = Properties.Settings.Default.ChilKatSFTPKey;
            sftp.FtpPort = Properties.Settings.Default.ftp_port;
            sftp.FtpPWD = Properties.Settings.Default.ftp_pwd;
            sftp.FtpUser = Properties.Settings.Default.ftp_user;
            sftp.Host = Properties.Settings.Default.ftp_server;
            System.IO.FileInfo fi = new System.IO.FileInfo(fileName);
            string remoteFileName = fi.Name + "_" + DateTime.Now.ToString("yyMMddHHmmss") + ".collates";
            sftp.RemoteFileName = remoteFileName;
            sftp.sftpPutFile(fileName);
            System.IO.FileInfo tf = new FileInfo(fileName);
            string archiveFileName = tf.Name + "_" + DateTime.Now.ToString("yyMMddHHmmss") + ".collates";
            tf.MoveTo(tf.DirectoryName + "\\archive\\" + archiveFileName);
            log.Info("**************************************************************************");
            log.Info("FTP File Name:" + fileName + " Archive:" + archiveFileName);
        }

        /// <summary>
        /// Convert rows to a collage file
        /// </summary>
        /// <param name="collate"></param>
        private static void CollateToFile(Collate collate)
        {

            //Todo Work on file name
            //Create collate file in the pub folder
            string fileName = Properties.Settings.Default.collatePath.Trim().TrimEnd('\\') + "\\";
            fileName += collate.FileName() + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".collates" ;
            log.Debug("Creating File:" + fileName);

            /* ***********************************************************************************************************
             * Create Collate File
             * ***********************************************************************************************************/
            using (StreamWriter sw = new StreamWriter(fileName))
            {
                string outString = collate.ToString();
                sw.Write(outString);
            }

            /* ***********************************************************************************************************
             * New FTP Logic
             * ***********************************************************************************************************/

            //Arhcive folder under pub folder 
            string remotePath = Properties.Settings.Default.collatePath + "\\archive";
            //
           
            if (!System.IO.Directory.Exists(remotePath))
            {
                System.IO.Directory.CreateDirectory(Properties.Settings.Default.collatePath + "\\archive");
            }
            
            ThillSFTP thillSftp = new ThillSFTP(
                Properties.Settings.Default.ftp_server,
                 Properties.Settings.Default.ftp_port,
                 Properties.Settings.Default.ftp_user,
                 Properties.Settings.Default.ftp_pwd
                );

 
            //This is the new collate file
            System.IO.FileInfo fi = new System.IO.FileInfo(fileName);
            //string outputFileName = fi.Directory + @"\tempcollate.txt";
            
            
            //System.IO.FileInfo tf = new FileInfo(outputFileName);

            //Where File is being placed on Gemini server
            string ftpStagingPath = Properties.Settings.Default.ftpStagePath.Trim().TrimEnd('/').TrimEnd('\\');

            //Name of staged file
            string ftpStagedFile = ftpStagingPath + "\\" + fi.Name;

            System.IO.File.Copy(fileName, ftpStagedFile);
            thillSftp.UploadFile(ftpStagedFile);

            //archive file
            //fi.MoveTo(fi.Directory + @"\archive\" + fi.Name);
 
            //Arhcive FIle
            string newPath = fi.DirectoryName + @"\Archive\" + fi.Name ;
            fi.MoveTo(newPath);
 

        }
        /// <summary>
        /// Read file and create a list of Collates to sort and write out to a file and then ftp.
        /// </summary>
        /// <param name="fileName"></param>
        private static List<Collate> ProcessFile(string fileName)
        {
            List<Collate> collates = new List<Collate>();
            //Read file and seperate each collate into a collate object
            using (StreamReader sr = new StreamReader(fileName))
            {
                List<string> rows = new List<string>();

                Collate d = null;
                Boolean firstTime = true;
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    //todo find out how to tell if it's a new collate
                    string recType = line.Substring(108, 3);
                    if (recType == "010")
                    {


                        if (firstTime)
                        {
                            rows = new List<string>();
                            rows.Add(line);
                            firstTime = false;
                        }
                        else
                        {
                            d = new Collate(rows, _account, true);
                            collates.Add(d);
                            rows = new List<string>();
                            rows.Add(line);
                        }

                    }
                    else
                    {
                        rows.Add(line);

                    }
                }
                d = new Collate(rows, _account, true);
                collates.Add(d);
            }

            
            return collates;
        }

        static public void CreateCollate(List<Collate> collates)
        {
            //Sort by Warehouse, Shipmethod and Language
            //var collatesSorted = collates.OrderBy(x => (x.WarehouseCode + x.Warehouse + x.ShipMethod + x.Language + x.SortingItem));
            var collatesSorted = collates.OrderBy(x => (x.WarehouseCode  + x.Language + x.Continuity +  x.Division + x.SortingItem));


            string holdShipMethod = "";
            string holdWarehouse = "";
            string holdLanguage = "";
            string holdWarehouseCode = "";
            string holdDivision = "";
            string holdContinuity = "";

            List<string> collateOut = new List<string>();

            int numberOfCollates = 0;
            //Loop thru sorted list, when a change occurs write it out
            foreach (var c in collatesSorted)
            {
                //log.Debug("FullOrderNo:" + c.FullOrderNo+ " Language:" + c.Language + " Division:" + c.Division + " Warehouse:" + c.WarehouseCode + " Continuity:" + c.Continuity);

                if (c.Language != holdLanguage)
                {
                    if (collateOut != null && collateOut.Count != 0)
                    {
                        Collate collate = new Collate(collateOut, _account, false);
                        log.Debug("******************************************* NEW FILE" + collate.FileName());
                        CollateToFile(collate);

                        log.Debug("Collates in file:" + numberOfCollates.ToString() + Environment.NewLine);
                        collateOut.Clear();
                        numberOfCollates = 0;
                    }

                    holdWarehouseCode = c.WarehouseCode;
                    holdLanguage = c.Language;
                    holdDivision = c.Division;
                    holdContinuity = c.Continuity;
                    Console.WriteLine(" ");
                }
                
                if (c.Continuity != holdContinuity)
                {
                    if (collateOut != null && collateOut.Count != 0)
                    {
                        Collate collate = new Collate(collateOut, _account, false);
                        log.Debug("******************************************* NEW FILE" + collate.FileName());   
                        CollateToFile(collate);
                        
                        log.Debug("Collates in file:" + numberOfCollates.ToString() + Environment.NewLine);
                        collateOut.Clear();
                        numberOfCollates = 0;
                    }

                    holdWarehouseCode = c.WarehouseCode;
                    holdLanguage = c.Language;
                    holdDivision = c.Division;
                    holdContinuity = c.Continuity;
                    Console.WriteLine(" ");
                }


                if (c.Division != holdDivision)
                {
                    if (collateOut != null && collateOut.Count != 0)
                    {
                        Collate collate = new Collate(collateOut, _account, false);
                        log.Debug("******************************************* NEW FILE" + collate.FileName()); 
                        log.Debug("Collates in file:" + numberOfCollates.ToString() + Environment.NewLine);
                        CollateToFile(collate);
                        collateOut.Clear();
                        numberOfCollates = 0;
                    }
                    holdWarehouseCode = c.WarehouseCode;
                    holdLanguage = c.Language;
                    holdDivision = c.Division;
                    holdContinuity = c.Continuity;
                    Console.WriteLine(" ");
                }

                if (c.WarehouseCode != holdWarehouseCode)
                {
                    if (collateOut != null && collateOut.Count != 0)
                    {
                        Collate collate = new Collate(collateOut, _account, false);
                        log.Debug("******************************************* NEW FILE" + collate.FileName()); 
                        log.Debug("Collates in file:" + numberOfCollates.ToString() + Environment.NewLine);
                         
                        CollateToFile(collate);
                        log.Debug("Collates in file:" + numberOfCollates.ToString() + Environment.NewLine);
                        collateOut.Clear();
                        numberOfCollates = 0;
                    }
                    holdWarehouseCode = c.WarehouseCode;
                    holdLanguage = c.Language;
                    holdDivision = c.Division;
                    holdContinuity = c.Continuity;
                    Console.WriteLine(" ");
                }

                numberOfCollates += 1;
                 collateOut.Add(c.ToString());
            }


            //Clean up and print last remaining collates
            if (collateOut != null && collateOut.Count != 0)
            {
                Collate collate = new Collate(collateOut, _account, false);
                log.Debug("Collates in file:" + numberOfCollates.ToString() + Environment.NewLine);
                CollateToFile(collate);
                collateOut.Clear();
            }
        }

        public static String Version()
        {
            string response;
            // This variable holds the amount of indenting that  
            // should be used when displaying each line of information.
            int indent = 0;
            // Display information about the EXE assembly.
            Assembly a = System.Reflection.Assembly.GetExecutingAssembly();
            response = " ".PadLeft(indent, ' ') + "Assembly identity=" + a.FullName;
            response += Environment.NewLine;
            response += " ".PadLeft(indent, ' ') + "Codebase=" + a.CodeBase;
            response += Environment.NewLine;
            // Display the set of assemblies our assemblies reference.

            response += " ".PadLeft(indent, ' ') + "Referenced assemblies:";
            response += Environment.NewLine;
            foreach (AssemblyName an in a.GetReferencedAssemblies())
            {
                response += response = " ".PadLeft(indent, ' ') + "Name=" + an.Name + " Version=" + an.Version.ToString();
                response += Environment.NewLine;
            }
            return response;
        }
    }

}
