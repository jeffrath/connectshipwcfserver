﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
namespace SuppleCollates
{
    class Collate
    {
        
        private string _account;

        public string Account
        {
            get { return _account; }
            set { _account = value; }
        }

        private string _division;

        public string Division
        {
            get { return _division; }
            set { _division = value; }
        }
        private string _continuity;

        public string Continuity
        {
            get { return _continuity; }
            set { _continuity = value; }
        }
        private string _shipMethod;

        public string ShipMethod
        {
            get { return _shipMethod; }
            set { _shipMethod = value; }
        }
        private string _warehouse;

        public string Warehouse
        {
            get { return _warehouse; }
            set { _warehouse = value; }
        }

        private string _warehouseCode;

        public string WarehouseCode
        {
            get { return _warehouseCode; }
            set { _warehouseCode = value; }
        }

        private string _language;

        public string Language
        {
            get { return _language; }
            set { _language = value; }
        }
        private List<string> _collateData;

        public List<string> CollateData
        {
            get { return _collateData; }
            set { _collateData = value; }
        }

        private string _sortingItem;

        public string SortingItem
        {
            get { return _sortingItem; }
            set { _sortingItem = value; }
        }

        private string _fullOrderNo;

        public string FullOrderNo
        {
            get { return _fullOrderNo; }
            set { _fullOrderNo = value; }
        }


        public Collate()
        { }

        public Collate(List<string> rows, string account, bool sort)
        {
            dbAccess db = new dbAccess(Properties.Settings.Default.dbConnectship, _account);
            _account = account;
            _collateData = new List<string>();
            if (sort)
            {
                var temp = rows.OrderBy(x => x.Substring(108, 3)).ThenBy(y => y.Substring(128, 20));

                foreach (var item in temp)
                {
                    _collateData.Add(item);
                }
            }
            else
            {
                _collateData = rows;
            }

            string warehouse = "";
            
            
                for (int i = 0; i < rows.Count; i++)
                {
                 
                
                if (rows[i].Substring(108, 3) == "010")
                {

                    _fullOrderNo = rows[i].Substring(8, 12);
                    _warehouse = db.GetWarehouse(_fullOrderNo, account);

                    if (_fullOrderNo.Substring(8, 1) == "5")
                    {
                        _continuity = "_CONT";
                    }
                    else
                    {
                        _continuity = string.Empty;
                    }

                }
                if (rows[i].Trim() != string.Empty)
                {
                //Don't ask about the final length it will make you sick.
                // Second time thru,all records for 1 order are stored in 1 row
                rows[i] = rows[i].Substring(0, 4) + _warehouse + rows[i].Substring(6, rows[i].Length - 7);
                }

                if (rows[i].Substring(108, 3) == "400")
                {
                 

                    if (
                        (!rows[i].Substring(128, 20).Trim().ToUpper().StartsWith("LOYALTY"))
                        && (!rows[i].Substring(128, 20).Trim().ToUpper().StartsWith("SHAKE"))
                        && (!rows[i].Substring(128, 20).Trim().ToUpper().StartsWith("LIT"))
                        )
                    {
                        _sortingItem = rows[i].Substring(128, 20);
                        break;
                    }
                }
            }
            //Set warehouse 5 - 6
            //Set ShipMethod 7 - 8
            //
            //Set Language
            //Division pos 3- 4

            
            _warehouse = rows[0].Substring(4, 2);
            _warehouseCode = db.TranslateFromEcometry("SUPPLE", _warehouse, "Warehouse");
            _shipMethod = rows[0].Substring(6, 2); 
            _language = rows[0].Substring(2, 2);
            

            _division = _language;

            if (_language == "02")
            {
                _language = "Spanish";
            }
            else
            {
                _language = "English";
            }
        }

        public string FileName()
        {

            string fileName = Account + "_" + WarehouseCode + "_" + Language +  _continuity + "_" + _division;
            //if (ShipMethod == "01" || ShipMethod == "01")
            //{
            //    fileName += "_Expedite";
            //}
            return fileName;
        }

        public string ToString()
        {
            string outString = string.Empty; 
            foreach (var item in _collateData)
            {
                outString += item + Environment.NewLine;
            }
            return outString;
        }
    }
}
