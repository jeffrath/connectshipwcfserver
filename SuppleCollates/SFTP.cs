﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FTPUtilities
{
    class ftpUtilities
    {
        private string _host;

        public string Host
        {
            get { return _host; }
            set { _host = value; }
        }
        private string _ftpUser;

        public string FtpUser
        {
            get { return _ftpUser; }
            set { _ftpUser = value; }
        }
        private string _ftpPWD;

        public string FtpPWD
        {
            get { return _ftpPWD; }
            set { _ftpPWD = value; }
        }
        private int _ftpPort;

        public int FtpPort
        {
            get { return _ftpPort; }
            set { _ftpPort = value; }
        }

        private bool _ftpPassive;

        public bool FtpPassive
        {
            get { return _ftpPassive; }
            set { _ftpPassive = value; }
        }

        private string _remoteDir;

        public string RemoteDir
        {
            get { return _remoteDir; }
            set { _remoteDir = value; }
        }

        private string _remoteFileName;

        public string RemoteFileName
        {
            get { return _remoteFileName; }
            set { _remoteFileName = value; }
        }

        private string _chilKatSFTPKey;

        public string ChilKatSFTPKey
        {
            get { return _chilKatSFTPKey; }
            set { _chilKatSFTPKey = value; }
        }


        private string _chilKatFTPKey;

        public string ChilKatFTPKey
        {
            get { return _chilKatFTPKey; }
            set { _chilKatFTPKey = value; }
        }
        public bool ftpPutFile(string fileName)
        {

            try
            {

                Chilkat.Ftp2 ftp = new Chilkat.Ftp2();

                bool success;

                //  Any string unlocks the component for the 1st 30-days.
                success = ftp.UnlockComponent(_chilKatFTPKey);
                if (success != true)
                {
                    throw new Exception(ftp.LastErrorText);
                }

                ftp.Hostname = _host;
                ftp.Username = _ftpUser;
                ftp.Password = _ftpPWD;

                //  The default data transfer mode is "Active" as opposed to "Passive".

                //  Connect and login to the FTP server.
                success = ftp.Connect();
                if (success != true)
                {
                    throw new Exception(ftp.LastErrorText);
                }

                //  Change to the remote directory where the file will be uploaded.
                if (string.Equals(_remoteDir, ""))
                {
                    success = ftp.ChangeRemoteDir(_remoteDir);
                    if (success != true)
                    {
                        throw new Exception(ftp.LastErrorText);
                    }
                }


                //  Upload a file.
                string localFilename;
                localFilename = fileName;
                string remoteFilename = fileName;
                remoteFilename = remoteFilename;

                success = ftp.PutFile(localFilename, remoteFilename);
                if (success != true)
                {
                    throw new Exception(ftp.LastErrorText);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool sftpMget(string localPath)
        {

            try
            {
                Chilkat.SFtp sftp = new Chilkat.SFtp();

                //  Any string automatically begins a fully-functional 30-day trial.
                bool success;
                success = sftp.UnlockComponent(_chilKatSFTPKey);
                if (success != true)
                {
                    throw new Exception(sftp.LastErrorText);
                }

                //  Set some timeouts, in milliseconds:
                sftp.ConnectTimeoutMs = 5000;
                sftp.IdleTimeoutMs = 10000;

                //  Connect to the SSH server.
                //  The standard SSH port = 22
                //  The hostname may be a hostname or IP address.
                int port;
                string hostname;
                hostname = _host;
                port = 22;
                success = sftp.Connect(hostname, port);
                if (success != true)
                {
                    throw new Exception(sftp.LastErrorText);
                }

                //  Authenticate with the SSH server.  Chilkat SFTP supports
                //  both password-based authenication as well as public-key
                //  authentication.  This example uses password authenication.
                success = sftp.AuthenticatePw(_ftpUser, _ftpPWD);
                if (success != true)
                {
                    throw new Exception(sftp.LastErrorText);
                }

                //  After authenticating, the SFTP subsystem must be initialized:
                success = sftp.InitializeSftp();
                if (success != true)
                {
                    throw new Exception(sftp.LastErrorText);
                }

                //  Open a directory on the server...
                //  Paths starting with a slash are "absolute", and are relative
                //  to the root of the file system. Names starting with any other
                //  character are relative to the user's default directory (home directory).
                //  A path component of ".." refers to the parent directory,
                //  and "." refers to the current directory.
                string handle;
                string dirPath;
                dirPath = _remoteDir;
                handle = sftp.OpenDir(dirPath);
                if (handle == null)
                {
                    throw new Exception(sftp.LastErrorText);
                }

                //  Download the directory listing:
                Chilkat.SFtpDir dirListing = null;
                dirListing = sftp.ReadDir(handle);
                if (dirListing == null)
                {
                    throw new Exception(sftp.LastErrorText);
                }

                //  Iterate over the files.
                int i;
                int n;
                n = dirListing.NumFilesAndDirs;
                if (n == 0)
                {
                    //TODO Log that no file was found
                }
                else
                {
                    for (i = 0; i <= n - 1; i++)
                    {
                        Chilkat.SFtpFile fileObj = null;
                        fileObj = dirListing.GetFileObject(i);


                        if (fileObj.IsDirectory)
                        {
                            continue;
                        }

                        //textBox1.Text += fileObj.Filename + "\r\n";

                        //  Does this filename match the desired pattern?
                        //  Write code here to determine if it's a match or not.

                        //  Assuming it's a match, you would download the file
                        //  like this:
                        string remoteFilePath;
                        remoteFilePath = dirPath + "/";
                        remoteFilePath = remoteFilePath + fileObj.Filename;
                        string localFilePath;
                        if (localPath.Trim() != string.Empty)
                        {
                            localFilePath = localPath.Trim().TrimEnd('\\') + @"\" + fileObj.Filename;
                        }
                        else
                        {
                            localFilePath = fileObj.Filename;

                        }

                        success = sftp.DownloadFileByName(remoteFilePath, localFilePath);
                        if (success != true)
                        {
                            throw new Exception(sftp.LastErrorText);
                        }
                        string processedFilePath = " ";
                        processedFilePath = _remoteDir.Trim().TrimEnd('\\') + @"/processed/" + fileObj.Filename;

                        //Logger.ErrorLog.ErrorRoutine(false, "File Retrieved:" + remoteFilePath + " LocalFilePath:" + localFilePath);
                        success = sftp.RenameFileOrDir(remoteFilePath, processedFilePath);
                        if (success != true)
                        {
                            throw new Exception(sftp.LastErrorText);
                        }

                    }

                }

                //  Close the directory
                success = sftp.CloseHandle(handle);
                if (success != true)
                {
                    throw new Exception(sftp.LastErrorText);
                }


                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool sftpPutFile(string fileName)
        {
            try
            {
                Chilkat.SFtp sftp = new Chilkat.SFtp();

                //  Any string automatically begins a fully-functional 30-day trial.
                bool success;
                //success = sftp.UnlockComponent("FFRRTHSSH_NEZYQN8T9Tnb");
                success = sftp.UnlockComponent(_chilKatSFTPKey);
                if (success != true)
                {
                    throw new Exception(sftp.LastErrorText);
                }

                //  Set some timeouts, in milliseconds:
                sftp.ConnectTimeoutMs = 15000;
                sftp.IdleTimeoutMs = 15000;

                //  Connect to the SSH server.
                //  The standard SSH port = 22
                //  The hostname may be a hostname or IP address.
                int port;
                string hostname;
                hostname = _host;
                port = _ftpPort;
                success = sftp.Connect(hostname, port);
                if (success != true)
                {
                    throw new Exception(sftp.LastErrorText);
                }

                //  Authenticate with the SSH server.  Chilkat SFTP supports
                //  both password-based authenication as well as public-key
                //  authentication.  This example uses password authenication.
                success = sftp.AuthenticatePw(_ftpUser, _ftpPWD);
                if (success != true)
                {
                    throw new Exception(sftp.LastErrorText);
                }

                //  After authenticating, the SFTP subsystem must be initialized:
                success = sftp.InitializeSftp();
                if (success != true)
                {
                    throw new Exception(sftp.LastErrorText);
                }

                //  Upload from the local file to the SSH server.
                //  Important -- the remote filepath is the 1st argument,
                //  the local filepath is the 2nd argument;
                string remoteFilePath;
                remoteFilePath = _remoteFileName;
                string localFilePath;
                localFilePath = fileName;



                success = sftp.UploadFileByName(remoteFilePath, localFilePath);
                if (success != true)
                {
                    throw new Exception(sftp.LastErrorText);
                }

                ////  Download the directory listing:
                //string handle;
                //string dirPath;
                //dirPath = _remoteDir;
                //handle = sftp.OpenDir(dirPath);
                //Chilkat.SFtpDir dirListing = null;
                //dirListing = sftp.ReadDir(handle);
                //if (dirListing == null)
                //{
                //    throw new Exception(sftp.LastErrorText);
                //}
                //dirListing.
                //Logger.ErrorLog.ErrorRoutine(false, "File " + localFilePath + " put to " + remoteFilePath);

            }
            catch (Exception ex)
            {
                throw;
            }
            return true;
        }


        public bool sftpString(string fileName, string ftpString)
        {
            try
            {
                Chilkat.SFtp sftp = new Chilkat.SFtp();

                //  Any string automatically begins a fully-functional 30-day trial.
                bool success;
                success = sftp.UnlockComponent("FFRRTHSSH_NEZYQN8T9Tnb");
                if (success != true)
                {
                    throw new Exception(sftp.LastErrorText);
                }

                //  Set some timeouts, in milliseconds:
                sftp.ConnectTimeoutMs = 5000;
                sftp.IdleTimeoutMs = 15000;


                //  Connect to the SSH server.
                //  The standard SSH port = 22
                //  The hostname may be a hostname or IP address.
                int port;
                string hostname;
                hostname = _host;
                port = _ftpPort;
                success = sftp.Connect(hostname, port);
                if (success != true)
                {
                    throw new Exception(sftp.LastErrorText);
                }

                //  Authenticate with the SSH server.  Chilkat SFTP supports
                //  both password-based authenication as well as public-key
                //  authentication.  This example uses password authenication.
                success = sftp.AuthenticatePw(_ftpUser, _ftpPWD);
                if (success != true)
                {
                    throw new Exception(sftp.LastErrorText);
                }

                //  After authenticating, the SFTP subsystem must be initialized:
                success = sftp.InitializeSftp();
                if (success != true)
                {
                    throw new Exception(sftp.LastErrorText);
                }


                //  Open a file on the server for writing.
                //  "createTruncate" means that a new file is created; if the file already exists, it is opened and truncated.
                string handle;
                handle = sftp.OpenFile(fileName, "writeOnly", "createTruncate");
                if (handle == null)
                {
                    throw new Exception(sftp.LastErrorText);
                }

                //  Write some text to the file:
                success = sftp.WriteFileText(handle, "ansi", ftpString);
                if (success != true)
                {
                    throw new Exception(sftp.LastErrorText);
                }


                //  Close the file.
                success = sftp.CloseHandle(handle);
                if (success != true)
                {
                    throw new Exception(sftp.LastErrorText);
                }

            }
            catch (Exception ex)
            {
                throw;
            }
            return true;
        }

    }
}
