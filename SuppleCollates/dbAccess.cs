﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace SuppleCollates
{
    class dbAccess
    {
        private System.Data.SqlClient.SqlConnectionStringBuilder _csEcometry;

        private System.Data.SqlClient.SqlConnectionStringBuilder _csConnectShip;

        private string _account;

        public dbAccess(string sqlAccount, string account)
        {
            _account = account;
            //_csEcometry = new System.Data.SqlClient.SqlConnectionStringBuilder(Properties.Settings.Default.dbEcometry);
            //_csEcometry.InitialCatalog = sqlAccount;
            _csConnectShip = new System.Data.SqlClient.SqlConnectionStringBuilder(Properties.Settings.Default.dbConnectship);
        }

        public string TranslateFromEcometry(string Account, string Ecometry, string Type)
        {
            string result = "";


            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {

                    cmd.CommandText = " SELECT ConnectShip from Translations where Ecometry = '" + Ecometry + "'"
                        + " and Account = '" + Account + "'"
                        + " and Type = '" + Type + "' ";

                    cn.ConnectionString = _csConnectShip.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    SqlDataReader rd = cmd.ExecuteReader();

                    if (!rd.HasRows)
                    {
                        if (Account != "GLOBAL")
                        {
                            return TranslateFromEcometry("GLOBAL", Ecometry, Type);

                        }
                        result = Properties.Settings.Default.DefaultShipper;
                        return result;
                    }
                    rd.Read();

                    result = rd[0].ToString();
                    return result;
                }
            }


        }


        public string GetWarehouse(string FullOrderNO, string Account)
        {


            using (SqlConnection cn = new SqlConnection())
            {
                using (SqlCommand cmd = new SqlCommand())
                {
        
            cmd.CommandText = " SELECT substring(WAREVEND,3,2)  from " + Account + "..PICKLOTDETAIL where FULLORDERNO  = '" + FullOrderNO + "'";


                    cn.ConnectionString = _csConnectShip.ToString();
                    cn.Open();
                    cmd.Connection = cn;
                    SqlDataReader rd = cmd.ExecuteReader();

                    if (!rd.HasRows)
                    {
                        return " ";
                    }
                    rd.Read();

                    string result = rd[0].ToString();
                    return result;
                }
            }


        }

    }
}
