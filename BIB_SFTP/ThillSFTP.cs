﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PWS.Mantis.ServiceContractsAndProxies;
using PWS.Mantis.DomainObjects.DataContract;


namespace BIB_SFTP
{
    class ThillSFTP
    {

        private DistributionClient m_ClientProxy;
        private string m_SFtpHost;
        private int m_SFtpPort;
        private string m_SFtpUserName;
        private string m_SFtpPassword;
        private string m_localPath;


        public ThillSFTP(String Host, Int32 Port, String User, String Password)
        {
            m_ClientProxy = new DistributionClient();
            m_SFtpHost = Host;
            m_SFtpPort = Port;
            m_SFtpUserName = User;
            m_SFtpPassword = Password;
        }

        
        public void UploadFile(string fileName)
        {
            //TODO REMOVE
            return;
            //Arrange
            FileTransferDistributionRequest request = BuildFileUploadRequest(fileName);
            
            DistributionResponse response = m_ClientProxy.SubmitRequest(request);
            FileTransferDistributionResponse fileResponse = response as FileTransferDistributionResponse;
            if (fileResponse == null)
            {
                throw new Exception("File Response was numll");
            }

            if (fileResponse.HasError)
            {
                string errorMessage = string.Empty;
                foreach (var message in fileResponse.ErrorMessages)
                {
                    errorMessage += message;
                }

                if (fileResponse.Exception != null)
                {
                    errorMessage += fileResponse.Exception.Message;
                }

                throw new Exception(errorMessage);
            }
        }

        private FileTransferDistributionRequest BuildFileUploadRequest(string fileName)
        {
            return new FileTransferDistributionRequest()
            {
                Host = m_SFtpHost,
                Port = m_SFtpPort,
                UserName = m_SFtpUserName,
                Password = m_SFtpPassword,
                FileAction = FileTransferAction.Upload,
                ProtocolType = FileTransferProtocol.SFTP,
                Resource =   fileName,
                RemoteDirectory = string.Empty
            };
        }
    }
}
