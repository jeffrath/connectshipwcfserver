﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace BIB_SFTP
{
    class dbAccess
    {
        private System.Data.SqlClient.SqlConnectionStringBuilder _csEcometry;

        private System.Data.SqlClient.SqlConnectionStringBuilder _csConnectShip;

        public dbAccess(string sqlAccount
            )
        {
            _csEcometry = new System.Data.SqlClient.SqlConnectionStringBuilder(Properties.Settings.Default.dbEcometry);
            _csEcometry.InitialCatalog = sqlAccount;
            _csConnectShip = new System.Data.SqlClient.SqlConnectionStringBuilder(Properties.Settings.Default.dbConnectship);
        }

        public string TranslateFromEcometry(string Account, string Ecometry, string Type)
        {
           string result = "";

            try
            {
                using (SqlConnection cn = new SqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
 
                        cmd.CommandText = " SELECT ConnectShip from Translations where Ecometry = '" + Ecometry + "'"
                            + " and Account = '" + Account + "'"
                            + " and Type = '" + Type + "' ";
 
                        cn.ConnectionString = _csConnectShip.ToString();
                        cn.Open();
                        cmd.Connection = cn;
                        SqlDataReader rd = cmd.ExecuteReader();

                        if (!rd.HasRows)
                        {
                            if (Account != "GLOBAL")
                            {
                                return TranslateFromEcometry("GLOBAL", Ecometry, Type);

                            }
                           result=Properties.Settings.Default.DefaultShipper;
                            return result;
                        }
                        rd.Read();

                       result = rd[0].ToString();
                       return result;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorLog.ErrorRoutine(false, ex);
                throw;
            }
        }
    }
}
