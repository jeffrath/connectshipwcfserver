﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ShipReport.AMPServices;
using SendEmail;
using Logger;


namespace ShipReport
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ProcessBackup();
                ProcessReport();
            }
            catch(Exception ex )
            {
                LogMessage(ex);
            }
            
        }
        
        static void ProcessBackup()
        {
            dbAccess db = new dbAccess();
            db.CreateBackup();
        }

        static void ProcessReport()
        {
            CoreXmlPortClient client = new CoreXmlPortClient();
            SearchRequest searchRequest = new SearchRequest();
            searchRequest.shipper = "TMAB";
            searchRequest.carrier = "TANDATA_FEDEXFSMS.FEDEX";
            searchRequest.filters = new DataDictionary();
            searchRequest.filters.shipdate = DateTime.Now.AddDays(0);
            SearchResponse searchResponse = client.Search(searchRequest);
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(@"c:\data\shipping" + searchRequest.filters.shipdate.ToString("yyyyMMdd") + ".txt"))
            {
                dbAccess db = new dbAccess();

                string message=null;
                foreach (var item in searchResponse.result.resultData)
                {
                    if (db.VerifyMSN(item.resultData.msn))
                    {
                        continue;
                    }
                     message += item.resultData.shipper.PadRight(5,' ') + " ";
                     message += searchRequest.filters.shipdate.ToString();
                     message += "  ";
                    message += item.resultData.msn.ToString("####") + " ";
                    if (item.resultData.consignee.company == null)
                    {
                        message += " ".PadRight(30, ' ') + " " ;
                    }
                    else
                    {
                        message += item.resultData.consignee.company.PadRight(30, ' ') + " "  ;
                    }
                    message += item.resultData.consignee.contact.PadRight(30, ' ') + " "   ;
                    message += item.resultData.consignee.address1.PadRight(30, ' ') + " " ;
                    message += item.resultData.consignee.city.PadRight(30, ' ') + " ";
                    message += item.resultData.consignee.stateProvince.PadRight(30, ' ') + " "  ;
                    message += item.resultData.consignee.postalCode.PadRight(30, ' ') + " ";
                    message += item.resultData.trackingNumber.PadRight(30, ' ') + " "; ;
                    message += item.resultData.weight.amount.ToString("##,##0.00").PadRight(10,' ') + " "; ;
                    message += item.resultData.barCode.PadRight(50, ' ') + " "; ;
                    message += item.resultData.service.PadRight(50, ' ') + " "; ;
                    message += item.resultData.apportionedTotal.amount.ToString("##,##0.00").PadRight(10, ' ') + " "; ;
                    message += " ";
                    //message += item.resultData.total.amount.ToString("##,##0.00");
                    message += Environment.NewLine;
                    
                }

                if (message != null)
                {
                    LogMessage(message, 1);
                    sw.Write(message);
                    sw.Flush();
                }
            }
        }

      static  public  void LogMessage(Exception ex)
        {
            
            SendEmail.SendEmail se = new SendEmail.SendEmail();
            Logger.ErrorLog.ErrorRoutine(false, ex);
            SendEmail.SendEmail.EmailFrom = Properties.Settings.Default.EmailFrom;
            SendEmail.SendEmail.EmailTo = Properties.Settings.Default.EmailTo;
            SendEmail.SendEmail.SMTPPORT = Properties.Settings.Default.SMTPPort;
            SendEmail.SendEmail.SMTPPWD = Properties.Settings.Default.SMTPPwd;
            SendEmail.SendEmail.SMTPServer = Properties.Settings.Default.SMTPHost;
            SendEmail.SendEmail.SMTPUSER = Properties.Settings.Default.SMTPUser;
            Console.WriteLine(SendEmail.SendEmail.sendError(ex));
        }


        static public  void LogMessage(string message, int severity)
        {
            string subject = "Informational Message from " + Environment.GetCommandLineArgs()[0];
            if (severity == 1)
            {
                subject = "Warning Message from " + Environment.GetCommandLineArgs()[0];
            }
            Console.WriteLine(message);
            Logger.ErrorLog.ErrorRoutine(false, message);
            SendEmail.SendEmail se = new SendEmail.SendEmail();
            SendEmail.SendEmail.EmailFrom = Properties.Settings.Default.EmailFrom;
            SendEmail.SendEmail.EmailTo = Properties.Settings.Default.EmailTo;
            SendEmail.SendEmail.SMTPPORT = Properties.Settings.Default.SMTPPort;
            SendEmail.SendEmail.SMTPPWD = Properties.Settings.Default.SMTPPwd;
            SendEmail.SendEmail.SMTPServer = Properties.Settings.Default.SMTPHost;
            SendEmail.SendEmail.SMTPUSER = Properties.Settings.Default.SMTPUser;
            SendEmail.SendEmail.sendMessage(message, subject);

        }

    }
}
