﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace ShipReport
{
    class dbAccess
    {

        private System.Data.SqlClient.SqlConnectionStringBuilder _csConnectShip;

        public dbAccess()
        {
            _csConnectShip = new System.Data.SqlClient.SqlConnectionStringBuilder(Properties.Settings.Default.dbConnectShip);

        }

        public bool VerifyMSN( int MSN)
        {
            string sql =
        " select count(*) from ShippingTransactions where Msn = @Msn";

            using (SqlConnection cn = new System.Data.SqlClient.SqlConnection())
            using (SqlCommand cmd = cn.CreateCommand())
            {
                cmd.Parameters.Add("@Msn", System.Data.SqlDbType.BigInt);
                cmd.Parameters["@Msn"].Value = MSN;

                cn.ConnectionString = _csConnectShip.ToString();
                cn.Open();
                cmd.CommandText = sql;
                int results = Convert.ToInt32(cmd.ExecuteScalar());

                if (results == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public void CreateBackup()
        {
            string sql =
                "	insert into ShippingTransactionsBackup "
                + " select distinct "
                + " [ST].[Account]  "
                + " ,[ST].[Carrier]  "
                + " ,[ST].[Shipper] "
                + " ,[ST].[Document]  "
                + " ,[ST].[ShipFile]  "
                + " ,[ST].[Msn]  "
                + " ,[ST].[FullOrderNo]  "
                + " ,[ST].[Weight]  "
                + " ,[ST].[TrackingNo]  "
                + " ,[ST].[Service]  "
                + " ,[ST].[EcometryShipMethod]  "
                + " ,[ST].[Cost]  "
                + " ,[ST].[ShipDate]  "
                + " ,[ST].[CreateDate]  "
                + " ,[ST].[StatusDate]  "
                + " ,[ST].[Status]  "
                + " ,[ST].[BarCode1]  "
                + " ,[ST].[BarCode2]  "
                + " ,[ST].[BarCode3]  "
                + " from ShippingTransactions ST"
                + " left join ShippingTransactionsBackup STB on ST.Msn = STB.Msn  "
                + " where isNull(STB.FullOrderNo,'Choose') = 'Choose'  "
                + " and ST.Msn <> 0  ";

                        using (SqlConnection cn = new System.Data.SqlClient.SqlConnection())
                        using (SqlCommand cmd = cn.CreateCommand())
                        {

                            cn.ConnectionString = _csConnectShip.ToString();
                            cn.Open();
                            cmd.CommandText = sql;
                            int results = cmd.ExecuteNonQuery();

                            if (results != 0)
                            {
                                Console.WriteLine("Backup records added:" + results);
                            }
                        }

        }
    }
}
