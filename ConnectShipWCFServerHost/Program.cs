﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Reflection;

namespace ConnectShipWCFServerHost
{
    class Program
    {
        static int Main(string[] args)
        {
            try
            {
                ConnectShipWCFServer.ConnectShip cp = new ConnectShipWCFServer.ConnectShip();
                 
                ServiceHost sHost = new ServiceHost(typeof(ConnectShipWCFServer.ConnectShip));
                Console.WriteLine("****** Starting:" + DateTime.Now.ToString() + "******");
                Console.WriteLine(Version());
                Console.WriteLine("<Hit Enter Key to End>");
                sHost.Open();
                Console.ReadLine();
                sHost.Close();
                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return 10;
            }
        }

        //*********************
        public static String Version()
        {
            string response;
            // This variable holds the amount of indenting that  
            // should be used when displaying each line of information.
            int indent = 0;
            // Display information about the EXE assembly.
            Assembly a = System.Reflection.Assembly.GetExecutingAssembly();
            response = " ".PadLeft(indent, ' ') + "Assembly identity=" + a.FullName;
            response += Environment.NewLine;
            response += " ".PadLeft(indent, ' ') + "Codebase=" + a.CodeBase;
            response += Environment.NewLine;
            // Display the set of assemblies our assemblies reference.

            response += " ".PadLeft(indent, ' ') + "Referenced assemblies:";
            response += Environment.NewLine;
            foreach (AssemblyName an in a.GetReferencedAssemblies())
            {
                response += response = " ".PadLeft(indent, ' ') + "Name=" + an.Name + " Version=" + an.Version.ToString();
                response += Environment.NewLine;
            }
            return response;
        } 
    }
}
