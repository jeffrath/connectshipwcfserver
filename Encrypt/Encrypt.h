// Encrypt.h : main header file for the Encrypt DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CEncryptApp
// See Encrypt.cpp for the implementation of this class
//

class CEncryptApp : public CWinApp
{
public:
	CEncryptApp();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
